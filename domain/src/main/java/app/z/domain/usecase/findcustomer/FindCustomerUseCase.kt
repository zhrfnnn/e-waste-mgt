package app.z.domain.usecase.findcustomer

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 27/01/2021
* */

class FindCustomerUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val users : List<User>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(wasteBank : User,value : String) : Observable<Result> {
        return repository.findCustomer(wasteBank, value)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
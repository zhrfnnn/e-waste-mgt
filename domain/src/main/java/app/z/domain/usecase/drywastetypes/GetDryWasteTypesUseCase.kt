package app.z.domain.usecase.drywastetypes

import app.z.domain.model.DryWasteType
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 25/01/2021
* */

class GetDryWasteTypesUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val dryWastes: List<DryWasteType>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute() : Observable<Result> {
        return repository.getDryWasteTypes()
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
package app.z.domain.usecase.photoprofile

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 12/01/2021
* */

class PhotoProfileUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val result: Boolean) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user : User) : Observable<Result> {
        return repository.photoProfile(user)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
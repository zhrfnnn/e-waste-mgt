package app.z.domain.usecase.requesthw

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 12/01/2021
* */

class RequestCollectionWasteUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val result: Boolean) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }
    
    fun execute(user: User,
                date: String?,
                wetWeight: Double?,
                wetPhoto: String?,
                dryWeight: Double?,
                dryPhoto: String?,
                dry : String? = null) : Observable<Result> {
        return repository.requestCollectWaste("1",user, date, wetWeight, wetPhoto, dryWeight, dryPhoto, dry)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
package app.z.domain.usecase.usertype

import app.z.domain.model.UserType
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 07/01/2021
* */

class UserTypeUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val userTypes: List<UserType>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute() : Observable<Result> {
        return repository.getUserType()
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
package app.z.domain.usecase.schedule

import app.z.domain.model.User
import app.z.domain.model.tablemodel.WasteCollectionTableModel
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Zharfan on 15/01/2021
 * */

class ScheduleUseCase @Inject constructor(private val repository: Repository) {
    
    sealed class Result {
        object Loading : Result()
        data class Success(val result: List<WasteCollectionTableModel>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }
    
    fun execute(user: User,
                date : String?) : Observable<Result> {
        return repository.scheduleDriver(user,date)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
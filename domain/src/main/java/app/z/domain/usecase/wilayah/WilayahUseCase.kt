package app.z.domain.usecase.wilayah

import app.z.domain.model.Wilayah
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 12/01/2021
* */

class WilayahUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val wilayahs: List<Wilayah>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute() : Observable<Result> {
        return repository.getWilayah()
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
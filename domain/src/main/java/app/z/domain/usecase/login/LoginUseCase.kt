package app.z.domain.usecase.login

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 09/01/2021
* */

class LoginUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val user: User) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(username : String, password : String) : Observable<Result> {
        return repository.login(username, password)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
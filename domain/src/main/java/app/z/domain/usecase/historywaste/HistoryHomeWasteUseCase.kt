package app.z.domain.usecase.historywaste

import app.z.domain.model.User
import app.z.domain.model.tablemodel.HomeWasteTableModel
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Zharfan on 16/01/2021
 * */

class HistoryHomeWasteUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val homeWasteList: List<HomeWasteTableModel>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user: User,startDate : String?,endDate : String?) : Observable<Result> {
        return repository.historyHomeWaste(user,startDate,endDate)
            .map { Result.Success(it) as Result}
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
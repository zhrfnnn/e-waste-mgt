package app.z.domain.usecase.location

import app.z.domain.model.Location
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 06/01/2021
* */

class LocationUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val locations: List<Location>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun executeProvince() : Observable<Result> {
        return repository.getProvince()
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    fun executeCity(province : String) : Observable<Result> {
        return repository.getCity(province)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    fun executeDistrict(city : String) : Observable<Result> {
        return repository.getDistrict(city)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    fun executeSubDistrict(district : String) : Observable<Result> {
        return repository.getSubDistrict(district)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    fun executePostal(subDistrict : String) : Observable<Result> {
        return repository.getPostal(subDistrict)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
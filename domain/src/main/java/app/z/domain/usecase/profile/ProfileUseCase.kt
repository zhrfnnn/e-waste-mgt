package app.z.domain.usecase.profile

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Zharfan on 10/12/2020
 * */

class ProfileUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val user : User) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user: User) : Observable<Result> {
        return repository.getProfilesByUserId(user)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
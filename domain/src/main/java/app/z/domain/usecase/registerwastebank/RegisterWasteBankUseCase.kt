package app.z.domain.usecase.registerwastebank

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 19/01/2021
* */

class RegisterWasteBankUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val result: Boolean) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user: User,
                wasteBankId : String) : Observable<Result> {
        return repository.registerWasteBank(user,wasteBankId)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
package app.z.domain.usecase.balanceinformation

import app.z.domain.model.User
import app.z.domain.model.tablemodel.BalanceInformationTableModel
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
* Created by Zharfan on 20/01/2021
* */

class BalanceInformationUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val balanceList : List<BalanceInformationTableModel>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user: User,startDate : String?, endDate : String?) : Observable<Result> {
        return repository.balanceInformation(user,startDate,endDate)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}
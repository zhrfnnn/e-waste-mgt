package app.z.domain.usecase.findwastebank

import app.z.domain.model.User
import app.z.domain.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Zharfan on 17/01/2021
 * */

class FindWasteBankUseCase @Inject constructor(private val repository: Repository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val wasteBankList : List<User>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(user: User) : Observable<Result> {
        return repository.findWasteBank(user)
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }
}

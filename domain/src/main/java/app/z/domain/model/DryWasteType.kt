package app.z.domain.model

/**
 * Created by Zharfan on 15/01/2021
 * */

class DryWasteType (
    var name : String? = null,
    var category : String? = null,
    var code : String? = null,
    var price : Double? = null
)
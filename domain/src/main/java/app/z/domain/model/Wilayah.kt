package app.z.domain.model

/**
* Created by Zharfan on 12/01/2021
* */

data class Wilayah (

    var id : Int? = null,
    var wilayah : String? = null
)
package app.z.domain.model

/**
* Created by Zharfan on 16/11/2020
* */

data class User(
    var id : Long,
    var fullname : String? = "",
    var address : String? = "",
    var picture : String? = "",
    var province : String? = "",
    var city : String? = "",
    var district : String? = "",
    var subDistrict : String? = "",
    var postalCode : String? = "",
    var nik : String? = "",
    var nikPicture : String? = "",
    var position : UserType?,
    var email : String? = "",
    var referralEmail : String? = "",
    var mobileNumber : String? = "",
    var password : String? = "",
    var beaconId : String? = "",
    var wilayah : Wilayah?,
    var wilayahName : String? = ""
)
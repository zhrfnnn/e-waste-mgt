package app.z.domain.model.tablemodel

/**
* Created by Zharfan on 22/12/2020
* */

open class WasteCollectionHistoryTableModel (
    var address : String? = null,
    var wilayah : String? = null,
    var homeId : String? = null,
    var weight : Double? = null,
    var total : Double? = null
) : BaseTableModel()
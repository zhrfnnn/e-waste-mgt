package app.z.domain.model.tablemodel

/**
* Created by Zharfan on 22/12/2020
* */

open class WasteCollectionTableModel (
    var id : String? = null,
    var homeId : String? = null,
    var wasteWeight : Double? = null,
    var image : String? = null,
    var status : String? = null
) : BaseTableModel() {

    fun isDone() : Boolean {
        return status.equals("1")
    }
}
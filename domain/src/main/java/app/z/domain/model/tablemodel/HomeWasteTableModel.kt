package app.z.domain.model.tablemodel

/**
* Created by Zharfan on 22/12/2020
* */

class HomeWasteTableModel (
    var truckId : String? = null,
    var wetWaste : Double? = null,
    var dryWaste : Double? = null,
    var total : Double? = null
) : BaseTableModel()
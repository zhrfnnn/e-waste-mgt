package app.z.domain.model.tablemodel

import app.z.domain.model.User
import java.util.*

/**
* Created by Zharfan on 22/12/2020
* */

class TransferHistoryTableModel (
    var customer : User? = null,
    var bank : String? = null,
    var amount : Long? = null
) : BaseTableModel()
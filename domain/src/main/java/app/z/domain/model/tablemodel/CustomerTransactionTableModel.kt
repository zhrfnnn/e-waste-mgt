package app.z.domain.model.tablemodel

import app.z.domain.model.DryWasteType
import app.z.domain.model.User
import java.util.*

/**
* Created by Zharfan on 22/12/2020
* */

class CustomerTransactionTableModel (
    var customer : User? = null,
    var dryWasteTypes: List<DryWasteType?>? = null,
    var total : Long? = null
) : BaseTableModel()
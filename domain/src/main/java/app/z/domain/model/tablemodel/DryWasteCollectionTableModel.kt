package app.z.domain.model.tablemodel

import java.io.Serializable

/**
* Created by Zharfan on 22/12/2020
* */

class DryWasteCollectionTableModel : WasteCollectionTableModel() {
    var memberId : Long? = null
    var price : Double? = null
    var totalPrice : Double? = null
}
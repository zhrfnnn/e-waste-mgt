package app.z.domain.model

/**
* Created by Zharfan on 07/01/2021
* */

data class UserType (
    var id : Int? = null,
    var name : String? = null
) {
    companion object {
        val HOME_WASTE = UserType(
            1,
            "Home Waste"
        )
        val WASTE_BANK = UserType(
            2,
            "Waste Bank"
        )
        val INDUSTRIAL_WASTE = UserType(
            3,
            "Industrial Waste"
        )
        val WASTE_COLLECTION = UserType(
            4,
            "Waste collection"
        )
        val WASTE_FACTORY_DISPOSAL = UserType(
            5,
            "Waste Factory Disposal"
        )
        val WASTE_TRANSFER_TRANSPORT = UserType(
            6,
            "Waste Transfer Transport"
        )
        val W3R = UserType(
            7,
            "Waste Reduce Recycling Reuse"
        )
        val TPS3R = UserType(
            8,
            "TPS 3R/ PDU"
        )

        fun getById(id : Int?) : UserType? {
            return when(id){
                1 -> HOME_WASTE
                2 -> WASTE_BANK
                3 -> INDUSTRIAL_WASTE
                4 -> WASTE_COLLECTION
                5 -> WASTE_FACTORY_DISPOSAL
                6 -> WASTE_TRANSFER_TRANSPORT
                7 -> W3R
                8 -> TPS3R
                else -> null
            }
        }
    }

    fun sameAs(userType: UserType) : Boolean {
        return this.name.equals(userType.name)
    }
}
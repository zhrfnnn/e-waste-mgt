package app.z.domain.model.tablemodel

/**
* Created by Zharfan on 22/12/2020
* */

class BalanceInformationTableModel (
    var notes : String? = null,
    var mutation : Double? = null,
    var balance : Double? = null
) : BaseTableModel()
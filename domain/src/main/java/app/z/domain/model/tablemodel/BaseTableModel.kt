package app.z.domain.model.tablemodel

import java.util.*

/**
* Created by Zharfan on 22/12/2020
* */

open class BaseTableModel {
    var date : Date? = null
    var time : String? = null
}
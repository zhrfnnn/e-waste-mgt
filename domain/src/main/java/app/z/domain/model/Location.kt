package app.z.domain.model

/**
* Created by Zharfan on 06/01/2021
* */

data class Location (
    var province : String? = null,
    var city : String? = null,
    var district : String? = null,
    var subDistrict : String? = null,
    var postal : String? = null
)
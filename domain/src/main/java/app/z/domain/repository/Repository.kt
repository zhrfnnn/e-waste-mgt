package app.z.domain.repository

import app.z.domain.model.*
import app.z.domain.model.tablemodel.*
import io.reactivex.Observable

/**
* Created by Zharfan on 06/01/2021
* */

interface Repository {

    fun register(user: User) : Observable<Boolean>


    fun login(username : String,
              password : String) : Observable<User>


    fun getProfilesByUserId(user: User) : Observable<User>

    fun updateProfile(user : User) : Observable<Boolean>

    fun photoProfile(user: User) : Observable<Boolean>


    fun getProvince() : Observable<List<Location>>

    fun getCity(province : String) : Observable<List<Location>>

    fun getDistrict(city : String) : Observable<List<Location>>

    fun getSubDistrict(district : String) : Observable<List<Location>>

    fun getPostal(subDistrict : String) : Observable<List<Location>>


    fun getUserType() : Observable<List<UserType>>

    fun getWilayah() : Observable<List<Wilayah>>


    fun requestCollectWaste(collect : String,
                            user: User,
                            date : String?,
                            wetWeight : Double?,
                            wetPhoto : String?,
                            dryWeight : Double?,
                            dryPhoto : String?,
                            dry : String? = null) : Observable<Boolean>

    fun historyHomeWaste(user: User,
                         startDate : String?,
                         endDate : String?) : Observable<List<HomeWasteTableModel>>

    fun scheduleDriver(user: User,
                       date : String?) : Observable<List<WasteCollectionTableModel>>

    fun dryScheduleDriver(user: User,
                       date : String?) : Observable<List<DryWasteCollectionTableModel>>

    fun updateWasteStatus(user: User,
                          wasteId : String?) : Observable<Boolean>

    fun historyWasteCollection(user: User,
                               startDate : String?,
                               endDate : String?) : Observable<List<WasteCollectionHistoryTableModel>>

    fun findWasteBank(user: User) : Observable<List<User>>

    fun getDryWasteTypes() : Observable<List<DryWasteType>>

    fun registerWasteBank(user: User,
                          wasteBankId : String) : Observable<Boolean>

    fun balanceInformation(user: User,
                           startDate : String?,
                           endDate : String?) : Observable<List<BalanceInformationTableModel>>

    fun findCustomer(wasteBank : User,
                     value : String) : Observable<List<User>>
}
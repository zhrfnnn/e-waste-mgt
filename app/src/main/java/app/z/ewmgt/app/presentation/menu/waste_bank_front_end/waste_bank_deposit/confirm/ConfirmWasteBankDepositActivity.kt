package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.domain.model.DryWasteType
import app.z.domain.model.User
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeActivity
import app.z.ewmgt.databinding.ActivityConfirmWasteBankDepositBinding
import com.google.gson.Gson
import javax.inject.Inject

/**
 * Created by Zharfan on 28/01/2021
 * */

class ConfirmWasteBankDepositActivity : BaseActivity() {

    companion object {
        const val SELECTED_USER = "SELECTED_USER"
    }

    @Inject
    lateinit var viewModel: ConfirmWasteBankDepositViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityConfirmWasteBankDepositBinding>(this, R.layout.activity_confirm_waste_bank_deposit)
        screenComponent.inject(this)

        binding.viewModel = viewModel

        val selectedUser = (Gson().fromJson(intent.getStringExtra(SELECTED_USER), User::class.java))
        viewModel.selectedUser.set(selectedUser)
        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            DryWasteTypeActivity.DRY_WASTE_TYPE -> {
                if (resultCode == Activity.RESULT_OK){
                    viewModel.onDryWasteTypeSelected(Gson().fromJson(data?.getStringExtra(DryWasteTypeActivity.EXTRA_DRY_WASTE_TYPE),DryWasteType::class.java))
                }
            }
        }
    }
}
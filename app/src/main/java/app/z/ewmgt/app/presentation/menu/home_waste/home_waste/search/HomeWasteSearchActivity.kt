package app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityHomeWasteSearchBinding
import app.z.ewmgt.app.customviews.CustomRangeDatePicker
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 16/01/2021
 * */

class HomeWasteSearchActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: HomeWasteSearchViewModel

    private var binding : ActivityHomeWasteSearchBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_waste_search)
        screenComponent.inject(this)
        binding?.viewModel = viewModel

        viewModel.bound()

        binding?.rangePickerAhws?.setListener(object : CustomRangeDatePicker.CustomRangeDatePickerListener {
            override fun onDatesPicked(fromDate: Date, toDate: Date) {
                viewModel.search(fromDate,toDate)
            }
        })
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_YEAR, -14)
        binding?.rangePickerAhws?.setDateFrom(c.time)
        binding?.rangePickerAhws?.setDateTo(Date())
        viewModel.search(c.time,Date())
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
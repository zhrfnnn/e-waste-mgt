package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.register.RegisterActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 24/01/2021
 * */

class CustomerRegistrationRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as CustomerRegistrationActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as CustomerRegistrationActivity).hideLoading()
    }

    fun onGetProvinces(provinces : List<String>){
        (activityRef.get() as CustomerRegistrationActivity).onGetProvinces(provinces)
    }

    fun onGetCities(cities : List<String>){
        (activityRef.get() as CustomerRegistrationActivity).onGetCities(cities)
    }

    fun onGetDistricts(districts : List<String>){
        (activityRef.get() as CustomerRegistrationActivity).onGetDistricts(districts)
    }

    fun onGetSubDistricts(subDistricts : List<String>){
        (activityRef.get() as CustomerRegistrationActivity).onGetSubDistricts(subDistricts)
    }

    fun onGetPostals(postals : List<String>){
        (activityRef.get() as CustomerRegistrationActivity).onGetPostals(postals)
    }
}
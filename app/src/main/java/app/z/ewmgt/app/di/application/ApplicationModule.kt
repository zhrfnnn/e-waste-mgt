package app.z.ewmgt.app.di.application

import android.content.Context
import app.z.ewmgt.app.EWasteMgtApp
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
* Created by Zharfan on 09/11/2020
* */

@Module
class ApplicationModule(private val application: EWasteMgtApp) {

    @Provides
    @Singleton
    fun provideApplication(): EWasteMgtApp {
        return application
    }

    private fun getInterceptor() : OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
        return  okHttpClient
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("http://plasaweb.com/ewaste/json/")
            .client(getInterceptor())
            .build()
    }

    @Provides
    fun provideContext() : Context {
        return application.applicationContext
    }

}

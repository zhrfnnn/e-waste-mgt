package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer

import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.log
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.usecase.findcustomer.FindCustomerUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Zharfan on 27/01/2021
 * */

class SearchCustomerViewModel @Inject constructor(private val context: Context,
                                                  private val findCustomerUseCase: FindCustomerUseCase,
                                                  private val searchCustomerRouter: SearchCustomerRouter) : BaseViewModel(searchCustomerRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val users = ObservableArrayList<User>()

    val fullName = ObservableField<String>()
    val email = ObservableField<String>()

    private fun handleSearchCustomer(result : FindCustomerUseCase.Result){
        when(result){
            is FindCustomerUseCase.Result.Success -> {
                users.clear()
                users.addAll(result.users)
            }
            is FindCustomerUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    fun onUserSelected(user: User) {
        val result = Intent()
        result.putExtra(SearchCustomerActivity.EXTRA_CUSTOMER,Gson().toJson(user))
        searchCustomerRouter.onFinishResultScreen(result)
    }

    fun onSearch(){
        if (Session.currentUser.isNotNull()){
            if (!fullName.get().isNullOrBlank() || !email.get().isNullOrBlank()){
                if (!email.get().isNullOrBlank()){
                    findCustomerUseCase.execute(Session.currentUser!!,email.get()!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleSearchCustomer(it) }
                        .addTo(disposables)
                } else {
                    if (!fullName.get().isNullOrBlank()){
                        findCustomerUseCase.execute(Session.currentUser!!,fullName.get()!!)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { handleSearchCustomer(it) }
                            .addTo(disposables)
                    } else {
                        context.toast("Please fill Customer Email or Full Name")
                    }
                }
            }
        }
    }
}
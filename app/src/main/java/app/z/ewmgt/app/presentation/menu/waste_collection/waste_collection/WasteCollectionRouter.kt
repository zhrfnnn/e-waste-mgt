package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search.WasteCollectionSearchActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 15/01/2021
 * */

class WasteCollectionRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun search(){
        nextScreen(WasteCollectionSearchActivity::class.java, Bundle())
    }

    fun searchBeacon(){
        (activityRef.get() as WasteCollectionActivity).bindBeacon()
    }

    fun unbindBeacon(){
        (activityRef.get() as WasteCollectionActivity).unbindBeacon()
    }

    fun showLoading(){
        (activityRef.get() as WasteCollectionActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as WasteCollectionActivity).hideLoading()
    }
}
package app.z.ewmgt.app.util

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
* Created by Zharfan on 19/06/2019
* */

open class CoroutineContextProvider {
    open val main: CoroutineContext by lazy { Dispatchers.Main }
    open val io: CoroutineContext by lazy { Dispatchers.IO }
}
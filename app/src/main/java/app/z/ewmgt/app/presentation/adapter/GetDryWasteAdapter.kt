package app.z.ewmgt.app.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.domain.model.DryWasteType
import app.z.ewmgt.app.util.toIDRKg
import app.z.ewmgt.databinding.ItemDryWasteBinding

/**
 * Created by Zharfan on 27/01/2021
 * */

class GetDryWasteAdapter(dryWasteTypes : ObservableList<DryWasteType>): ObservableRecyclerViewAdapter<DryWasteType, GetDryWasteAdapter.Holder>(dryWasteTypes){

    lateinit var onDryWasteTypeSelected: (dryWasteType : DryWasteType) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemDryWasteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class Holder(private val binding: ItemDryWasteBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(dryWasteType : DryWasteType) {
            binding.priceIdw.text = dryWasteType.price?.toIDRKg()
            binding.codeIdw.text = dryWasteType.code
            binding.categoryIdw.text = dryWasteType.category
            binding.nameIdw.text = dryWasteType.name

            binding.root.setOnClickListener {
                onDryWasteTypeSelected.invoke(dryWasteType)
            }
        }
    }
}
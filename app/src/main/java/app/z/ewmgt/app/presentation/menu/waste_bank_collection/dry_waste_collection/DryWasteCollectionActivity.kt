package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityDryWasteCollectionBinding
import javax.inject.Inject

/**
 * Created by Zharfan on 24/01/2021
 * */

class DryWasteCollectionActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DryWasteCollectionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityDryWasteCollectionBinding>(this, R.layout.activity_dry_waste_collection)
        screenComponent.inject(this)
        binding?.viewModel = viewModel
    }

    override fun onResume() {
        super.onResume()

        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
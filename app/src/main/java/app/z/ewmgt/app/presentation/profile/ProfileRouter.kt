package app.z.ewmgt.app.presentation.profile

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.register.RegisterActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 17/12/2020
* */

class ProfileRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun setProfilePicture(profilePicture : String?){
        (activityRef.get() as ProfileActivity).setProfilePicture(profilePicture)
    }

    fun searchBeacon(){
        (activityRef.get() as ProfileActivity).bindBeacon()
    }

    fun onGetProvinces(provinces : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetProvinces(provinces,position)
    }

    fun onGetCities(cities : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetCities(cities,position)
    }

    fun onGetDistricts(districts : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetDistricts(districts,position)
    }

    fun onGetSubDistricts(subDistricts : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetSubDistricts(subDistricts,position)
    }

    fun onGetPostals(postals : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetPostals(postals,position)
    }

    fun onGetWilayahs(wilayahs : List<String>, position : Int){
        (activityRef.get() as ProfileActivity).onGetWilayahs(wilayahs,position)
    }

    fun showLoading(){
        (activityRef.get() as ProfileActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as ProfileActivity).hideLoading()
    }
}
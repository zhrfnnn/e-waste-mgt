package app.z.ewmgt.app.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.domain.model.User
import app.z.ewmgt.databinding.ItemUserBinding

/**
 * Created by Zharfan on 27/01/2021
 * */

class SearchCustomerAdapter(users : ObservableList<User>) : ObservableRecyclerViewAdapter<User, SearchCustomerAdapter.Holder>(users){

    lateinit var onUserSelected: (user: User) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class Holder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(user : User) {
            binding.nameIu.text = user.fullname
            binding.emailIu.text = user.email

            binding.root.setOnClickListener {
                onUserSelected.invoke(user)
            }
        }
    }
}
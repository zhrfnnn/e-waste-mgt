package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm

import android.content.Context
import androidx.databinding.ObservableField
import app.z.data.util.log
import app.z.domain.model.DryWasteType
import app.z.domain.model.User
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import javax.inject.Inject

/**
 * Created by Zharfan on 28/01/2021
 * */

class ConfirmWasteBankDepositViewModel @Inject constructor(private val context: Context,
                                                           private val confirmWasteBankDepositRouter: ConfirmWasteBankDepositRouter) : BaseViewModel(confirmWasteBankDepositRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val selectedUser = ObservableField<User>()

    override fun bound() {
        super.bound()

    }

    fun onAddWaste(){
        confirmWasteBankDepositRouter.addWaste()
    }

    fun onDryWasteTypeSelected(dryWasteType: DryWasteType?){
        log(dryWasteType?.name)
    }
}
package app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toServerFormatString
import app.z.domain.model.tablemodel.HomeWasteTableModel
import app.z.domain.usecase.historywaste.HistoryHomeWasteUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 16/01/2021
 * */

class HomeWasteSearchViewModel @Inject constructor(private val context: Context,
                                                   private val homeWasteSearchRouter: HomeWasteSearchRouter,
                                                   private val historyHomeWasteUseCase: HistoryHomeWasteUseCase) : BaseViewModel(homeWasteSearchRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val homeWasteList = ObservableArrayList<HomeWasteTableModel>()

    private fun handleSearchList(result : HistoryHomeWasteUseCase.Result){
        when(result){
            is HistoryHomeWasteUseCase.Result.Success -> {
                homeWasteList.clear()
                homeWasteList.addAll(result.homeWasteList)
            }
            is HistoryHomeWasteUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    fun search(startDate: Date,
               endDate: Date){
        if (Session.currentUser.isNotNull()){
            historyHomeWasteUseCase.execute(
                Session.currentUser!!,
                startDate.toServerFormatString(),
                endDate.toServerFormatString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleSearchList(it) }
                .addTo(disposables)
        }
    }
}
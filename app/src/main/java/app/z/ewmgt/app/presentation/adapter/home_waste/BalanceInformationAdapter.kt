package app.z.ewmgt.app.presentation.adapter.home_waste

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.data.util.toDefaultFormatString
import app.z.domain.model.tablemodel.BalanceInformationTableModel
import app.z.ewmgt.app.presentation.adapter.ObservableRecyclerViewAdapter
import app.z.ewmgt.app.util.toIDR
import app.z.ewmgt.databinding.ItemBalanceInformationBinding

/**
 * Created by Zharfan on 20/01/2021
 * */

class BalanceInformationAdapter (balances : ObservableList<BalanceInformationTableModel>) : ObservableRecyclerViewAdapter<BalanceInformationTableModel, BalanceInformationAdapter.Holder>(balances){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemBalanceInformationBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(private val binding: ItemBalanceInformationBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(waste : BalanceInformationTableModel) {
            binding.dateIbi.text = waste.date.toDefaultFormatString()
            binding.notesIbi.text = waste.notes
            binding.mutationIbi.text = waste.mutation?.toIDR()
            binding.balanceIbi.text = waste.balance?.toIDR()
        }
    }
}
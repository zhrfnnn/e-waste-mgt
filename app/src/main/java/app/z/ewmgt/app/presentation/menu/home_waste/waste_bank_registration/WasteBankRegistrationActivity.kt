package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.data.util.gone
import app.z.data.util.visible
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityWasteBankRegistrationBinding
import javax.inject.Inject

/**
* Created by Zharfan on 19/12/2020
* */

class WasteBankRegistrationActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : WasteBankRegistrationViewModel

    private var openedMore = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityWasteBankRegistrationBinding = DataBindingUtil.setContentView(this, R.layout.activity_waste_bank_registration)
        screenComponent.inject(this)
        binding.viewModel = viewModel

        viewModel.bound()

        binding.butMoreAwbr.setOnClickListener {
            if (openedMore){
                openedMore = false
                binding.moreLayoutAwbr.gone()
                binding.butMoreAwbr.setImageResource(R.drawable.arrow_border_down)
            } else {
                openedMore = true
                binding.moreLayoutAwbr.visible()
                binding.butMoreAwbr.setImageResource(R.drawable.arrow_border_up)
            }
        }
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

}
package app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 16/01/2021
 * */

class HomeWasteSearchRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as HomeWasteSearchActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as HomeWasteSearchActivity).hideLoading()
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 27/01/2021
 * */

class DryWasteTypeRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as DryWasteTypeActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as DryWasteTypeActivity).hideLoading()
    }
}
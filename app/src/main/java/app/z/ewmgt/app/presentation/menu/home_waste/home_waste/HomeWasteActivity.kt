package app.z.ewmgt.app.presentation.menu.home_waste.home_waste

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import app.z.data.util.gone
import app.z.data.util.log
import app.z.data.util.visible
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.fromFilePath
import app.z.ewmgt.databinding.ActivityHomeWasteBinding
import javax.inject.Inject

/**
* Created by Zharfan on 19/12/2020
* */

class HomeWasteActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : HomeWasteViewModel

    private var binding : ActivityHomeWasteBinding? = null
    private var openedList = false
    private var isWetWastePicture = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_waste)
        screenComponent.inject(this)
        binding?.viewModel = viewModel

        viewModel.bound()

        binding?.butListAhw?.setOnClickListener {
            if (openedList){
                openedList = false
                binding?.pastLayout?.gone()
                binding?.butListAhw?.setImageResource(R.drawable.arrow_border_down)
            } else {
                openedList = true
                binding?.pastLayout?.visible()
                binding?.butListAhw?.setImageResource(R.drawable.arrow_border_up)
            }
        }

        binding?.imgWetAhw?.setOnClickListener {
            isWetWastePicture = true
            getImage(Companion.ImageRatio.IMAGE_4_3)
        }

        binding?.imgDryAhw?.setOnClickListener {
            isWetWastePicture = false
            getImage(Companion.ImageRatio.IMAGE_4_3)
        }

        binding?.moreAhw?.setOnClickListener {
            openMenu(it)
        }
    }

    private fun openMenu(view : View){
        val popup = PopupMenu(this,view)
        popup.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.search_hwm -> {
                    viewModel.moreSearch()
                    return@setOnMenuItemClickListener true
                }
                else-> {
                    return@setOnMenuItemClickListener false
                }
            }
        }
        popup.inflate(R.menu.home_waste_menu)
        popup.show()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onSuccessGettingImage(filePath: String?) {
        if (isWetWastePicture){
            binding?.imgWetAhw?.fromFilePath(filePath,R.drawable.ic_camera)
            viewModel.wetPhoto.set(filePath)
        } else {
            binding?.imgDryAhw?.fromFilePath(filePath,R.drawable.ic_camera)
            viewModel.dryPhoto.set(filePath)
        }
    }

    override fun onFailedGettingImage(error: String?) {
        log(error)
    }
}
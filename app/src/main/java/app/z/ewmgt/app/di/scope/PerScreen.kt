package app.z.ewmgt.app.di.scope

import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME

/**
* Created by Zharfan on 09/11/2020
* */

@Scope
@Retention(RUNTIME)
annotation class PerScreen
package app.z.ewmgt.app.presentation.profile

import android.os.Bundle
import android.os.RemoteException
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import app.z.data.util.gone
import app.z.data.util.log
import app.z.data.util.toProfileUrl
import app.z.data.util.visible
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.*
import app.z.ewmgt.databinding.ActivityProfileBinding
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_profile.*
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.MonitorNotifier
import org.altbeacon.beacon.Region
import javax.inject.Inject

/**
* Created by Zharfan on 17/12/2020
* */

class ProfileActivity : BaseActivity(),BeaconConsumer {

    @Inject
    lateinit var viewModel : ProfileViewModel

    @Inject
    lateinit var beaconManager: BeaconManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        screenComponent.inject(this)
        binding.viewModel = viewModel

        viewModel.bound()

        edit_profilepict_ap?.setOnClickListener {
            getImage(Companion.ImageRatio.IMAGE_1_1)
        }
    }

    override fun onDestroy() {
        viewModel.unbound()
        unbindBeacon()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        viewModel.showError.observe()
            .subscribe {
                AlertDialog.Builder(this)
                    .setTitle(Strings.get(R.string.error_title))
                    .setMessage(viewModel.errorString.get())
                    .setNeutralButton(Strings.get(R.string.close)) { dialog, _ -> dialog.dismiss() }
            }.addTo(disposables)
    }

    override fun onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(object : MonitorNotifier{

            override fun didEnterRegion(region: Region?) {
                try {
                    beaconManager.addRangeNotifier { beacons, region ->
                        if (beacons.isNotEmpty()){
                            beacons.forEach {
                                if (it.distance < 1.0){
                                    viewModel.beaconId.set(it.bluetoothAddress)
                                    unbindBeacon()
                                    return@forEach
                                }
                            }
                        } else {
                            log("There's no beacons here.")
                        }
                    }
                    beaconManager.startRangingBeaconsInRegion(Region("app.z.ewmgt.profile", null, null, null))
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }

            override fun didDetermineStateForRegion(state: Int, region: Region?) {}
            override fun didExitRegion(region: Region?) {}
        })
        try {
            beaconManager.startMonitoringBeaconsInRegion(Region("app.z.ewmgt.profile", null, null, null))
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun setProfilePicture(profilePicture : String?){
        profilepict_ap?.fromUrl(profilePicture.toProfileUrl(), R.drawable.ic_empty_profile)
    }

    fun bindBeacon(){
        if (!bluetoothState.isEnabled())
            bluetoothState.enable()
        Beacons.bind(beaconManager,this)
    }

    fun unbindBeacon(){
        if (bluetoothState.isEnabled())
            bluetoothState.disable()
        Beacons.unbind(beaconManager,this)
    }

    fun onGetProvinces(provinces : List<String>, position : Int){
        spinner_province_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,provinces)
        spinner_province_ap?.setSelection(position)
        spinner_city_ap?.gone()
        spinner_district_ap?.gone()
        spinner_sub_district_ap?.gone()
        spinner_postal_ap?.gone()
    }

    fun onGetCities(cities : List<String>, position : Int){
        spinner_city_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cities)
        spinner_city_ap?.setSelection(position)
        spinner_city_ap?.visible()
        spinner_district_ap?.gone()
        spinner_sub_district_ap?.gone()
        spinner_postal_ap?.gone()
    }

    fun onGetDistricts(districts : List<String>, position : Int){
        spinner_district_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,districts)
        spinner_district_ap?.setSelection(position)
        spinner_district_ap?.visible()
        spinner_sub_district_ap?.gone()
        spinner_postal_ap?.gone()
    }

    fun onGetSubDistricts(subDistricts : List<String>, position : Int){
        spinner_sub_district_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,subDistricts)
        spinner_sub_district_ap?.setSelection(position)
        spinner_sub_district_ap?.visible()
        spinner_postal_ap?.gone()
    }

    fun onGetPostals(postals : List<String>, position : Int){
        spinner_postal_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,postals)
        spinner_postal_ap?.setSelection(position)
        spinner_postal_ap?.visible()
    }

    fun onGetWilayahs(wilayahs : List<String>, position: Int){
        spinner_wilayah_ap?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,wilayahs)
        spinner_wilayah_ap?.setSelection(position)
    }

    override fun onSuccessGettingImage(filePath: String?) {
        profilepict_ap?.fromFilePath(filePath,R.drawable.ic_empty_profile)
        viewModel.profilePicture.set(filePath)
    }

    override fun onFailedGettingImage(error: String?) {
        log(error)
    }
}
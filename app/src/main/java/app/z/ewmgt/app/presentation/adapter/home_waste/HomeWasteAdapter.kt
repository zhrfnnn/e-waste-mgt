package app.z.ewmgt.app.presentation.adapter.home_waste

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.data.util.isNotNull
import app.z.data.util.toDefaultFormatString
import app.z.domain.model.tablemodel.HomeWasteTableModel
import app.z.ewmgt.app.presentation.adapter.ObservableRecyclerViewAdapter
import app.z.ewmgt.app.presentation.adapter.home_waste.HomeWasteAdapter.Holder
import app.z.ewmgt.databinding.ItemHomeWasteBinding

/**
 * Created by Zharfan on 16/01/2021
 * */

class HomeWasteAdapter(homeWasteList: ObservableList<HomeWasteTableModel>)
    : ObservableRecyclerViewAdapter<HomeWasteTableModel,Holder>(homeWasteList){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemHomeWasteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(private val binding: ItemHomeWasteBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(waste : HomeWasteTableModel) {
            binding.dateIhw.text = waste.date.toDefaultFormatString()
            binding.wetWasteIhw.text = if (waste.wetWaste.isNotNull()) waste.wetWaste.toString() else "0"
            binding.dryWasteIhw.text = if (waste.dryWaste.isNotNull()) waste.dryWaste.toString() else "0"
            binding.totalIhw.text = waste.total.toString()
        }
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.data.util.toWasteUrl
import app.z.domain.model.tablemodel.DryWasteCollectionTableModel
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.fromUrl
import app.z.ewmgt.databinding.ActivityConfirmDryWasteCollectionBinding
import com.google.gson.Gson
import javax.inject.Inject

/**
 * Created by Zharfan on 25/01/2021
 * */

class ConfirmDryWasteCollectionActivity : BaseActivity() {

    companion object {
        const val DRY_WASTE = "DRY_WASTE"
    }

    @Inject
    lateinit var viewModel: ConfirmDryWasteCollectionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityConfirmDryWasteCollectionBinding>(this,R.layout.activity_confirm_dry_waste_collection)
        screenComponent.inject(this)
        binding?.viewModel = viewModel

        val selectedDryWaste = (Gson().fromJson(intent.getStringExtra(DRY_WASTE),DryWasteCollectionTableModel::class.java))
        viewModel.selectedDryWaste.set(selectedDryWaste)
        viewModel.bound()

        binding?.dryPhotoAcdwc?.fromUrl(selectedDryWaste.image.toWasteUrl(),R.drawable.ic_wastebank_noimage)
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
package app.z.ewmgt.app.presentation.menu.home_waste.home_waste

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import app.z.data.util.*
import app.z.domain.model.tablemodel.HomeWasteTableModel
import app.z.domain.usecase.historywaste.HistoryHomeWasteUseCase
import app.z.domain.usecase.requesthw.RequestCollectionWasteUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
* Created by Zharfan on 19/12/2020
* */

class HomeWasteViewModel @Inject constructor(private val context: Context,
                                             private val homeWasteRouter: HomeWasteRouter,
                                             private val historyHomeWasteUseCase: HistoryHomeWasteUseCase,
                                             private val collectionWasteUseCase: RequestCollectionWasteUseCase) : BaseViewModel(homeWasteRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val todayHistory = ObservableArrayList<HomeWasteTableModel>()
    val pastHistory = ObservableArrayList<HomeWasteTableModel>()

    val enableWetWaste = ObservableBoolean()
    val enableDryWaste = ObservableBoolean()

    val entryTitle = ObservableField<String>()

    val wetWeight = ObservableField<String>()
    val wetPhoto = ObservableField<String>()
    val dryWeight = ObservableField<String>()
    val dryPhoto = ObservableField<String>()

    override fun bound() {
        super.bound()
        getTodayList()
        getPastList()
    }

    private fun handleGetTodayHistory(result : HistoryHomeWasteUseCase.Result){
        when(result){
            is HistoryHomeWasteUseCase.Result.Success -> {
                todayHistory.clear()
                todayHistory.addAll(result.homeWasteList)
            }
            is HistoryHomeWasteUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        homeWasteRouter.hideLoading()
    }

    private fun handleGetPastHistory(result : HistoryHomeWasteUseCase.Result){
        when(result){
            is HistoryHomeWasteUseCase.Result.Success -> {
                pastHistory.clear()
                if (result.homeWasteList.size > 10){
                    var count = 0
                    while (count < 10){
                        pastHistory.add(result.homeWasteList[result.homeWasteList.size-count-1])
                        count++
                    }
                } else {
                    pastHistory.addAll(result.homeWasteList)
                }
            }
            is HistoryHomeWasteUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        homeWasteRouter.hideLoading()
    }

    fun getTodayList(){
        homeWasteRouter.showLoading()
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        entryTitle.set("Entry Home Waste ( ${calendar.time.toDefaultFormatString()} )")
        val startDate = calendar.time.toServerFormatString()
        calendar.add(Calendar.DAY_OF_YEAR,1)
        val endDate = calendar.time.toServerFormatString()

        historyHomeWasteUseCase.execute(Session.currentUser!!,startDate,endDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ handleGetTodayHistory(it) }
            .addTo(disposables)
    }

    fun getPastList(){
        homeWasteRouter.showLoading()
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.DAY_OF_YEAR,-1)
        val endDate = calendar.time.toServerFormatString()
        calendar.add(Calendar.DAY_OF_YEAR,-10)
        val startDate = calendar.time.toServerFormatString()

        historyHomeWasteUseCase.execute(Session.currentUser!!,startDate,endDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ handleGetPastHistory(it) }
            .addTo(disposables)
    }

    fun moreSearch(){
        homeWasteRouter.search()
    }

    fun onSubmit(){
        if (enableWetWaste.get() || enableDryWaste.get()){

            var wetWeightFinal : Double? = null
            var dryWeightFinal : Double? = null

            var wetPhotoFinal : String? = null
            var dryPhotoFinal : String? = null

            var wetDone = false
            var dryDone = false

            if (enableWetWaste.get()){
                if (!wetWeight.get().isNullOrBlank()){
                    wetWeightFinal = wetWeight.get()!!.toDouble()
                } else {
                    wetWeightFinal = 10.toDouble()
                }
                if (!wetPhoto.get().isNullOrBlank()){
                    wetPhotoFinal = wetPhoto.get()
                    wetDone = true
                }
            } else {
                wetDone = true
            }
            if (enableDryWaste.get()){
                if (!dryWeight.get().isNullOrBlank()){
                    dryWeightFinal = dryWeight.get()!!.toDouble()
                } else {
                    dryWeightFinal = 10.toDouble()
                }
                if (!dryPhoto.get().isNullOrBlank()){
                    dryPhotoFinal = dryPhoto.get()
                    dryDone = true
                }
            } else {
                dryDone = true
            }

            if (wetDone && dryDone){
                if (Session.currentUser.isNotNull()){
                    homeWasteRouter.showLoading()
                    collectionWasteUseCase.execute(
                        user = Session.currentUser!!,
                        date = Date().toServerFormatString(),
                        wetWeight = wetWeightFinal,
                        wetPhoto = wetPhotoFinal,
                        dryWeight = dryWeightFinal,
                        dryPhoto = dryPhotoFinal,
                        dry = if (!dryPhotoFinal.isNullOrBlank()) { "P11" } else { null })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            when(it){
                                is RequestCollectionWasteUseCase.Result.Success -> {
                                    if (it.result){
                                        context.toast("Successfully submitted")
                                        homeWasteRouter.onBack()
                                    }
                                }
                                is RequestCollectionWasteUseCase.Result.Failure -> {
                                    errorString.set(it.throwable.localizedMessage)
                                    showError.trigger(true)
                                }
                            }
                            homeWasteRouter.hideLoading()
                        }
                        .addTo(disposables)
                }
            } else {
                context.toast("Please take a picture of waste")
            }

        } else {
            context.toast("You must check one of the waste to submit")
        }
    }
}
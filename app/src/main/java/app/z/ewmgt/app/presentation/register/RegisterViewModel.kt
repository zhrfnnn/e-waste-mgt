package app.z.ewmgt.app.presentation.register

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.model.UserType
import app.z.domain.model.Wilayah
import app.z.domain.usecase.location.LocationUseCase
import app.z.domain.usecase.register.RegisterUseCase
import app.z.domain.usecase.usertype.UserTypeUseCase
import app.z.domain.usecase.wilayah.WilayahUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
* Created by Zharfan on 17/11/2020
* */

class RegisterViewModel @Inject constructor(private val registerRouter: RegisterRouter,
                                            private val registerUseCase: RegisterUseCase,
                                            private val locationUseCase: LocationUseCase,
                                            private val userTypeUseCase: UserTypeUseCase,
                                            private val wilayahUseCase: WilayahUseCase,
                                            private val context: Context) : BaseViewModel(registerRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()
    val progressVisible = ObservableBoolean()

    val provinces = ObservableArrayList<String>()
    val provincePosition = MutableLiveData<Int>()
    private val selectedProvince : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(provincePosition) {
            value = provinces[it]
        }
    }
    val cities = ObservableArrayList<String>()
    val cityPosition = MutableLiveData<Int>()
    private val selectedCity : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(cityPosition) {
            value = cities[it]
        }
    }
    val districts = ObservableArrayList<String>()
    val districtPosition = MutableLiveData<Int>()
    private val selectedDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(districtPosition) {
            value = districts[it]
        }
    }
    val subDistricts = ObservableArrayList<String>()
    val subDistrictPosition = MutableLiveData<Int>()
    private val selectedSubDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(subDistrictPosition) {
            value = subDistricts[it]
        }
    }
    val postals = ObservableArrayList<String>()
    val postalPosition = MutableLiveData<Int>()
    private val selectedPostal : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(postalPosition) {
            value = postals[it]
        }
    }

    val userTypes = ObservableArrayList<UserType>()
    val userTypePosition = MutableLiveData<Int>()
    private val selectedUserType : LiveData<UserType> = MediatorLiveData<UserType>().apply {
        addSource(userTypePosition) {
            value = userTypes[it]
        }
    }

    val wilayahs = ObservableArrayList<Wilayah>()
    val wilayahPosition = MutableLiveData<Int>()
    private val selectedWilayah : LiveData<Wilayah> = MediatorLiveData<Wilayah>().apply {
        addSource(wilayahPosition) {
            value = wilayahs[it]
        }
    }

    val referralEmailVisible = ObservableBoolean()

    val fullName = ObservableField<String>()
    val address = ObservableField<String>()
    val nik = ObservableField<String>()
    val email = ObservableField<String>()
    val referralEmail = ObservableField<String>()
    val mobileNumber = ObservableField<String>()
    val password = ObservableField<String>()
    val confirmPassword = ObservableField<String>()
    val nikPicture = ObservableField<String>()

    override fun bound() {
        super.bound()
        registerRouter.showLoading()
        userTypeUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetUserType(it) }
            .addTo(disposables)

        wilayahUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetWilayah(it) }
            .addTo(disposables)

        locationUseCase.executeProvince()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetProvince(it) }
            .addTo(disposables)

        selectedProvince.observeForever { province ->
            if (province.isNotBlank() && !province.equals("=== Please Select ==="))
                registerRouter.showLoading()
                locationUseCase.executeCity(province)
                    .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleGetCity(it) }
                   .addTo(disposables)

        }

        selectedCity.observeForever {city ->
            if (city.isNotBlank() && !city.equals("=== Please Select ==="))
                registerRouter.showLoading()
                locationUseCase.executeDistrict(city)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleGetDistrict(it) }
                    .addTo(disposables)
        }

        selectedDistrict.observeForever {district ->
            if (district.isNotBlank() && !district.equals("=== Please Select ==="))
                registerRouter.showLoading()
                locationUseCase.executeSubDistrict(district)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleGetSubDistrict(it) }
                    .addTo(disposables)
        }

        selectedSubDistrict.observeForever {subDistrict ->
            if (subDistrict.isNotBlank() && !subDistrict.equals("=== Please Select ==="))
                registerRouter.showLoading()
                locationUseCase.executePostal(subDistrict)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleGetPostal(it) }
                    .addTo(disposables)
        }

        selectedPostal.observeForever {  }

        selectedWilayah.observeForever {  }

        selectedUserType.observeForever {
            referralEmailVisible.set(!it.sameAs(UserType.HOME_WASTE))
        }
    }

    private fun handleGetUserType(result : UserTypeUseCase.Result){
        when (result){
            is UserTypeUseCase.Result.Success -> {
                userTypes.clear()
                userTypes.addAll(result.userTypes)
                val stringUserTypes = arrayListOf<String>()
                result.userTypes.forEach {
                    if (it.name.isNotNull())
                        stringUserTypes.add(it.name!!)
                }
                registerRouter.onGetUserTypes(stringUserTypes.toList())
            }
            is UserTypeUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetWilayah(result : WilayahUseCase.Result){
        when (result){
            is WilayahUseCase.Result.Success -> {
                wilayahs.clear()
                wilayahs.addAll(result.wilayahs)
                val stringWilayahs = arrayListOf<String>()
                result.wilayahs.forEach {
                    if (it.wilayah.isNotNull())
                        stringWilayahs.add(it.wilayah!!)
                }
                registerRouter.onGetWilayahs(stringWilayahs.toList())
            }
            is WilayahUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetProvince(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                provinces.clear()
                provinces.add("=== Please Select ===")
                result.locations.forEach {
                    provinces.add(it.province)
                }
                registerRouter.onGetProvinces(provinces)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        registerRouter.hideLoading()
    }

    private fun handleGetCity(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                cities.clear()
                cities.add("=== Please Select ===")
                result.locations.forEach {
                    cities.add(it.city)
                }
                registerRouter.onGetCities(cities)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        registerRouter.hideLoading()
    }

    private fun handleGetDistrict(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                districts.clear()
                districts.add("=== Please Select ===")
                result.locations.forEach {
                    districts.add(it.district)
                }
                registerRouter.onGetDistricts(districts)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        registerRouter.hideLoading()
    }

    private fun handleGetSubDistrict(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                subDistricts.clear()
                subDistricts.add("=== Please Select ===")
                result.locations.forEach {
                    subDistricts.add(it.subDistrict)
                }
                registerRouter.onGetSubDistricts(subDistricts)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        registerRouter.hideLoading()
    }

    private fun handleGetPostal(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                postals.clear()
                postals.add("=== Please Select ===")
                result.locations.forEach {
                    postals.add(it.postal)
                }
                registerRouter.onGetPostals(postals)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        registerRouter.hideLoading()
    }

    fun onSubmit(){
        if (!selectedProvince.value.equals("=== Please Select ===") &&
            !selectedCity.value.equals("=== Please Select ===") &&
            !selectedDistrict.value.equals("=== Please Select ===") &&
            !selectedSubDistrict.value.equals("=== Please Select ===") &&
            !selectedPostal.value.equals("=== Please Select ===") &&
            !nikPicture.get().isNullOrBlank()){

            val user = User(0,position = selectedUserType.value,wilayah = selectedWilayah.value)
            user.fullname = fullName.get()
            user.address = address.get()
            user.province = selectedProvince.value
            user.city = selectedCity.value
            user.district = selectedDistrict.value
            user.subDistrict = selectedSubDistrict.value
            user.postalCode = selectedPostal.value
            user.nik = nik.get()
            if (!nikPicture.get().isNullOrBlank()){
                user.nikPicture = nikPicture.get()
            }
            user.email = email.get()
            user.mobileNumber = mobileNumber.get()

            if (!referralEmail.get().isNullOrBlank()){
                user.referralEmail = referralEmail.get()
            }
            if (!password.get().isNullOrBlank() && !confirmPassword.get().isNullOrBlank()){
                if (password.get().equals(confirmPassword.get())){
                    user.password = password.get()

                    registerRouter.showLoading()
                    registerUseCase.execute(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            when(it){
                                is RegisterUseCase.Result.Success -> {
                                    registerRouter.navigate(RegisterRouter.Route.LOGIN)
                                }
                                is RegisterUseCase.Result.Failure -> {
                                    errorString.set(it.throwable.localizedMessage)
                                    showError.trigger(true)
                                }
                            }
                            registerRouter.hideLoading()
                        }
                        .addTo(disposables)
                } else {
                    context.toast("Password must match")
                }
            }
        } else {
            context.toast("Please fill correctly")
        }
    }
}
package app.z.ewmgt.app.presentation.adapter.home_waste

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.domain.model.User
import app.z.ewmgt.app.presentation.adapter.ObservableRecyclerViewAdapter
import app.z.ewmgt.databinding.ItemWasteBankRegistrationBinding

/**
 * Created by Zharfan on 16/01/2021
 * */

class WasteBankRegistrationAdapter(val wasteBanks: ObservableList<User>)
    : ObservableRecyclerViewAdapter<User, WasteBankRegistrationAdapter.Holder>(wasteBanks) {

    lateinit var onWasteBankSelected: (wasteBank: User) -> Unit

    private var lastCheckedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemWasteBankRegistrationBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position),position)
    }

    inner class Holder(private val binding: ItemWasteBankRegistrationBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val copy = lastCheckedPosition
                lastCheckedPosition = adapterPosition
                notifyItemChanged(copy)
                notifyItemChanged(lastCheckedPosition)
                onWasteBankSelected.invoke(wasteBanks[lastCheckedPosition])
            }
        }
        fun bind(wasteBank : User, position: Int){
            binding.nameIwbr.text = wasteBank.fullname
            binding.addressIwbr.text = "${wasteBank.address} " +
                    "${wasteBank.province} " +
                    "${wasteBank.city} " +
                    "${wasteBank.district} " +
                    "${wasteBank.subDistrict} " +
                    "${wasteBank.postalCode} "
            binding.radioIwbr.isChecked = position == lastCheckedPosition
        }
    }
}
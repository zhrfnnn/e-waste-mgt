package app.z.ewmgt.app.permission

import app.z.ewmgt.app.presentation.BaseActivity

/**
* Created by Zharfan on 20/11/2020
* */

interface PermissionHandler {
    fun checkHasPermission(activity : BaseActivity, permission : String) : Boolean
    fun requestPermission(activity: BaseActivity, permissions : Array<String>, requestCode : Int)
}
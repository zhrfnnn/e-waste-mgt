package app.z.ewmgt.app.customviews

import android.content.Context
import android.widget.ImageView

/**
* Created by Zharfan on 13/07/2019
* */

class SquareImageView(context: Context) : androidx.appcompat.widget.AppCompatImageView(context) {

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measuredWidth = measuredWidth
        val measuredHeight = measuredHeight
        if (measuredWidth > measuredHeight) {
            setMeasuredDimension(measuredHeight, measuredHeight)
        } else {
            setMeasuredDimension(measuredWidth, measuredWidth)
        }
    }
}
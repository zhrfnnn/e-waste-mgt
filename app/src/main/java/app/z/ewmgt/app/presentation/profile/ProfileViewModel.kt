package app.z.ewmgt.app.presentation.profile

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.model.Wilayah
import app.z.domain.usecase.location.LocationUseCase
import app.z.domain.usecase.photoprofile.PhotoProfileUseCase
import app.z.domain.usecase.profile.ProfileUseCase
import app.z.domain.usecase.updateprofile.UpdateProfileUseCase
import app.z.domain.usecase.wilayah.WilayahUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
* Created by Zharfan on 17/12/2020
* */

class ProfileViewModel @Inject constructor(private val context: Context,
                                           private val profileRouter: ProfileRouter,
                                           private val profileUseCase: ProfileUseCase,
                                           private val photoProfileUseCase: PhotoProfileUseCase,
                                           private val locationUseCase: LocationUseCase,
                                           private val wilayahUseCase: WilayahUseCase,
                                           private val updateProfileUseCase: UpdateProfileUseCase) : BaseViewModel(profileRouter) {

    val showError = StickyAction<Boolean>()
    val errorString = ObservableField<String>()

    val id = ObservableField<String>()
    val fullName = ObservableField<String>()
    val address = ObservableField<String>()

    val provinces = ObservableArrayList<String>()
    val provincePosition = MutableLiveData<Int>()
    private val selectedProvince : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(provincePosition) {
            value = provinces[it]
        }
    }
    val cities = ObservableArrayList<String>()
    val cityPosition = MutableLiveData<Int>()
    private val selectedCity : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(cityPosition) {
            value = cities[it]
        }
    }
    val districts = ObservableArrayList<String>()
    val districtPosition = MutableLiveData<Int>()
    private val selectedDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(districtPosition) {
            value = districts[it]
        }
    }
    val subDistricts = ObservableArrayList<String>()
    val subDistrictPosition = MutableLiveData<Int>()
    private val selectedSubDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(subDistrictPosition) {
            value = subDistricts[it]
        }
    }
    val postals = ObservableArrayList<String>()
    val postalPosition = MutableLiveData<Int>()
    private val selectedPostal : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(postalPosition) {
            value = postals[it]
        }
    }

    val wilayahs = ObservableArrayList<Wilayah>()
    val wilayahPosition = MutableLiveData<Int>()
    private val selectedWilayah : LiveData<Wilayah> = MediatorLiveData<Wilayah>().apply {
        addSource(wilayahPosition){
            value = wilayahs[it]
        }
    }

    val nik = ObservableField<String>()
    val email = ObservableField<String>()
    val profilePicture = ObservableField<String>()
    val referralEmail = ObservableField<String>()
    val mobileNumber = ObservableField<String>()
    val password = ObservableField<String>()
    val beaconId = ObservableField<String>()

    var tempUser : User? = null

    override fun bound() {
        super.bound()
        if (Session.currentUser.isNotNull()){
            profileRouter.showLoading()
            profileUseCase.execute(Session.currentUser!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetProfile(it) }
                .addTo(disposables)

            selectedProvince.observeForever { province ->
                if (province.isNotBlank() && !province.equals("=== Please Select ===")) {
                    locationUseCase.executeCity(province)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleGetCity(it,Session.currentUser?.city) }
                        .addTo(disposables)
                }

            }

            selectedCity.observeForever {city ->
                if (city.isNotBlank() && !city.equals("=== Please Select ==="))
                    locationUseCase.executeDistrict(city)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleGetDistrict(it,Session.currentUser?.district) }
                        .addTo(disposables)
            }

            selectedDistrict.observeForever {district ->
                if (district.isNotBlank() && !district.equals("=== Please Select ==="))
                    locationUseCase.executeSubDistrict(district)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleGetSubDistrict(it,Session.currentUser?.subDistrict) }
                        .addTo(disposables)
            }

            selectedSubDistrict.observeForever {subDistrict ->
                if (subDistrict.isNotBlank() && !subDistrict.equals("=== Please Select ==="))
                    locationUseCase.executePostal(subDistrict)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleGetPostal(it,Session.currentUser?.postalCode) }
                        .addTo(disposables)
            }

            selectedPostal.observeForever {  }

            selectedWilayah.observeForever {  }
        }
    }

    private fun handleGetProfile(result : ProfileUseCase.Result){
        when(result){
            is ProfileUseCase.Result.Success -> {
                Session.currentUser = result.user
                implementUser(Session.currentUser)
                tempUser = Session.currentUser
            }
            is ProfileUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetProvince(result : LocationUseCase.Result, current : String?){
        when (result){
            is LocationUseCase.Result.Success -> {
                provinces.clear()
                provinces.add("=== Please Select ===")
                var position = 0
                for (i in result.locations.indices){
                    provinces.add(result.locations[i].province)
                    if (result.locations[i].province.equals(current)){
                        position = i+1
                    }
                }
                profileRouter.onGetProvinces(provinces,position)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetCity(result : LocationUseCase.Result, current : String?){
        when (result){
            is LocationUseCase.Result.Success -> {
                cities.clear()
                cities.add("=== Please Select ===")
                var position = 0
                for (i in result.locations.indices){
                    cities.add(result.locations[i].city)
                    if (result.locations[i].city.equals(current)){
                        position = i+1
                    }
                }
                profileRouter.onGetCities(cities,position)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetDistrict(result : LocationUseCase.Result, current : String?){
        when (result){
            is LocationUseCase.Result.Success -> {
                districts.clear()
                districts.add("=== Please Select ===")
                var position = 0
                for (i in result.locations.indices){
                    districts.add(result.locations[i].district)
                    if (result.locations[i].district.equals(current)){
                        position = i+1
                    }
                }
                profileRouter.onGetDistricts(districts,position)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetSubDistrict(result : LocationUseCase.Result, current : String?){
        when (result){
            is LocationUseCase.Result.Success -> {
                subDistricts.clear()
                subDistricts.add("=== Please Select ===")
                var position = 0
                for (i in result.locations.indices){
                    subDistricts.add(result.locations[i].subDistrict)
                    if (result.locations[i].subDistrict.equals(current)){
                        position = i+1
                    }
                }
                profileRouter.onGetSubDistricts(subDistricts,position)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleGetPostal(result : LocationUseCase.Result, current : String?){
        when (result){
            is LocationUseCase.Result.Success -> {
                postals.clear()
                postals.add("=== Please Select ===")
                var position = 0
                for (i in result.locations.indices){
                    postals.add(result.locations[i].postal)
                    if (result.locations[i].postal.equals(current)){
                        position = i+1
                    }
                }
                profileRouter.onGetPostals(postals,position)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        profileRouter.hideLoading()
    }

    private fun handleGetWilayah(result : WilayahUseCase.Result, current: String?){
        when (result){
            is WilayahUseCase.Result.Success -> {
                wilayahs.clear()
                wilayahs.addAll(result.wilayahs)
                val stringWilayahs = arrayListOf<String>()
                var position = 0
                for (i in result.wilayahs.indices){
                    if (result.wilayahs[i].wilayah.isNotNull()){
                        stringWilayahs.add(result.wilayahs[i].wilayah!!)
                        if (result.wilayahs[i].wilayah.equals(current)){
                            position = i
                        }
                    }
                }
                profileRouter.onGetWilayahs(stringWilayahs.toList(),position)
            }
            is WilayahUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun implementUser(user : User?){

        id.set("User-${user?.id}")

        profileRouter.setProfilePicture(user?.picture)
        fullName.set(user?.fullname)
        address.set(user?.address)

        locationUseCase.executeProvince()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetProvince(it,user?.province) }
            .addTo(disposables)

        wilayahUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetWilayah(it,user?.wilayahName) }
            .addTo(disposables)

        nik.set(user?.nik)
        email.set(user?.email)
        referralEmail.set(user?.referralEmail)
        mobileNumber.set(user?.mobileNumber)
        password.set(user?.password)

        if (user?.beaconId.isNullOrBlank()){
            profileRouter.searchBeacon()
        } else {
            beaconId.set(user?.beaconId)
        }
    }

    fun save(){
        if (!selectedProvince.value.equals("=== Please Select ===") &&
            !selectedCity.value.equals("=== Please Select ===") &&
            !selectedDistrict.value.equals("=== Please Select ===") &&
            !selectedSubDistrict.value.equals("=== Please Select ===") &&
            !selectedPostal.value.equals("=== Please Select ===")){

            tempUser?.fullname = fullName.get()
            tempUser?.email = email.get()
            tempUser?.mobileNumber = mobileNumber.get()
            tempUser?.address = address.get()
            tempUser?.wilayah = selectedWilayah.value
            tempUser?.province = selectedProvince.value
            tempUser?.city = selectedCity.value
            tempUser?.district = selectedDistrict.value
            tempUser?.subDistrict = selectedSubDistrict.value
            tempUser?.postalCode = selectedPostal.value
            tempUser?.beaconId = beaconId.get()

            if (tempUser.isNotNull()){
                profileRouter.showLoading()
                updateProfileUseCase.execute(tempUser!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        when(it){
                            is UpdateProfileUseCase.Result.Success -> {
                                if (!profilePicture.get().isNullOrBlank()){
                                    tempUser?.picture = profilePicture.get()

                                    profileRouter.showLoading()
                                    photoProfileUseCase.execute(tempUser!!)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe{
                                            when(it){
                                                is PhotoProfileUseCase.Result.Success -> {
                                                    profileRouter.recreate()
                                                }
                                                is PhotoProfileUseCase.Result.Failure -> {
                                                    errorString.set(it.throwable.localizedMessage)
                                                    showError.trigger(true)
                                                }
                                            }
                                            profileRouter.hideLoading()
                                        }
                                        .addTo(disposables)
                                } else {
                                    profileRouter.recreate()
                                }
                            }
                            is UpdateProfileUseCase.Result.Failure -> {
                                errorString.set(it.throwable.localizedMessage)
                                showError.trigger(true)
                            }
                        }
                        profileRouter.hideLoading()
                    }
                    .addTo(disposables)
            }

        } else {
            context.toast("Please fill correctly")
        }
     }
}
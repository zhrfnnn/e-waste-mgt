package app.z.ewmgt.app.customviews

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import app.z.ewmgt.R
import kotlinx.android.synthetic.main.custom_range_date_picker_layout.view.*
import java.util.*

/**
* Created by Zharfan on 04/08/2020
* */

class CustomRangeDatePicker : LinearLayout {
    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private var dateFrom : Date? = null
    private var dateTo : Date? = null

    private var listener : CustomRangeDatePickerListener? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_range_date_picker_layout, this, true)
        orientation = HORIZONTAL

        fromDate?.setListener(object : CustomDatePicker.CustomDatePickerListener{
            override fun onDatePicked(date: Date) {
                dateFrom = date
                toDate?.setMinDate(date.time)
                listener?.onDatesPicked(date,getDateTo()!!)
            }
        })

        toDate?.setListener(object : CustomDatePicker.CustomDatePickerListener{
            override fun onDatePicked(date: Date) {
                dateTo = date
                fromDate?.setMaxDate(date.time)
                listener?.onDatesPicked(getDateFrom()!!,date)
            }
        })
    }

    fun setDateFrom (dateFrom : Date?) {
        fromDate?.setDate(dateFrom)
        this.dateFrom = dateFrom
    }

    fun getDateFrom () : Date? {
        dateFrom = fromDate?.getDate()
        return dateFrom
    }

    fun setDateTo (dateTo : Date?) {
        toDate?.setDate(dateTo)
        toDate?.setMinDate(this.dateFrom?.time)
        this.dateTo = dateTo
    }

    fun getDateTo () : Date? {
        dateTo = toDate?.getDate()
        return dateTo
    }

    fun setListener (listener : CustomRangeDatePickerListener?) {
        this.listener = listener
    }

    interface CustomRangeDatePickerListener {
        fun onDatesPicked(fromDate : Date, toDate : Date)
    }
}
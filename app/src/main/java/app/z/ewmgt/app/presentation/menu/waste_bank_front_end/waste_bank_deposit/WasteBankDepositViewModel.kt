package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableField
import app.z.data.util.log
import app.z.domain.model.User
import app.z.domain.model.tablemodel.DryWasteCollectionTableModel
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm.ConfirmDryWasteCollectionActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm.ConfirmWasteBankDepositActivity
import app.z.ewmgt.app.rx.StickyAction
import com.google.gson.Gson
import javax.inject.Inject

/**
 * Created by Zharfan on 24/01/2021
 * */

class WasteBankDepositViewModel @Inject constructor(private val context: Context,
                                                    private val wasteBankDepositRouter: WasteBankDepositRouter) : BaseViewModel(wasteBankDepositRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    fun onSelectedCustomer(customer : User?){
        wasteBankDepositRouter.onSelectedUser(Bundle().apply {
            putString(ConfirmWasteBankDepositActivity.SELECTED_USER, Gson().toJson(customer))
        })
    }

    fun onSearchCustomer(){
        wasteBankDepositRouter.searchCustomer()
    }
}
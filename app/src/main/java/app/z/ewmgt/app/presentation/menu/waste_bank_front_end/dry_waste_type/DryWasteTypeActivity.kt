package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityDryWasteTypeBinding
import javax.inject.Inject

/**
 * Created by Zharfan on 27/01/2021
 * */

class DryWasteTypeActivity : BaseActivity() {

    companion object {
        const val DRY_WASTE_TYPE = 112

        const val EXTRA_DRY_WASTE_TYPE = "EXTRA_DRY_WASTE_TYPE"
    }

    @Inject
    lateinit var viewModel: DryWasteTypeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityDryWasteTypeBinding>(this,R.layout.activity_dry_waste_type)
        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
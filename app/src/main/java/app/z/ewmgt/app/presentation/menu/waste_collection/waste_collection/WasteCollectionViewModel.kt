package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.*
import app.z.domain.model.tablemodel.WasteCollectionTableModel
import app.z.domain.usecase.schedule.ScheduleUseCase
import app.z.domain.usecase.updatestatus.UpdateWasteStatusUseCase
import app.z.ewmgt.R
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import app.z.ewmgt.app.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
* Created by Zharfan on 15/01/2021
* */

class WasteCollectionViewModel @Inject constructor(private val context: Context,
                                                   private val scheduleUseCase: ScheduleUseCase,
                                                   private val updateWasteStatusUseCase: UpdateWasteStatusUseCase,
                                                   private val wasteCollectionRouter: WasteCollectionRouter) : BaseViewModel(wasteCollectionRouter){
    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val title = ObservableField<String>()
    val wasteCollections = ObservableArrayList<WasteCollectionTableModel>()

    override fun bound() {
        super.bound()

        title.set("${Strings.get(R.string.truck_schedule_coll)} ${Date().toDefaultFormatString()}")

        if (Session.currentUser.isNotNull()){
            scheduleUseCase.execute(Session.currentUser!!,Date().toServerFormatString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetSchedules(it) }
                .addTo(disposables)
        }
    }

    private fun handleGetSchedules(result : ScheduleUseCase.Result){
        when(result){
            is ScheduleUseCase.Result.Success -> {
                wasteCollections.clear()
                wasteCollections.addAll(result.result)
                wasteCollectionRouter.searchBeacon()
            }
            is ScheduleUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun handleUpdateWasteStatus(result : UpdateWasteStatusUseCase.Result){
        when(result){
            is UpdateWasteStatusUseCase.Result.Success -> {
                scheduleUseCase.execute(Session.currentUser!!,Date().toServerFormatString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleGetSchedules(it) }
                    .addTo(disposables)
            }
            is UpdateWasteStatusUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        wasteCollectionRouter.unbindBeacon()
    }

    fun updateWasteStatus(beaconIdHome : String){
        wasteCollections.forEach {
            if (!it.isDone()){
                if (it.homeId.equals(beaconIdHome)){
                    updateWasteStatusUseCase.execute(user = Session.currentUser!!, wasteId = it.id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { handleUpdateWasteStatus(it) }
                        .addTo(disposables)
                }
            }
        }
    }

    fun moreSearch(){
        wasteCollectionRouter.search()
    }

    fun onFinish(){
        context.toast("Finished collecting waste")
        wasteCollectionRouter.onBack()
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.data.util.log
import app.z.domain.model.User
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerActivity
import app.z.ewmgt.databinding.ActivityWasteBankDepositBinding
import com.google.gson.Gson
import javax.inject.Inject

/**
* Created by Zharfan on 24/01/2021
* */

class WasteBankDepositActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: WasteBankDepositViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityWasteBankDepositBinding>(this,R.layout.activity_waste_bank_deposit)
        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            SearchCustomerActivity.SEARCH_CUSTOMER -> {
                if (resultCode == Activity.RESULT_OK){
                    viewModel.onSelectedCustomer(Gson().fromJson(data?.getStringExtra(SearchCustomerActivity.EXTRA_CUSTOMER),User::class.java))
                }
            }
        }
    }
}
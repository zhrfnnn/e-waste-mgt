package app.z.ewmgt.app.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

/**
* Created by Zharfan on 22/12/2020
* */

class TableView : LinearLayout {

    constructor(context : Context) : super(context)
    constructor(context : Context, attributeSet: AttributeSet) : super(context,attributeSet)
    constructor(context : Context, attributeSet: AttributeSet, defStyle : Int) : super(context,attributeSet,defStyle)
}
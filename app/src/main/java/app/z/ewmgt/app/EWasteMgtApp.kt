package app.z.ewmgt.app

import android.app.Application
import app.z.data.util.Session
import app.z.ewmgt.app.di.application.ApplicationComponent
import app.z.ewmgt.app.di.application.ApplicationModule
import app.z.ewmgt.app.di.application.DaggerApplicationComponent

/**
* Created by Zharfan on 09/11/2020
* */

class EWasteMgtApp : Application() {

    companion object {
        lateinit var instance : EWasteMgtApp private set
    }

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        Session.init(this)
        instance = this
        inject()
    }

    fun inject(){
        component = DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(this)).build()
        component.inject(this)
    }
}
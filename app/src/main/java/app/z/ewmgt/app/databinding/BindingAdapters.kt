package app.z.ewmgt.app.databinding

import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import androidx.databinding.ObservableField
import com.squareup.picasso.Picasso


/**
* Created by Zharfan on 09/11/2020
* */

@BindingConversion
fun setVisibility(state: Boolean): Int {
  return if (state) View.VISIBLE else View.GONE
}
@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
  if (url != null) {
    Picasso.get().load(url).into(imageView)
  }
}

package app.z.ewmgt.app.presentation.register

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import app.z.data.util.gone
import app.z.data.util.log
import app.z.data.util.visible
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.*
import app.z.ewmgt.databinding.ActivityRegisterBinding
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

/**
* Created by Zharfan on 16/11/2020
* */

class RegisterActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()

        camera_nik_ar?.setOnClickListener {
            getImage(Companion.ImageRatio.IMAGE_4_3)
        }
    }

    fun onGetUserTypes(userTypes : List<String>){
        spinner_position_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,userTypes)
    }

    fun onGetWilayahs(wilayahs : List<String>){
        spinner_wilayah_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,wilayahs)
    }

    fun onGetProvinces(provinces : List<String>){
        spinner_province_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,provinces)
        spinner_city_ar?.gone()
        spinner_district_ar?.gone()
        spinner_sub_district_ar?.gone()
        spinner_postal_ar?.gone()
    }

    fun onGetCities(cities : List<String>){
        spinner_city_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cities)
        spinner_city_ar?.visible()
        spinner_district_ar?.gone()
        spinner_sub_district_ar?.gone()
        spinner_postal_ar?.gone()
    }

    fun onGetDistricts(districts : List<String>){
        spinner_district_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,districts)
        spinner_district_ar?.visible()
        spinner_sub_district_ar?.gone()
        spinner_postal_ar?.gone()
    }

    fun onGetSubDistricts(subDistricts : List<String>){
        spinner_sub_district_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,subDistricts)
        spinner_sub_district_ar?.visible()
        spinner_postal_ar?.gone()
    }

    fun onGetPostals(postals : List<String>){
        spinner_postal_ar?.adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,postals)
        spinner_postal_ar?.visible()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        viewModel.showError.observe()
            .subscribe {
                AlertDialog.Builder(this)
                    .setTitle(Strings.get(R.string.error_title))
                    .setMessage(viewModel.errorString.get())
                    .setNeutralButton(Strings.get(R.string.close)) { dialog, _ -> dialog.dismiss() }
            }.addTo(disposables)
    }

    override fun onSuccessGettingImage(filePath: String?) {
        camera_nik_ar?.fromFilePath(filePath,R.drawable.ic_camera)
        viewModel.nikPicture.set(filePath)
    }

    override fun onFailedGettingImage(error: String?) {
        log(error)
    }
}
package app.z.ewmgt.app.ext

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
* Created by Zharfan on 09/11/2020
* */

@JvmName("addToComposite")
fun Disposable.addTo(disposableComposite: CompositeDisposable) {
  disposableComposite.add(this)
}
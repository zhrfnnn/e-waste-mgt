package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search.HomeWasteSearchActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 17/01/2021
 * */

class WasteCollectionSearchRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as HomeWasteSearchActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as HomeWasteSearchActivity).hideLoading()
    }
}
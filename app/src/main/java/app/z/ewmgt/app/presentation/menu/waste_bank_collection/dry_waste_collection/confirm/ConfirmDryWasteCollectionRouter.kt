package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 25/01/2021
 * */

class ConfirmDryWasteCollectionRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as ConfirmDryWasteCollectionActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as ConfirmDryWasteCollectionActivity).hideLoading()
    }
}
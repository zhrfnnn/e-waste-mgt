package app.z.ewmgt.app.rx

/**
 * Created by Zharfan on 15/01/2021
 * */

interface GetImageListener {
    fun onSuccessGettingImage(filePath : String?)
    fun onFailedGettingImage(error : String?)
}
package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 28/01/2021
 * */

class ConfirmWasteBankDepositRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as ConfirmWasteBankDepositActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as ConfirmWasteBankDepositActivity).hideLoading()
    }

    fun addWaste(){
        forResultScreen(DryWasteTypeActivity::class.java,DryWasteTypeActivity.DRY_WASTE_TYPE)
    }
}
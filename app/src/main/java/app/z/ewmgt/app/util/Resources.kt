package app.z.ewmgt.app.util

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import app.z.data.util.log
import app.z.ewmgt.app.EWasteMgtApp
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager

/**
* Created by Zharfan on 21/11/2020
* */

object Strings {
    fun get(@StringRes stringRes: Int) : String {
        return EWasteMgtApp.instance.resources.getString(stringRes)
    }
}

object Colors {
    fun get (@ColorRes colorRes : Int) : Int {
        return EWasteMgtApp.instance.resources.getColor(colorRes)
    }
}

object Beacons {

    fun bind(manager: BeaconManager,consumer: BeaconConsumer){
        if (manager.isBound(consumer))
            manager.unbind(consumer)
        manager.bind(consumer)
        log("binding beacon manager")
    }

    fun unbind(manager: BeaconManager,consumer: BeaconConsumer){
        if (manager.isBound(consumer)){
            manager.removeAllRangeNotifiers()
            manager.removeAllMonitorNotifiers()
            manager.unbind(consumer)
        }
        log("unbind beacon manager")
    }
}
package app.z.ewmgt.app.presentation.login

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.home.HomeActivity
import app.z.ewmgt.app.presentation.register.RegisterActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 16/11/2020
* */

class LoginRouter (val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    enum class Route {
        REGISTER,
        HOME
    }

    fun navigate (route : Route, bundle : Bundle = Bundle()) {
        when (route) {
            Route.HOME -> { standaloneScreen(HomeActivity::class.java,bundle) }
            Route.REGISTER -> { nextScreen(RegisterActivity::class.java,bundle) }
        }
    }

    fun showLoading(){
        (activityRef.get() as LoginActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as LoginActivity).hideLoading()
    }

}
package app.z.ewmgt.app.di.application

import android.bluetooth.BluetoothAdapter
import android.content.Context
import app.z.ewmgt.app.util.BluetoothManager
import dagger.Module
import dagger.Provides
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import javax.inject.Singleton

/**
* Created by Zharfan on 13/12/2020
* */

@Module
class BluetoothModule {
    companion object {
        const val RUUVI_LAYOUT = "m:0-2=0499,i:4-19,i:20-21,i:22-23,p:24-24"
        const val IBEACON_LAYOUT = "m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"
        const val CUSTOM_LAYOUT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"
        const val ALTBEACON_LAYOUT = BeaconParser.ALTBEACON_LAYOUT
        const val URI_BEACON_LAYOUT = BeaconParser.URI_BEACON_LAYOUT
        const val EDDYSTONE_UID_LAYOUT = BeaconParser.EDDYSTONE_UID_LAYOUT
        const val EDDYSTONE_URL_LAYOUT = BeaconParser.EDDYSTONE_URL_LAYOUT
        const val EDDYSTONE_TLM_LAYOUT = BeaconParser.EDDYSTONE_TLM_LAYOUT
    }

    @Provides
    @Singleton
    fun providesBluetoothAdapter() = BluetoothAdapter.getDefaultAdapter()

    @Provides
    @Singleton
    fun providesBluetoothManager(adapter: BluetoothAdapter,context: Context) = BluetoothManager(adapter,context)

    @Provides
    @Singleton
    fun providesBeaconManager(context: Context): BeaconManager {
        val instance = BeaconManager.getInstanceForApplication(context)

        // Add all the beacon types we want to discover
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(IBEACON_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(ALTBEACON_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(URI_BEACON_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(CUSTOM_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(EDDYSTONE_UID_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(EDDYSTONE_URL_LAYOUT))
        instance.beaconParsers.add(BeaconParser().setBeaconLayout(EDDYSTONE_TLM_LAYOUT))

        return instance
    }
}
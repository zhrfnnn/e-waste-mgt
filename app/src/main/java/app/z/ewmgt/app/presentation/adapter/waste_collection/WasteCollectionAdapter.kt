package app.z.ewmgt.app.presentation.adapter.waste_collection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.domain.model.tablemodel.WasteCollectionTableModel
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.adapter.ObservableRecyclerViewAdapter
import app.z.ewmgt.app.presentation.adapter.waste_collection.WasteCollectionAdapter.Holder
import app.z.ewmgt.app.util.fromUrl
import app.z.data.util.toTimeFromServer
import app.z.data.util.toWasteUrl
import app.z.ewmgt.databinding.ItemWasteCollectionBinding

/**
 * Created by Zharfan on 15/01/2021
 * */

class WasteCollectionAdapter (wasteCollections: ObservableList<WasteCollectionTableModel>) :
    ObservableRecyclerViewAdapter<WasteCollectionTableModel,Holder>(wasteCollections) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemWasteCollectionBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(private val binding: ItemWasteCollectionBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(waste : WasteCollectionTableModel) {

            binding.homeIdIwc.text = waste.homeId
            binding.weightIwc.text = waste.wasteWeight.toString()
            binding.statusIwc.setImageResource(
                if (waste.isDone()){
                    R.drawable.status_done
                } else {
                    R.drawable.status_undone
                }
            )
            binding.timeIwc.text = waste.time?.toTimeFromServer()
            if (!waste.image.isNullOrBlank()){
                binding.pictureIwc.fromUrl(waste.image.toWasteUrl(),R.drawable.ic_waste_empty)
            }
        }
    }
}
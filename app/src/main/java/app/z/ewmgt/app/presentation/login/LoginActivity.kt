package app.z.ewmgt.app.presentation.login

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.Strings
import app.z.ewmgt.databinding.ActivityLoginBinding
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

/**
* Created by Zharfan on 16/11/2020
* */

class LoginActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()
        setClickableText(
            text_dont_have_account_al,
            Strings.get(R.string.dont_have_account),
            Strings.get(R.string.register),
            R.color.bg_regist_1){
            viewModel.registerClicked()
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.showError.observe()
            .subscribe {
                AlertDialog.Builder(this)
                    .setTitle(Strings.get(R.string.error_title))
                    .setMessage(viewModel.errorString.get())
                    .setNeutralButton(Strings.get(R.string.close)) { dialog, _ -> dialog.dismiss() }
            }.addTo(disposables)
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
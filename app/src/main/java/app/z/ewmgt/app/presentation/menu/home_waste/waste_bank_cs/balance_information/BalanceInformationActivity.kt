package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.customviews.CustomRangeDatePicker
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityBalanceInformationBinding
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 20/01/2021
 * */

class BalanceInformationActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: BalanceInformationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityBalanceInformationBinding>(this,R.layout.activity_balance_information)
        screenComponent.inject(this)
        binding?.viewModel = viewModel

        viewModel.bound()

        binding?.rangePickerAbi?.setListener(object : CustomRangeDatePicker.CustomRangeDatePickerListener {
            override fun onDatesPicked(fromDate: Date, toDate: Date) {
                viewModel.search(fromDate,toDate)
            }
        })
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_YEAR, -14)
        binding?.rangePickerAbi?.setDateFrom(c.time)
        binding?.rangePickerAbi?.setDateTo(Date())
        viewModel.search(c.time, Date())
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
package app.z.ewmgt.app.util

import android.os.Build

/**
* Created by Zharfan on 28/11/2020
* */

class AndroidVersion {
    fun isLollipopOrLater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

    fun isMarshmallowOrLater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    fun isNougatOrLater() =  Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

    fun isNougatMR1OrLater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1

    fun isOreoOrLater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

    fun isPieOrLater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
}
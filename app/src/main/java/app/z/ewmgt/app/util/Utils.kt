package app.z.ewmgt.app.util

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.FileProvider
import app.z.data.util.isNotNull
import app.z.ewmgt.R
import com.squareup.picasso.Picasso
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.NumberFormat
import java.util.*

/**
* Created by Zharfan on 16/11/2020
* */

fun ImageView.fromFilePath(filePath : String?, errorResources : Int){
    Picasso.get()
        .load(File(filePath))
        .error(errorResources)
        .into(this)
}

fun ImageView.fromUrl(url : String?, errorResources : Int){
    Picasso.get()
        .load(url)
        .error(errorResources)
        .into(this)
}

fun Double?.toIDR() : String? {
    return if (this.isNotNull())
        NumberFormat.getCurrencyInstance(Locale("in", "ID")).format(this)
    else
        null
}

fun Double?.toIDRKg() : String? {
    return if (this.isNotNull())
        "${NumberFormat.getCurrencyInstance(Locale("in", "ID")).format(this)}/Kg"
    else
        null
}
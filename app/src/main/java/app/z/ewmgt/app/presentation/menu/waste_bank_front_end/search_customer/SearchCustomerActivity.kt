package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivitySearchCustomerBinding
import javax.inject.Inject

/**
 * Created by Zharfan on 27/01/2021
 * */

class SearchCustomerActivity : BaseActivity() {

    companion object {
        const val SEARCH_CUSTOMER = 111

        const val EXTRA_CUSTOMER = "EXTRA_CUSTOMER"
    }

    @Inject
    lateinit var viewModel: SearchCustomerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySearchCustomerBinding>(this,R.layout.activity_search_customer)
        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
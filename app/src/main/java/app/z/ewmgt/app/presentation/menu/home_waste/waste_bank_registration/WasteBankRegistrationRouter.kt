package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.profile.ProfileActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 19/12/2020
* */

class WasteBankRegistrationRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as WasteBankRegistrationActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as WasteBankRegistrationActivity).hideLoading()
    }
}
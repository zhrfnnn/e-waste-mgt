package app.z.ewmgt.app.presentation.welcome

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.login.LoginActivity
import app.z.ewmgt.app.presentation.register.RegisterActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 17/11/2020
* */

class WelcomeRouter (activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    enum class Route {
        LOGIN,
        REGISTER
    }

    fun navigate (route : Route, bundle : Bundle = Bundle()) {
        when (route) {
            Route.LOGIN -> { changeScreen(LoginActivity::class.java,bundle) }
            Route.REGISTER -> { nextScreen(RegisterActivity::class.java,bundle) }
        }
    }

}
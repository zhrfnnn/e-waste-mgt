package app.z.ewmgt.app.presentation

import io.reactivex.disposables.CompositeDisposable

/**
* Created by Zharfan on 19/12/2020
* */

open class BaseViewModel(private val baseRouter: BaseRouter) {

    val disposables = CompositeDisposable()

    open fun bound(){}

    open fun unbound(){
        disposables.clear()
    }

    fun onBackClicked(){
        baseRouter.onBack()
    }
}
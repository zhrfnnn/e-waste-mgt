package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.usecase.findwastebank.FindWasteBankUseCase
import app.z.domain.usecase.login.LoginUseCase
import app.z.domain.usecase.photoprofile.PhotoProfileUseCase
import app.z.domain.usecase.profile.ProfileUseCase
import app.z.domain.usecase.registerwastebank.RegisterWasteBankUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
* Created by Zharfan on 19/12/2020
* */

class WasteBankRegistrationViewModel @Inject constructor(private val context: Context,
                                                         private val profileUseCase: ProfileUseCase,
                                                         private val findWasteBankUseCase: FindWasteBankUseCase,
                                                         private val registerWasteBankUseCase: RegisterWasteBankUseCase,
                                                         private val wasteBankRegistrationRouter: WasteBankRegistrationRouter) : BaseViewModel(wasteBankRegistrationRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val fullName = ObservableField<String>()
    val address = ObservableField<String>()
    val province = ObservableField<String>()
    val city = ObservableField<String>()
    val district = ObservableField<String>()
    val subDistrict = ObservableField<String>()
    val postal = ObservableField<String>()
    val nik = ObservableField<String>()
    val wilayah = ObservableField<String>()
    val email = ObservableField<String>()
    val mobileNumber = ObservableField<String>()

    val selectedWasteBank = ObservableField<User>()
    val wasteBankList = ObservableArrayList<User>()

    override fun bound() {
        super.bound()

        if (Session.currentUser.isNotNull()){
            wasteBankRegistrationRouter.showLoading()
            profileUseCase.execute(Session.currentUser!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetProfile(it) }
                .addTo(disposables)
        }
    }

    private fun handleGetProfile(result : ProfileUseCase.Result){
        when(result){
            is ProfileUseCase.Result.Success -> {
                Session.currentUser = result.user
                implementUser()

                findWasteBankUseCase.execute(Session.currentUser!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleFindWasteBank(it) }
                    .addTo(disposables)
            }
            is ProfileUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        wasteBankRegistrationRouter.hideLoading()
    }

    private fun handleFindWasteBank(result : FindWasteBankUseCase.Result){
        when(result){
            is FindWasteBankUseCase.Result.Success -> {
                wasteBankList.clear()
                wasteBankList.addAll(result.wasteBankList)
            }
            is FindWasteBankUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    private fun implementUser(){
        val user = Session.currentUser!!

        fullName.set(user.fullname)
        address.set(user.address)
        province.set(user.province)
        city.set(user.city)
        district.set(user.district)
        subDistrict.set(user.subDistrict)
        postal.set(user.postalCode)
        nik.set(user.nik)
        wilayah.set(user.wilayahName)
        email.set(user.email)
        mobileNumber.set(user.mobileNumber)

        wasteBankRegistrationRouter.hideLoading()
    }

    fun onSubmit(){
        if (Session.currentUser.isNotNull() && selectedWasteBank.get().isNotNull()){
            wasteBankRegistrationRouter.showLoading()
            registerWasteBankUseCase.execute(Session.currentUser!!,selectedWasteBank.get()!!.id.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when(it){
                        is RegisterWasteBankUseCase.Result.Success -> {
                            if(it.result){
                                context.toast("Successfully Registered")
                                wasteBankRegistrationRouter.onBack()
                            } else {
                                errorString.set(it.toString())
                                showError.trigger(true)
                            }
                        }
                        is RegisterWasteBankUseCase.Result.Failure -> {
                            errorString.set(it.throwable.localizedMessage)
                            showError.trigger(true)
                        }
                    }
                    wasteBankRegistrationRouter.hideLoading()
                }
                .addTo(disposables)
        }
    }
}
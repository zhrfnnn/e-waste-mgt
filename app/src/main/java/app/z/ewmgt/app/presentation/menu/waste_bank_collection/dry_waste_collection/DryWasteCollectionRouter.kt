package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection

import android.app.Activity
import android.os.Bundle
import app.z.domain.model.tablemodel.DryWasteCollectionTableModel
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm.ConfirmDryWasteCollectionActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 24/01/2021
 * */

class DryWasteCollectionRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as DryWasteCollectionActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as DryWasteCollectionActivity).hideLoading()
    }

    fun onDryWasteSelected(bundle: Bundle){
        nextScreen(ConfirmDryWasteCollectionActivity::class.java, bundle)
    }
}
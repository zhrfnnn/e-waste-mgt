package app.z.ewmgt.app.presentation.home

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import app.z.data.util.Session
import app.z.ewmgt.R
import app.z.ewmgt.app.customviews.EWasteAlert
import app.z.ewmgt.app.permission.PermissionHandlerAndroid
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.presentation.adapter.MenuGridAdapter
import app.z.ewmgt.app.util.Strings
import app.z.ewmgt.databinding.ActivityHomeBinding
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

/**
* Created by Zharfan on 16/11/2020
* */

class HomeActivity : BaseActivity(){

    @Inject lateinit var viewModel : HomeViewModel
    @Inject lateinit var permissionHandler : PermissionHandlerAndroid
    private var binding : ActivityHomeBinding? = null

    private val REQ_PERM_CODE = 124

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        screenComponent.inject(this)
        binding?.viewModel = viewModel
        requestPermissions()
        binding?.menuAh?.setOnClickListener {
            openMenu(it)
        }
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        viewModel.showError.observe()
            .subscribe {
                AlertDialog.Builder(this)
                    .setTitle(Strings.get(R.string.error_title))
                    .setMessage(viewModel.errorString.get())
                    .setNeutralButton(Strings.get(R.string.close)) { dialog, _ -> dialog.dismiss() }
            }.addTo(disposables)
    }

    private fun requestPermissions(){
        if (permissionHandler.checkHasPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) &&
            permissionHandler.checkHasPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) &&
            permissionHandler.checkHasPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) &&
            permissionHandler.checkHasPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            viewModel.bound()
        } else {
            permissionHandler.requestPermission(this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQ_PERM_CODE
            )
        }
    }

    fun handleGridMenu(columnSize : Int){
        binding?.gridMenuAh?.numColumns = columnSize
        binding?.gridMenuAh?.adapter = MenuGridAdapter(applicationContext,viewModel.menus,viewModel.menuOnClick)
    }

    fun handleBackgroundColor(color : Int){
        binding?.backgroundAh?.setBackgroundColor(color)
    }

    private fun openMenu(view : View){
        val popup = PopupMenu(this,view)
        popup.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.setting_hm -> {
                    viewModel.openSetting()
                    return@setOnMenuItemClickListener true
                }
                R.id.logout_hm -> {
                    viewModel.logout()
                    return@setOnMenuItemClickListener true
                }
                else-> {
                    return@setOnMenuItemClickListener false
                }
            }
        }
        popup.inflate(R.menu.home_menu)
        popup.show()
    }

    @Suppress("DEPRECATED_IDENTITY_EQUALS")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQ_PERM_CODE -> {
                var letsGo = true
                for (i in 0 until permissions.size) {
                    val grantResult = grantResults[i]
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        letsGo = false

                    }
                }
                if (letsGo){
                    requestPermissions()
                } else {
                    EWasteAlert.instance(this,
                        Strings.get(R.string.func_limited),
                        Strings.get(R.string.func_limited_detail),
                        object : EWasteAlert.EWasteAlertListener {
                            override fun onDismiss() {
                                requestPermissions()
                            }
                        }
                    ).show()
                }
            }
        }
    }
}
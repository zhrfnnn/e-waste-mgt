package app.z.ewmgt.app.di.application

import app.z.ewmgt.app.EWasteMgtApp
import app.z.ewmgt.app.di.screen.ScreenComponent
import app.z.ewmgt.app.di.screen.ScreenModule
import dagger.Component
import org.altbeacon.beacon.BeaconManager
import javax.inject.Singleton

/**
* Created by Zharfan on 09/11/2020
* */

@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, EndpointModule::class, BluetoothModule::class])
interface ApplicationComponent {

  fun providesBeaconManager() : BeaconManager

  fun inject(activity: EWasteMgtApp)

  fun plus(screenModule: ScreenModule): ScreenComponent
}

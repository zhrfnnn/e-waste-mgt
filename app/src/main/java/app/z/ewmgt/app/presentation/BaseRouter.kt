package app.z.ewmgt.app.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import app.z.domain.model.User
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 16/11/2020
* */

open class BaseRouter(private val activityRef : WeakReference<Activity>) {

    fun standaloneScreen(clazz: Class<*>, bundle: Bundle) {
        val intent = Intent(activityRef.get(), clazz).putExtras(bundle)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activityRef.get()?.startActivity(intent)
    }

    fun changeScreen(clazz: Class<*>, bundle: Bundle) {
        activityRef.get()?.startActivity(Intent(activityRef.get(), clazz).putExtras(bundle))
        activityRef.get()?.finish()
    }

    fun nextScreen(clazz: Class<*>, bundle: Bundle) {
        activityRef.get()?.startActivity(Intent(activityRef.get(), clazz).putExtras(bundle))
    }

    fun forResultScreen(clazz: Class<*>,requestCode : Int){
        activityRef.get()?.startActivityForResult(Intent(activityRef.get(), clazz),requestCode)
    }

    fun onFinishResultScreen(resultIntent : Intent){
        activityRef.get()?.setResult(Activity.RESULT_OK,resultIntent)
        activityRef.get()?.finish()
    }

    fun onBack(){
        activityRef.get()?.onBackPressed()
    }

    fun recreate(){
        activityRef.get()?.recreate()
    }
}
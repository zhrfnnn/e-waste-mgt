package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm

import android.content.Context
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.log
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.model.UserType
import app.z.domain.model.Wilayah
import app.z.domain.model.tablemodel.DryWasteCollectionTableModel
import app.z.domain.usecase.profile.ProfileUseCase
import app.z.domain.usecase.updatestatus.UpdateWasteStatusUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import app.z.ewmgt.app.util.toIDR
import app.z.ewmgt.app.util.toIDRKg
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Zharfan on 25/01/2021
 * */

class ConfirmDryWasteCollectionViewModel @Inject constructor(private val context: Context,
                                                             private val profileUseCase: ProfileUseCase,
                                                             private val updateWasteStatusUseCase: UpdateWasteStatusUseCase,
                                                             private val confirmDryWasteCollectionRouter: ConfirmDryWasteCollectionRouter) : BaseViewModel(confirmDryWasteCollectionRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val selectedDryWaste = ObservableField<DryWasteCollectionTableModel>()
    val selectedUser = ObservableField<User>()

    val priceKg = ObservableField<String>()
    val totalPrice = ObservableField<String>()

    override fun bound() {
        super.bound()

        if (selectedDryWaste.get()?.memberId.isNotNull()){
            implementDryWaste(selectedDryWaste.get())
            confirmDryWasteCollectionRouter.showLoading()
            profileUseCase.execute(User(id = selectedDryWaste.get()!!.memberId!!,position = UserType(),wilayah = Wilayah()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetProfile(it) }
                .addTo(disposables)
        }
    }

    private fun handleGetProfile(result : ProfileUseCase.Result){
        when(result){
            is ProfileUseCase.Result.Success -> {
                selectedUser.set(result.user)
            }
            is ProfileUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        confirmDryWasteCollectionRouter.hideLoading()
    }

    private fun implementDryWaste(dryWaste : DryWasteCollectionTableModel?){
        priceKg.set(dryWaste?.price?.toIDRKg())
        totalPrice.set(dryWaste?.totalPrice?.toIDR())
    }

    fun onSubmit(){
        if (Session.currentUser.isNotNull()){
            updateWasteStatusUseCase.execute(Session.currentUser!!,selectedDryWaste.get()?.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when(it){
                        is UpdateWasteStatusUseCase.Result.Success -> {
                            if (it.result){
                                context.toast("Successfully submitted")
                                confirmDryWasteCollectionRouter.onBack()
                            } else {
                                errorString.set(it.result.toString())
                                showError.trigger(true)
                            }
                        }
                        is UpdateWasteStatusUseCase.Result.Failure -> {
                            errorString.set(it.throwable.localizedMessage)
                            showError.trigger(true)
                        }
                    }
                }
                .addTo(disposables)
        }
    }
}
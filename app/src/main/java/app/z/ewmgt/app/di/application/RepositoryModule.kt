package app.z.ewmgt.app.di.application

import app.z.data.Api
import app.z.data.mapper.Mapper
import app.z.data.repository.RepositoryImpl
import app.z.domain.repository.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
* Created by Zharfan on 09/11/2020
* */

@Module
class RepositoryModule {

  @Provides
  @Singleton
  fun provideRepository(api: Api, mapper: Mapper): Repository {
    return RepositoryImpl(api, mapper)
  }

}

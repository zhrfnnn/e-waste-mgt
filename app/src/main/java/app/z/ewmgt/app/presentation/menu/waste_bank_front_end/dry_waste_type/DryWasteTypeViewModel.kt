package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type

import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.domain.model.DryWasteType
import app.z.domain.usecase.drywastetypes.GetDryWasteTypesUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerActivity
import app.z.ewmgt.app.rx.StickyAction
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Zharfan on 27/01/2021
 * */

class DryWasteTypeViewModel @Inject constructor(private val context: Context,
                                                private val getDryWasteTypesUseCase: GetDryWasteTypesUseCase,
                                                private val dryWasteTypeRouter: DryWasteTypeRouter) : BaseViewModel(dryWasteTypeRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val dryWasteTypes = ObservableArrayList<DryWasteType>()

    override fun bound() {
        super.bound()

        dryWasteTypeRouter.showLoading()
        getDryWasteTypesUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetDryWasteTypes(it) }
            .addTo(disposables)
    }

    private fun handleGetDryWasteTypes(result : GetDryWasteTypesUseCase.Result){
        when(result){
            is GetDryWasteTypesUseCase.Result.Success -> {
                dryWasteTypes.clear()
                dryWasteTypes.addAll(result.dryWastes)
            }
            is GetDryWasteTypesUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        dryWasteTypeRouter.hideLoading()
    }

    fun onDryWasteTypeSelected(dryWasteType : DryWasteType){
        val result = Intent()
        result.putExtra(DryWasteTypeActivity.EXTRA_DRY_WASTE_TYPE, Gson().toJson(dryWasteType))
        dryWasteTypeRouter.onFinishResultScreen(result)
    }
}
package app.z.ewmgt.app.di.screen

import app.z.ewmgt.app.di.scope.PerScreen
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.presentation.home.HomeActivity
import app.z.ewmgt.app.presentation.login.LoginActivity
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.HomeWasteActivity
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search.HomeWasteSearchActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.WasteBankCSActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information.BalanceInformationActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration.WasteBankRegistrationActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.DryWasteCollectionActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm.ConfirmDryWasteCollectionActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist.CustomerRegistrationActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.WasteBankDepositActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm.ConfirmWasteBankDepositActivity
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.WasteCollectionActivity
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search.WasteCollectionSearchActivity
import app.z.ewmgt.app.presentation.profile.ProfileActivity
import app.z.ewmgt.app.presentation.register.RegisterActivity
import app.z.ewmgt.app.presentation.welcome.WelcomeActivity
import dagger.Subcomponent

/**
* Created by Zharfan on 09/11/2020
* */

@PerScreen
@Subcomponent(modules = [ScreenModule::class])
interface ScreenComponent {

  //Inject activity in here
  fun inject(baseActivity: BaseActivity)
  fun inject(homeActivity: HomeActivity)
  fun inject(welcomeActivity: WelcomeActivity)
  fun inject(loginActivity: LoginActivity)
  fun inject(registerActivity: RegisterActivity)
  fun inject(registerActivity: ProfileActivity)

  fun inject(homeWasteActivity: HomeWasteActivity)
  fun inject(homeWasteSearchActivity: HomeWasteSearchActivity)
  fun inject(wasteBankRegistrationActivity: WasteBankRegistrationActivity)
  fun inject(wasteBankCSActivity: WasteBankCSActivity)
  fun inject(balanceInformationActivity: BalanceInformationActivity)

  fun inject(wasteCollectionActivity: WasteCollectionActivity)
  fun inject(wasteCollectionSearchActivity: WasteCollectionSearchActivity)

  fun inject(dryWasteCollectionActivity: DryWasteCollectionActivity)
  fun inject(confirmDryWasteCollectionActivity: ConfirmDryWasteCollectionActivity)

  fun inject(customerRegistrationActivity: CustomerRegistrationActivity)
  fun inject(wasteBankDepositActivity: WasteBankDepositActivity)
  fun inject(confirmWasteBankDepositActivity: ConfirmWasteBankDepositActivity)

  fun inject(searchCustomerActivity: SearchCustomerActivity)
  fun inject(dryWasteTypeActivity: DryWasteTypeActivity)
}

package app.z.ewmgt.app.customviews.dialog

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import app.z.ewmgt.R

/**
* Created by Zharfan on 12/01/2021
* */

class LoadingDialog(val activity: Activity) : Dialog(activity) {

    companion object {
        interface LoadingListener {
            fun showLoading()
            fun hideLoading()
        }
    }

    init {
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loading_dialog)
        val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window?.setLayout(layoutParams.width, layoutParams.height)
    }

    fun start() {
        activity.runOnUiThread { setting(true) }
    }
    fun stop() {
        activity.runOnUiThread { setting(false) }
    }

    private fun setting(isVisible : Boolean) {
        if (isVisible){
            if (!isShowing)
                show()
        } else {
            if (isShowing)
                dismiss()
        }
    }

}
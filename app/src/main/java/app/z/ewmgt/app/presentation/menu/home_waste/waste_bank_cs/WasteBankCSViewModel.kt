package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs

import android.content.Context
import androidx.databinding.ObservableField
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import javax.inject.Inject

/**
 * Created by Zharfan on 20/01/2021
 * */

class WasteBankCSViewModel @Inject constructor(private val context: Context,
                                               private val wasteBankCSRouter: WasteBankCSRouter) : BaseViewModel(wasteBankCSRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    fun onBalanceInformation(){
        wasteBankCSRouter.balanceInformation()
    }

    fun onFundTransferRequest(){
        wasteBankCSRouter.fundTransferRequest()
    }
}
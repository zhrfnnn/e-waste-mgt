package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection

import android.os.Bundle
import android.os.RemoteException
import android.view.View
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import app.z.data.util.log
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.Beacons
import app.z.ewmgt.databinding.ActivityWasteCollectionBinding
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.MonitorNotifier
import org.altbeacon.beacon.Region
import javax.inject.Inject

/**
 * Created by Zharfan on 15/01/2021
 * */

class WasteCollectionActivity : BaseActivity(),BeaconConsumer {

    @Inject
    lateinit var viewModel: WasteCollectionViewModel

    @Inject
    lateinit var beaconManager: BeaconManager

    private var binding : ActivityWasteCollectionBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_waste_collection)
        screenComponent.inject(this)
        binding?.viewModel = viewModel
        viewModel.bound()

        binding?.moreAwc?.setOnClickListener {
            openMenu(it)
        }
    }

    override fun onDestroy() {
        viewModel.unbound()
        unbindBeacon()
        super.onDestroy()
    }

    override fun onResume() {
        if (viewModel.wasteCollections.isNotEmpty()){
            bindBeacon()
        }
        super.onResume()
    }

    override fun onPause() {
        unbindBeacon()
        super.onPause()
    }

    private fun openMenu(view : View){
        val popup = PopupMenu(this,view)
        popup.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.search_wcm -> {
                    viewModel.moreSearch()
                    return@setOnMenuItemClickListener true
                }
                else-> {
                    return@setOnMenuItemClickListener false
                }
            }
        }
        popup.inflate(R.menu.waste_collection_menu)
        popup.show()
    }

    override fun onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(object : MonitorNotifier {
            override fun didEnterRegion(region: Region?) {
                try {
                    beaconManager.addRangeNotifier { beacons, region ->
                        if (beacons.isNotEmpty()){
                            beacons.forEach {
                                if (it.distance < 1.0){
                                    log(it.bluetoothAddress)
                                    viewModel.updateWasteStatus(it.bluetoothAddress)
                                    return@forEach
                                }
                            }
                        } else {
                            log("There's no beacons here.")
                        }
                    }
                    beaconManager.startRangingBeaconsInRegion(Region("app.z.ewmgt.wastecollect", null, null, null))
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }

            override fun didDetermineStateForRegion(state: Int, region: Region?) {}
            override fun didExitRegion(region: Region?) {}
        })
        try {
            beaconManager.startMonitoringBeaconsInRegion(Region("app.z.ewmgt.wastecollect", null, null, null))
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun bindBeacon(){
        if (!bluetoothState.isEnabled())
            bluetoothState.enable()
        Beacons.bind(beaconManager,this)
    }

    fun unbindBeacon(){
        if (bluetoothState.isEnabled())
            bluetoothState.disable()
        Beacons.unbind(beaconManager,this)
    }
}
package app.z.ewmgt.app.presentation.menu.home_waste.home_waste

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search.HomeWasteSearchActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 19/12/2020
* */

class HomeWasteRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun search(){
        nextScreen(HomeWasteSearchActivity::class.java, Bundle())
    }

    fun showLoading(){
        (activityRef.get() as HomeWasteActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as HomeWasteActivity).hideLoading()
    }
}
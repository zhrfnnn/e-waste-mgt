package app.z.ewmgt.app.customviews

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import app.z.ewmgt.R
import kotlinx.android.synthetic.main.layout_alert.*

/**
* Created by Zharfan on 20/11/2020
* */

class EWasteAlert(context: Context) : Dialog(context) {

    interface EWasteAlertListener {
        fun onDoneClicked(){}
        fun onCancelClicked(){}
        fun onDismiss()
    }

    companion object {
        private var message : String? = null
        private var title : String? = null
        private var listener : EWasteAlertListener? = null

        fun instance(context: Context,
                     title : String? = null,
                     message : String? = null,
                     listener: EWasteAlertListener? = null) : EWasteAlert {
            this.message = message
            this.title = title
            this.listener = listener
            return EWasteAlert(context)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_alert)
        setCancelable(false)
        title_la?.text = title
        message_la?.text = message
        bt_done_la?.setOnClickListener {
            listener?.onDoneClicked()
            dismiss()
        }
        bt_cancel_la?.setOnClickListener {
            listener?.onCancelClicked()
            dismiss()
        }
        setOnDismissListener {
            listener?.onDismiss()
        }
        window?.setLayout((context.resources.displayMetrics.widthPixels * 0.80).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}
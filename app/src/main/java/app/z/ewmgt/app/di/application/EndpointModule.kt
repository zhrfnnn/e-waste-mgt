package app.z.ewmgt.app.di.application

import app.z.data.APIEndpoint
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
* Created by Zharfan on 09/11/2020
* */

@Module
class EndpointModule {

  @Provides
  @Singleton
  fun provideApiEndpoint(retrofit: Retrofit): APIEndpoint {
    return retrofit.create(APIEndpoint::class.java)
  }
}

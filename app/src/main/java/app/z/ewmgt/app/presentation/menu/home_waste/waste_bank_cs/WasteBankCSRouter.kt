package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.HomeWasteActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information.BalanceInformationActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 20/01/2021
 * */

class WasteBankCSRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as WasteBankCSActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as WasteBankCSActivity).hideLoading()
    }

    fun balanceInformation(){
        nextScreen(BalanceInformationActivity::class.java, Bundle())
    }

    fun fundTransferRequest(){
//        nextScreen()
    }
}
package app.z.ewmgt.app.presentation.welcome

import androidx.databinding.ObservableField
import app.z.ewmgt.BuildConfig
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
* Created by Zharfan on 17/11/2020
* */

class WelcomeViewModel @Inject constructor(private val welcomeRouter: WelcomeRouter) : BaseViewModel(welcomeRouter) {

    var appVersion = ObservableField<String>()

    override fun bound() {
        super.bound()
        appVersion.set("App Version : ${BuildConfig.VERSION_NAME}")
    }

    fun onRegisterButtonClicked(){
        welcomeRouter.navigate(WelcomeRouter.Route.REGISTER)
    }

    fun onLoginButtonClicked(){
        welcomeRouter.navigate(WelcomeRouter.Route.LOGIN)
    }

}
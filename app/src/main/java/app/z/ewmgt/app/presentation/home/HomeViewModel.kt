package app.z.ewmgt.app.presentation.home

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.underConstructionToast
import app.z.domain.model.UserType
import app.z.ewmgt.R
import app.z.ewmgt.app.model.HomeMenu
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import app.z.ewmgt.app.util.Colors
import javax.inject.Inject

/**
* Created by Zharfan on 16/11/2020
* */

class HomeViewModel @Inject constructor(private val homeRouter: HomeRouter,
                                        private val context: Context) : BaseViewModel(homeRouter) {

    val progressVisible = ObservableBoolean()

    val showError = StickyAction<Boolean>()
    val errorString = ObservableField<String>()

    val menus = ObservableArrayList<HomeMenu>()
    val menuOnClick : (homeMenu : HomeMenu) -> Unit = {
        when(it){
            HomeMenu.MENU_HOME_WASTE -> {
                homeRouter.navigate(HomeRouter.Route.MENU_HOME_WASTE)
            }
            HomeMenu.MENU_WASTE_BANK_REGISTRATION -> {
                homeRouter.navigate(HomeRouter.Route.MENU_WASTE_BANK_REGIST)
            }
            HomeMenu.MENU_WASTE_BANK_CS -> {
                homeRouter.navigate(HomeRouter.Route.MENU_WASTE_BANK_CS)
            }

            HomeMenu.MENU_WASTE_COLLECTION -> {
                homeRouter.navigate(HomeRouter.Route.MENU_WASTE_COLL)
            }

            HomeMenu.MENU_DRY_WASTE_COLLECTION -> {
                homeRouter.navigate(HomeRouter.Route.MENU_DRY_WASTE_COLL)
            }

            HomeMenu.MENU_CUSTOMER_REGISTRATION -> {
                homeRouter.navigate(HomeRouter.Route.MENU_CUSTOMER_REGIST)
            }
            HomeMenu.MENU_WASTE_BANK_DEPOSIT -> {
                homeRouter.navigate(HomeRouter.Route.MENU_WASTE_BANK_DEPOSIT)
            }
            HomeMenu.MENU_WASTE_BANK_BY_CS -> {
                homeRouter.navigate(HomeRouter.Route.MENU_WASTE_BANK_BY_CS)
            }
        }
    }
    private fun menuColumn() : Int {
        return when {
            menus.size < 2 -> 1
            menus.size < 5 -> 2
            else -> 3
        }
    }

    fun openSetting(){
        homeRouter.navigate(HomeRouter.Route.PROFILE)
    }

    fun logout(){
        Session.currentUser = null
        bound()
    }

    override fun bound(){
        super.bound()
        homeRouter.showLoading()
        val currentUser = Session.currentUser
        if (!currentUser.isNotNull()){
            homeRouter.navigate(HomeRouter.Route.WELCOME)
        } else {
            if (currentUser!!.position!!.sameAs(UserType.HOME_WASTE)){
                menus.add(HomeMenu.MENU_HOME_WASTE)
                menus.add(HomeMenu.MENU_WASTE_BANK_REGISTRATION)
                menus.add(HomeMenu.MENU_WASTE_BANK_CS)
                homeRouter.handleBackground(Colors.get(R.color.bg_home_waste_1))
            } else if (currentUser.position!!.sameAs(UserType.WASTE_COLLECTION)){
                menus.add(HomeMenu.MENU_WASTE_COLLECTION)
                homeRouter.handleBackground(Colors.get(R.color.bg_waste_collection_1))
            } else if (currentUser.position!!.sameAs(UserType.WASTE_BANK)){
                menus.add(HomeMenu.MENU_DRY_WASTE_COLLECTION)
                menus.add(HomeMenu.MENU_CUSTOMER_REGISTRATION)
                menus.add(HomeMenu.MENU_WASTE_BANK_DEPOSIT)
                menus.add(HomeMenu.MENU_WASTE_BANK_BY_CS)
                homeRouter.handleBackground(Colors.get(R.color.bg_waste_bank_1))
            }
//            when(currentUser!!.position){
//                UserType.HOME_WASTE -> {
//                    menus.add(HomeMenu.MENU_HOME_WASTE)
//                    menus.add(HomeMenu.MENU_WASTE_BANK_REGISTRATION)
//                    menus.add(HomeMenu.MENU_WASTE_BANK_CS)
//                    homeRouter.handleBackground(Colors.get(R.color.bg_home_waste_1))
//                }
//                Position.WASTE_COLLECTION_DRIVER_ASSISTANT -> {
//                    menus.add(HomeMenu.MENU_WASTE_COLLECTION)
//                    homeRouter.handleBackground(Colors.get(R.color.bg_waste_collection_1))
//                }
//                Position.WASTE_BANK_FROND_END_STAFF -> {
//                    menus.add(HomeMenu.MENU_CUSTOMER_REGISTRATION)
//                    menus.add(HomeMenu.MENU_WASTE_BANK_DEPOSIT)
//                    menus.add(HomeMenu.MENU_WASTE_BANK_BY_CS)
//                    homeRouter.handleBackground(Colors.get(R.color.bg_waste_bank_1))
//                }
//                else -> {}
//            }
                homeRouter.handleGridMenu(menuColumn())
        }
        homeRouter.hideLoading()
    }

    override fun unbound() {
        super.unbound()
        disposables.clear()
    }
}
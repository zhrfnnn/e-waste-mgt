package app.z.ewmgt.app.presentation.home

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.HomeWasteActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.WasteBankCSActivity
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration.WasteBankRegistrationActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.DryWasteCollectionActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist.CustomerRegistrationActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.WasteBankDepositActivity
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.WasteCollectionActivity
import app.z.ewmgt.app.presentation.profile.ProfileActivity
import app.z.ewmgt.app.presentation.register.RegisterActivity
import app.z.ewmgt.app.presentation.welcome.WelcomeActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 16/11/2020
* */

class HomeRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    enum class Route {
        WELCOME,
        PROFILE,

        //Home Waste
        MENU_HOME_WASTE,
        MENU_WASTE_BANK_REGIST,
        MENU_WASTE_BANK_CS,

        //Dry Waste Collection
        MENU_DRY_WASTE_COLL,

        //Waste Collection
        MENU_WASTE_COLL,

        //Waste Bank CS
        MENU_CUSTOMER_REGIST,
        MENU_WASTE_BANK_DEPOSIT,
        MENU_WASTE_BANK_BY_CS
    }

    fun navigate(route : Route, bundle : Bundle = Bundle()) {
        when (route) {
            Route.WELCOME -> { changeScreen(WelcomeActivity::class.java,bundle) }
            Route.PROFILE -> { nextScreen(ProfileActivity::class.java,bundle)}

            Route.MENU_HOME_WASTE -> { nextScreen(HomeWasteActivity::class.java,bundle)}
            Route.MENU_WASTE_BANK_REGIST -> { nextScreen(WasteBankRegistrationActivity::class.java,bundle)}
            Route.MENU_WASTE_BANK_CS -> { nextScreen(WasteBankCSActivity::class.java,bundle) }

            Route.MENU_DRY_WASTE_COLL -> { nextScreen(DryWasteCollectionActivity::class.java,bundle) }

            Route.MENU_WASTE_COLL -> { nextScreen(WasteCollectionActivity::class.java,bundle) }

            Route.MENU_CUSTOMER_REGIST -> { nextScreen(CustomerRegistrationActivity::class.java,bundle) }
            Route.MENU_WASTE_BANK_DEPOSIT -> { nextScreen(WasteBankDepositActivity::class.java,bundle) }
            Route.MENU_WASTE_BANK_BY_CS -> {  }
        }
    }

    fun handleGridMenu(columnSize : Int){
        (activityRef.get() as HomeActivity).handleGridMenu(columnSize)
    }

    fun handleBackground(color : Int){
        (activityRef.get() as HomeActivity).handleBackgroundColor(color)
    }

    fun showLoading(){
        (activityRef.get() as HomeActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as HomeActivity).hideLoading()
    }
}
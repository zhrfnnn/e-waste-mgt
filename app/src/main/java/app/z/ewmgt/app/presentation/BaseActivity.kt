package app.z.ewmgt.app.presentation

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toFile
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.z.data.util.compress
import app.z.data.util.isNotNull
import app.z.ewmgt.app.EWasteMgtApp
import app.z.ewmgt.app.customviews.dialog.LoadingDialog
import app.z.ewmgt.app.di.application.ApplicationComponent
import app.z.ewmgt.app.di.screen.ScreenModule
import app.z.ewmgt.app.presentation.adapter.GetDryWasteAdapter
import app.z.ewmgt.app.presentation.adapter.SearchCustomerAdapter
import app.z.ewmgt.app.presentation.adapter.dry_waste_collection.DryWasteCollectionAdapter
import app.z.ewmgt.app.presentation.adapter.home_waste.BalanceInformationAdapter
import app.z.ewmgt.app.presentation.adapter.home_waste.HomeWasteAdapter
import app.z.ewmgt.app.presentation.adapter.home_waste.WasteBankRegistrationAdapter
import app.z.ewmgt.app.presentation.adapter.waste_collection.WasteCollectionAdapter
import app.z.ewmgt.app.presentation.adapter.waste_collection.WasteCollectionHistoryAdapter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.HomeWasteViewModel
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search.HomeWasteSearchViewModel
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information.BalanceInformationViewModel
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration.WasteBankRegistrationViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.DryWasteCollectionViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerViewModel
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.WasteCollectionViewModel
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search.WasteCollectionSearchViewModel
import app.z.ewmgt.app.rx.GetImageListener
import app.z.ewmgt.app.util.*
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*
import javax.inject.Inject

/**
* Created by Zharfan on 09/11/2020
* */

open class BaseActivity : AppCompatActivity(),GetImageListener,LoadingDialog.Companion.LoadingListener {

  companion object {
    enum class ImageRatio {
      IMAGE_1_1,IMAGE_4_3
    }

    @JvmStatic
    @BindingAdapter("today_home_waste_adapter")
    fun bindTodayList(recyclerView: RecyclerView, viewModel: HomeWasteViewModel) {
      val adapter = HomeWasteAdapter(viewModel.todayHistory)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("past_home_waste_adapter")
    fun bindPastList(recyclerView: RecyclerView, viewModel: HomeWasteViewModel) {
      val adapter = HomeWasteAdapter(viewModel.pastHistory)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("search_home_waste_adapter")
    fun bindHistoryList(recyclerView: RecyclerView, viewModel: HomeWasteSearchViewModel) {
      val adapter = HomeWasteAdapter(viewModel.homeWasteList)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("search_waste_collection_adapter")
    fun bindHistoryWCList(recyclerView: RecyclerView, viewModel: WasteCollectionSearchViewModel) {
      val adapter = WasteCollectionHistoryAdapter(viewModel.wasteCollections)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("waste_collection_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: WasteCollectionViewModel) {
      val adapter = WasteCollectionAdapter(viewModel.wasteCollections)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("waste_bank_register_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: WasteBankRegistrationViewModel) {
      val adapter = WasteBankRegistrationAdapter(viewModel.wasteBankList)
      adapter.onWasteBankSelected = { viewModel.selectedWasteBank.set(it) }
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("balance_information_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: BalanceInformationViewModel) {
      val adapter = BalanceInformationAdapter(viewModel.balanceList)
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("dry_waste_collection_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: DryWasteCollectionViewModel) {
      val adapter = DryWasteCollectionAdapter(viewModel.dryWasteCollections)
      adapter.onDryWasteSelected = { viewModel.onDryWasteSelected(it) }
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("search_customer_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: SearchCustomerViewModel) {
      val adapter = SearchCustomerAdapter(viewModel.users)
      adapter.onUserSelected = { viewModel.onUserSelected(it) }
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("dry_waste_type_adapter")
    fun bindList(recyclerView: RecyclerView, viewModel: DryWasteTypeViewModel) {
      val adapter = GetDryWasteAdapter(viewModel.dryWasteTypes)
      adapter.onDryWasteTypeSelected = { viewModel.onDryWasteTypeSelected(it) }
      recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
      recyclerView.adapter = adapter
    }
  }

  @Inject lateinit var bluetoothState : BluetoothManager
  protected val disposables = CompositeDisposable()
  private var loadingDialog : LoadingDialog? = null

  val screenComponent by lazy {
    getApplicationComponent().plus(ScreenModule(this))
  }

  private fun getApplicationComponent(): ApplicationComponent {
    return (application as EWasteMgtApp).component
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    screenComponent.inject(this)
    loadingDialog = LoadingDialog(this)
  }

  override fun onPause() {
    disposables.clear()
    super.onPause()
  }

  fun getImage(ratio : ImageRatio){
    val cropImage = CropImage.activity()
    when(ratio){
      ImageRatio.IMAGE_1_1 -> cropImage.setAspectRatio(1, 1)
      ImageRatio.IMAGE_4_3 -> cropImage.setAspectRatio(4, 3)
    }
    cropImage.setFixAspectRatio(true)
    cropImage.setGuidelines(CropImageView.Guidelines.ON)
    cropImage.start(this)
  }

  private fun compressPicture(uri: Uri){
    GlobalScope.launch(CoroutineContextProvider().main){
      try {
        val picture: File? = File(uri.path)
        if (picture.isNotNull()){
          GlobalScope.launch(CoroutineContextProvider().io){
            picture!!.compress(153600)
          }
          onSuccessGettingImage(uri.path)
        } else {
          onFailedGettingImage("Path is null")
        }
      } catch (e : Exception){
        onFailedGettingImage(e.localizedMessage)
      }
    }
  }


  fun setClickableText(textView : TextView, firstText : String, clickedText : String, clickedTextColor : Int, onClick : ()-> Unit) {
    val text = SpannableStringBuilder("$firstText ")
    text.append(clickedText)
    text.setSpan(object : ClickableSpan(){
      override fun onClick(view: View) {
        onClick()
      }
      override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = false
        ds.isFakeBoldText = true
        ds.color = Colors.get(clickedTextColor)
      }
    },text.length - clickedText.length,text.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

    textView.movementMethod = LinkMovementMethod.getInstance()
    textView.setText(text, TextView.BufferType.SPANNABLE)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    when(requestCode){
      CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
        val result = CropImage.getActivityResult(data)
        when(resultCode){
          RESULT_OK -> {
            compressPicture(result.uri)
          }
          CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {
            onFailedGettingImage(result.error.localizedMessage)
          }
        }
      }
    }
  }

  override fun onSuccessGettingImage(filePath: String?) {}

  override fun onFailedGettingImage(error: String?) {}

  override fun showLoading() {
    loadingDialog?.start()
  }

  override fun hideLoading() {
    loadingDialog?.stop()
  }
}
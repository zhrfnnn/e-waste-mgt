package app.z.ewmgt.app.util

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject

/**
* Created by Zharfan on 28/11/2020
* */

class BluetoothManager @Inject constructor(private val adapter: BluetoothAdapter?, context: Context) {
    
    enum class BluetoothState {
        STATE_OFF,
        STATE_TURNING_OFF,
        STATE_ON,
        STATE_TURNING_ON
    }

    private val subject: BehaviorProcessor<BluetoothState> =
        BehaviorProcessor.createDefault<BluetoothState>(getStateFromAdapterState(adapter?.state ?: BluetoothAdapter.STATE_OFF))

    init {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(ctx: Context, intent: Intent) {
                if (BluetoothAdapter.ACTION_STATE_CHANGED == intent.action) {
                    val state = getStateFromAdapterState(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR))

                    subject.onNext(state)
                }
            }
        }
        context.registerReceiver(receiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    fun getStateFromAdapterState(state: Int) : BluetoothState {
        return when (state) {
            BluetoothAdapter.STATE_OFF -> BluetoothState.STATE_OFF
            BluetoothAdapter.STATE_TURNING_OFF -> BluetoothState.STATE_TURNING_OFF
            BluetoothAdapter.STATE_ON -> BluetoothState.STATE_ON
            BluetoothAdapter.STATE_TURNING_ON -> BluetoothState.STATE_TURNING_ON
            else -> BluetoothState.STATE_OFF
        }
    }

    fun disable() = adapter?.disable()

    fun enable() = adapter?.enable()

    fun asFlowable(): Flowable<BluetoothState> {
        return subject
    }

    fun isEnabled() = adapter?.isEnabled == true

    fun toggle() {
        if (isEnabled()) {
            disable()
        } else {
            enable()
        }
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist.CustomerRegistrationActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerActivity
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm.ConfirmWasteBankDepositActivity
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 24/01/2021
 * */

class WasteBankDepositRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as CustomerRegistrationActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as CustomerRegistrationActivity).hideLoading()
    }

    fun searchCustomer(){
        forResultScreen(SearchCustomerActivity::class.java,SearchCustomerActivity.SEARCH_CUSTOMER)
    }

    fun onSelectedUser(bundle: Bundle){
        nextScreen(ConfirmWasteBankDepositActivity::class.java,bundle)
    }
}
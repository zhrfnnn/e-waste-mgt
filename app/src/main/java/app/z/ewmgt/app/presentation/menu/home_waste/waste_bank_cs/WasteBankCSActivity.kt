package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityWasteBankCsBinding
import javax.inject.Inject

/**
* Created by Zharfan on 20/01/2021
* */

class WasteBankCSActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: WasteBankCSViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityWasteBankCsBinding>(this, R.layout.activity_waste_bank_cs)
        screenComponent.inject(this)
        binding?.viewModel = viewModel

        viewModel.bound()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
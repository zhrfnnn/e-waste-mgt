package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 27/01/2021
 * */

class SearchCustomerRouter(private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as SearchCustomerActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as SearchCustomerActivity).hideLoading()
    }
}
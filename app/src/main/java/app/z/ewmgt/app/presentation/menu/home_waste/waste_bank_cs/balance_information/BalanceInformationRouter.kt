package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information

import android.app.Activity
import app.z.ewmgt.app.presentation.BaseRouter
import java.lang.ref.WeakReference

/**
 * Created by Zharfan on 20/01/2021
 * */

class BalanceInformationRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    fun showLoading(){
        (activityRef.get() as BalanceInformationActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as BalanceInformationActivity).hideLoading()
    }
}
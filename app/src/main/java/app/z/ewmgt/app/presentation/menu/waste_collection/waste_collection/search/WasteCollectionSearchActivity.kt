package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.customviews.CustomRangeDatePicker
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.databinding.ActivityWasteCollectionSearchBinding
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 17/01/2021
 * */

class WasteCollectionSearchActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: WasteCollectionSearchViewModel

    private var binding : ActivityWasteCollectionSearchBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_waste_collection_search)

        screenComponent.inject(this)
        binding?.viewModel = viewModel

        viewModel.bound()

        binding?.rangePickerAwcs?.setListener(object : CustomRangeDatePicker.CustomRangeDatePickerListener {
            override fun onDatesPicked(fromDate: Date, toDate: Date) {
                viewModel.search(fromDate,toDate)
            }
        })
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_YEAR, -14)
        binding?.rangePickerAwcs?.setDateFrom(c.time)
        binding?.rangePickerAwcs?.setDateTo(Date())
        viewModel.search(c.time, Date())
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
package app.z.ewmgt.app.model

import app.z.ewmgt.R

/**
* Created by Zharfan on 13/12/2020
* */

enum class HomeMenu(val logo :  Int, val menuName : String, val textColor : Int) {

    //Home Waste (HOME_WASTE)
    MENU_HOME_WASTE(R.drawable.ic_menu_home_waste,"Home\nWaste",R.color.black),
    MENU_WASTE_BANK_REGISTRATION(R.drawable.ic_menu_waste_bank,"Waste Bank\nRegistration",R.color.black),
    MENU_WASTE_BANK_CS(R.drawable.ic_menu_waste_bank_cs,"Waste Bank\nCustomer Support",R.color.black),

    //Waste Collection (WASTE_COLLECTION_DRIVER_ASSISTANT)
    MENU_WASTE_COLLECTION(R.drawable.ic_menu_waste_collection,"Waste\nCollection",R.color.black),

    //Waste Bank Collection (todo)
    MENU_DRY_WASTE_COLLECTION(R.drawable.ic_menu_waste_bank,"Dry Waste\nCollection",R.color.white),

    //Waste Bank Front-End (WASTE_BANK_FROND_END_STAFF)
    MENU_CUSTOMER_REGISTRATION(R.drawable.ic_menu_waste_registration,"Customer\nRegistration",R.color.white),
    MENU_WASTE_BANK_DEPOSIT(R.drawable.ic_menu_waste_bank,"Waste Bank\nDeposit",R.color.white),
    MENU_WASTE_BANK_BY_CS(R.drawable.ic_menu_waste_bank_cs,"Waste Bank\nCustomer Support",R.color.white)

    //Waste Bank Director todo
}
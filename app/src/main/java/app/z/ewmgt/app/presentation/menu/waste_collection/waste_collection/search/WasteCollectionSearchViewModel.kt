package app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toServerFormatString
import app.z.domain.model.tablemodel.WasteCollectionHistoryTableModel
import app.z.domain.usecase.historywastecollection.HistoryWasteCollectionUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 17/01/2021
 * */

class WasteCollectionSearchViewModel @Inject constructor(private val context: Context,
                                                         private val historyWasteCollectionUseCase: HistoryWasteCollectionUseCase,
                                                         private val wasteCollectionSearchRouter: WasteCollectionSearchRouter) : BaseViewModel(wasteCollectionSearchRouter) {
    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val wasteCollections = ObservableArrayList<WasteCollectionHistoryTableModel>()

    private fun handleSearchList(result : HistoryWasteCollectionUseCase.Result){
        when(result){
            is HistoryWasteCollectionUseCase.Result.Success -> {
                wasteCollections.clear()
                wasteCollections.addAll(result.homeWasteList)
            }
            is HistoryWasteCollectionUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    fun search(startDate: Date,
               endDate: Date){
        if (Session.currentUser.isNotNull()){
            historyWasteCollectionUseCase.execute(
                Session.currentUser!!,
                startDate.toServerFormatString(),
                endDate.toServerFormatString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleSearchList(it) }
                .addTo(disposables)
        }
    }
}
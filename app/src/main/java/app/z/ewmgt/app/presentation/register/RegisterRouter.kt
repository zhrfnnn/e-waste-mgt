package app.z.ewmgt.app.presentation.register

import android.app.Activity
import android.os.Bundle
import app.z.ewmgt.app.presentation.BaseRouter
import app.z.ewmgt.app.presentation.home.HomeActivity
import app.z.ewmgt.app.presentation.login.LoginActivity
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 17/11/2020
* */

class RegisterRouter (private val activityRef : WeakReference<Activity>) : BaseRouter(activityRef) {

    enum class Route {
        HOME,
        LOGIN
    }

    fun navigate (route : Route, bundle : Bundle = Bundle()) {
        when (route) {
            Route.HOME -> { standaloneScreen(HomeActivity::class.java,bundle) }
            Route.LOGIN -> { changeScreen(LoginActivity::class.java,bundle) }
        }
    }

    fun onGetUserTypes(userTypes : List<String>){
        (activityRef.get() as RegisterActivity).onGetUserTypes(userTypes)
    }

    fun onGetWilayahs(wilayahs : List<String>){
        (activityRef.get() as RegisterActivity).onGetWilayahs(wilayahs)
    }

    fun onGetProvinces(provinces : List<String>){
        (activityRef.get() as RegisterActivity).onGetProvinces(provinces)
    }

    fun onGetCities(cities : List<String>){
        (activityRef.get() as RegisterActivity).onGetCities(cities)
    }

    fun onGetDistricts(districts : List<String>){
        (activityRef.get() as RegisterActivity).onGetDistricts(districts)
    }

    fun onGetSubDistricts(subDistricts : List<String>){
        (activityRef.get() as RegisterActivity).onGetSubDistricts(subDistricts)
    }

    fun onGetPostals(postals : List<String>){
        (activityRef.get() as RegisterActivity).onGetPostals(postals)
    }

    fun showLoading(){
        (activityRef.get() as RegisterActivity).showLoading()
    }

    fun hideLoading(){
        (activityRef.get() as RegisterActivity).hideLoading()
    }
}
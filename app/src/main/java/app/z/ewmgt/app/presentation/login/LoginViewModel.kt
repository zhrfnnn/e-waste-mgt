package app.z.ewmgt.app.presentation.login

import android.content.Context
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.toast
import app.z.domain.usecase.login.LoginUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
* Created by Zharfan on 16/11/2020
* */

class LoginViewModel @Inject constructor(private val context : Context,
                                         private val loginRouter: LoginRouter,
                                         private val loginUseCase : LoginUseCase) : BaseViewModel(loginRouter){

    val id = ObservableField<String>()
    val password = ObservableField<String>()

    val showError = StickyAction<Boolean>()
    val errorString = ObservableField<String>()

    override fun bound() {
        super.bound()
        id.set("haris@plasaweb.com")
        password.set("123456")
    }

    private fun handleLogin(result : LoginUseCase.Result){
        when (result){
            is LoginUseCase.Result.Success -> {
                Session.currentUser = result.user
                loginRouter.navigate(LoginRouter.Route.HOME)
            }
            is LoginUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        loginRouter.hideLoading()
    }

    fun loginClicked(){
        if (!id.get().isNullOrBlank() && !password.get().isNullOrBlank()){
            loginRouter.showLoading()
            loginUseCase.execute(id.get()!!,password.get()!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleLogin(it) }
                .addTo(disposables)
        } else {
            context.toast("Fill username and password correctly")
        }
    }

    fun forgotClicked(){

    }

    fun registerClicked(){
        loginRouter.navigate(LoginRouter.Route.REGISTER)
    }
}
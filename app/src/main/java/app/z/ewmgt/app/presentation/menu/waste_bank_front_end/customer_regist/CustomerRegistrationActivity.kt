package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import app.z.data.util.gone
import app.z.data.util.log
import app.z.data.util.visible
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.Strings
import app.z.ewmgt.app.util.fromFilePath
import app.z.ewmgt.databinding.ActivityCustomerRegistrationBinding
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_customer_registration.*
import javax.inject.Inject

/**
 * Created by Zharfan on 24/01/2021
 * */

class CustomerRegistrationActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: CustomerRegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityCustomerRegistrationBinding = DataBindingUtil.setContentView(this, R.layout.activity_customer_registration)

        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()

        camera_nik_acr?.setOnClickListener {
            getImage(Companion.ImageRatio.IMAGE_4_3)
        }
    }

    fun onGetProvinces(provinces : List<String>){
        spinner_province_acr?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,provinces)
        spinner_city_acr?.gone()
        spinner_district_acr?.gone()
        spinner_sub_district_acr?.gone()
        spinner_postal_acr?.gone()
    }

    fun onGetCities(cities : List<String>){
        spinner_city_acr?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,cities)
        spinner_city_acr?.visible()
        spinner_district_acr?.gone()
        spinner_sub_district_acr?.gone()
        spinner_postal_acr?.gone()
    }

    fun onGetDistricts(districts : List<String>){
        spinner_district_acr?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,districts)
        spinner_district_acr?.visible()
        spinner_sub_district_acr?.gone()
        spinner_postal_acr?.gone()
    }

    fun onGetSubDistricts(subDistricts : List<String>){
        spinner_sub_district_acr?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,subDistricts)
        spinner_sub_district_acr?.visible()
        spinner_postal_acr?.gone()
    }

    fun onGetPostals(postals : List<String>){
        spinner_postal_acr?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,postals)
        spinner_postal_acr?.visible()
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        viewModel.showError.observe()
            .subscribe {
                AlertDialog.Builder(this)
                    .setTitle(Strings.get(R.string.error_title))
                    .setMessage(viewModel.errorString.get())
                    .setNeutralButton(Strings.get(R.string.close)) { dialog, _ -> dialog.dismiss() }
            }.addTo(disposables)
    }

    override fun onSuccessGettingImage(filePath: String?) {
        camera_nik_acr?.fromFilePath(filePath, R.drawable.ic_camera)
        viewModel.nikPicture.set(filePath)
    }

    override fun onFailedGettingImage(error: String?) {
        log(error)
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.z.data.util.Session
import app.z.data.util.toast
import app.z.domain.model.User
import app.z.domain.model.UserType
import app.z.domain.model.Wilayah
import app.z.domain.usecase.location.LocationUseCase
import app.z.domain.usecase.login.LoginUseCase
import app.z.domain.usecase.register.RegisterUseCase
import app.z.domain.usecase.registerwastebank.RegisterWasteBankUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.presentation.login.LoginRouter
import app.z.ewmgt.app.presentation.register.RegisterRouter
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Zharfan on 24/01/2021
 * */

class CustomerRegistrationViewModel @Inject constructor(private val context: Context,
                                                        private val registerUseCase: RegisterUseCase,
                                                        private val locationUseCase: LocationUseCase,
                                                        private val registerWasteBankUseCase: RegisterWasteBankUseCase,
                                                        private val loginUseCase : LoginUseCase,
                                                        private val customerRegistrationRouter: CustomerRegistrationRouter) : BaseViewModel(customerRegistrationRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val provinces = ObservableArrayList<String>()
    val provincePosition = MutableLiveData<Int>()
    private val selectedProvince : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(provincePosition) {
            value = provinces[it]
        }
    }
    val cities = ObservableArrayList<String>()
    val cityPosition = MutableLiveData<Int>()
    private val selectedCity : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(cityPosition) {
            value = cities[it]
        }
    }
    val districts = ObservableArrayList<String>()
    val districtPosition = MutableLiveData<Int>()
    private val selectedDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(districtPosition) {
            value = districts[it]
        }
    }
    val subDistricts = ObservableArrayList<String>()
    val subDistrictPosition = MutableLiveData<Int>()
    private val selectedSubDistrict : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(subDistrictPosition) {
            value = subDistricts[it]
        }
    }
    val postals = ObservableArrayList<String>()
    val postalPosition = MutableLiveData<Int>()
    private val selectedPostal : LiveData<String> = MediatorLiveData<String>().apply {
        addSource(postalPosition) {
            value = postals[it]
        }
    }

    val fullName = ObservableField<String>()
    val address = ObservableField<String>()
    val nik = ObservableField<String>()
    val email = ObservableField<String>()
    val mobileNumber = ObservableField<String>()
    val password = ObservableField<String>()
    val confirmPassword = ObservableField<String>()
    val nikPicture = ObservableField<String>()
    
    override fun bound() {
        super.bound()

        customerRegistrationRouter.showLoading()

        locationUseCase.executeProvince()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleGetProvince(it) }
            .addTo(disposables)

        selectedProvince.observeForever { province ->
            if (province.isNotBlank() && !province.equals("=== Please Select ==="))
                customerRegistrationRouter.showLoading()
            locationUseCase.executeCity(province)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetCity(it) }
                .addTo(disposables)

        }

        selectedCity.observeForever {city ->
            if (city.isNotBlank() && !city.equals("=== Please Select ==="))
                customerRegistrationRouter.showLoading()
            locationUseCase.executeDistrict(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetDistrict(it) }
                .addTo(disposables)
        }

        selectedDistrict.observeForever {district ->
            if (district.isNotBlank() && !district.equals("=== Please Select ==="))
                customerRegistrationRouter.showLoading()
            locationUseCase.executeSubDistrict(district)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetSubDistrict(it) }
                .addTo(disposables)
        }

        selectedSubDistrict.observeForever {subDistrict ->
            if (subDistrict.isNotBlank() && !subDistrict.equals("=== Please Select ==="))
                customerRegistrationRouter.showLoading()
            locationUseCase.executePostal(subDistrict)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetPostal(it) }
                .addTo(disposables)
        }

        selectedPostal.observeForever {  }
    }

    private fun handleGetProvince(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                provinces.clear()
                provinces.add("=== Please Select ===")
                result.locations.forEach {
                    provinces.add(it.province)
                }
                customerRegistrationRouter.onGetProvinces(provinces)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    private fun handleGetCity(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                cities.clear()
                cities.add("=== Please Select ===")
                result.locations.forEach {
                    cities.add(it.city)
                }
                customerRegistrationRouter.onGetCities(cities)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    private fun handleGetDistrict(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                districts.clear()
                districts.add("=== Please Select ===")
                result.locations.forEach {
                    districts.add(it.district)
                }
                customerRegistrationRouter.onGetDistricts(districts)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    private fun handleGetSubDistrict(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                subDistricts.clear()
                subDistricts.add("=== Please Select ===")
                result.locations.forEach {
                    subDistricts.add(it.subDistrict)
                }
                customerRegistrationRouter.onGetSubDistricts(subDistricts)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    private fun handleGetPostal(result : LocationUseCase.Result){
        when (result){
            is LocationUseCase.Result.Success -> {
                postals.clear()
                postals.add("=== Please Select ===")
                result.locations.forEach {
                    postals.add(it.postal)
                }
                customerRegistrationRouter.onGetPostals(postals)
            }
            is LocationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    private fun handleLogin(result : LoginUseCase.Result){
        when (result){
            is LoginUseCase.Result.Success -> {
                customerRegistrationRouter.showLoading()
                registerWasteBankUseCase.execute(result.user,Session.currentUser?.id.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        when(it){
                            is RegisterWasteBankUseCase.Result.Success -> {
                                if (it.result){
                                    context.toast("Successfully Registered")
                                    customerRegistrationRouter.onBack()
                                } else {
                                    errorString.set(it.result.toString())
                                    showError.trigger(true)
                                }
                            }
                            is RegisterWasteBankUseCase.Result.Failure -> {
                                errorString.set(it.throwable.localizedMessage)
                                showError.trigger(true)
                            }
                        }
                        customerRegistrationRouter.hideLoading()
                    }
                    .addTo(disposables)
            }
            is LoginUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        customerRegistrationRouter.hideLoading()
    }

    fun onSubmit(){
        if (!selectedProvince.value.equals("=== Please Select ===") &&
            !selectedCity.value.equals("=== Please Select ===") &&
            !selectedDistrict.value.equals("=== Please Select ===") &&
            !selectedSubDistrict.value.equals("=== Please Select ===") &&
            !selectedPostal.value.equals("=== Please Select ===") &&
            !nikPicture.get().isNullOrBlank()){

            val user = User(0,position = UserType.HOME_WASTE,wilayah = Wilayah(id = 0))
            user.fullname = fullName.get()
            user.address = address.get()
            user.province = selectedProvince.value
            user.city = selectedCity.value
            user.district = selectedDistrict.value
            user.subDistrict = selectedSubDistrict.value
            user.postalCode = selectedPostal.value
            user.nik = nik.get()
            if (!nikPicture.get().isNullOrBlank()){
                user.nikPicture = nikPicture.get()
            }
            user.email = email.get()
            user.mobileNumber = mobileNumber.get()
            if (!password.get().isNullOrBlank() && !confirmPassword.get().isNullOrBlank()){
                if (password.get().equals(confirmPassword.get())){
                    user.password = password.get()

                    customerRegistrationRouter.showLoading()
                    registerUseCase.execute(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            when(it){
                                is RegisterUseCase.Result.Success -> {
                                    if (it.result){
                                        customerRegistrationRouter.showLoading()
                                        loginUseCase.execute(email.get()!!,password.get()!!)
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe { handleLogin(it) }
                                            .addTo(disposables)
                                    }
                                }
                                is RegisterUseCase.Result.Failure -> {
                                    errorString.set(it.throwable.localizedMessage)
                                    showError.trigger(true)
                                }
                            }
                            customerRegistrationRouter.hideLoading()
                        }
                        .addTo(disposables)
                } else {
                    context.toast("Password must match")
                }
            }
        } else {
            context.toast("Please fill correctly")
        }
    }
}
package app.z.ewmgt.app.presentation.welcome

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.z.ewmgt.R
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.util.Strings
import app.z.ewmgt.databinding.ActivityWelcomeBinding
import kotlinx.android.synthetic.main.activity_welcome.*
import javax.inject.Inject

/**
* Created by Zharfan on 17/11/2020
* */

class WelcomeActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : WelcomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityWelcomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome)

        screenComponent.inject(this)

        binding.viewModel = viewModel
        viewModel.bound()
        setClickableText(text_alr_have_account_aw,
            Strings.get(R.string.alr_have_account),
            Strings.get(R.string.login),
            R.color.bt_login){
            viewModel.onLoginButtonClicked()
        }
    }

    override fun onDestroy() {
        viewModel.unbound()
        super.onDestroy()
    }
}
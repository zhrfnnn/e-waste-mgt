package app.z.ewmgt.app.permission

import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import app.z.ewmgt.app.presentation.BaseActivity
import javax.inject.Inject

/**
* Created by Zharfan on 20/11/2020
* */

class PermissionHandlerAndroid @Inject constructor(): PermissionHandler {
    override fun checkHasPermission(activity: BaseActivity, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(activity,permission) == PackageManager.PERMISSION_GRANTED
    }

    override fun requestPermission(activity: BaseActivity, permissions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }
}
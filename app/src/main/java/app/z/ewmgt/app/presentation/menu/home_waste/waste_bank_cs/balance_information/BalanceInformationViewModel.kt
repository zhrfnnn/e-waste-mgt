package app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.Session
import app.z.data.util.isNotNull
import app.z.data.util.toServerFormatString
import app.z.domain.model.tablemodel.BalanceInformationTableModel
import app.z.domain.usecase.balanceinformation.BalanceInformationUseCase
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 20/01/2021
 * */

class BalanceInformationViewModel @Inject constructor(private val context: Context,
                                                      private val balanceInformationUseCase: BalanceInformationUseCase,
                                                      private val balanceInformationRouter: BalanceInformationRouter) : BaseViewModel(balanceInformationRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val currentBalance = ObservableField<String>()
    val balanceList = ObservableArrayList<BalanceInformationTableModel>()

    private fun handleBalanceInformation(result : BalanceInformationUseCase.Result){
        when(result){
            is BalanceInformationUseCase.Result.Success -> {
                balanceList.clear()
                balanceList.addAll(result.balanceList)
            }
            is BalanceInformationUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
        balanceInformationRouter.hideLoading()
    }

    fun search(startDate: Date,
               endDate: Date){
        if (Session.currentUser.isNotNull()){
            balanceInformationRouter.showLoading()
            balanceInformationUseCase.execute(
                user = Session.currentUser!!,
                startDate = startDate.toServerFormatString(),
                endDate = endDate.toServerFormatString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleBalanceInformation(it) }
                .addTo(disposables)
        }
    }

    fun onSendEmail(){}
}
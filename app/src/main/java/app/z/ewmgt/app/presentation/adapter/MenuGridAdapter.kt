package app.z.ewmgt.app.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.ObservableArrayList
import app.z.ewmgt.R
import app.z.ewmgt.app.customviews.MenuView
import app.z.ewmgt.app.model.HomeMenu
import app.z.ewmgt.app.util.Colors
import kotlinx.android.synthetic.main.menu_view.view.*

/**
 * Created by Zharfan on 15/01/2021
 * */

class MenuGridAdapter(val context: Context,
                      val menus : ObservableArrayList<HomeMenu>,
                      val menuOnClickListener : (menu : HomeMenu) -> Unit) : BaseAdapter() {

    override fun getView(pos: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val baseView = layoutInflater.inflate(R.layout.menu_layout,null)

        val menuView : MenuView = baseView.findViewById(R.id.menu_view_ml)

        menuView.menu_icon_mv.setImageResource(menus[pos].logo)
        menuView.menu_label_mv.text = menus[pos].menuName
        menuView.menu_label_mv.setTextColor(Colors.get(menus[pos].textColor))

        menuView.menu_icon_mv.setOnClickListener {
            menuOnClickListener(getItem(pos))
        }

        return baseView
    }

    override fun getItem(pos: Int): HomeMenu {
        return menus[pos]
    }

    override fun getItemId(pos: Int): Long {
        return menus[pos].ordinal.toLong()
    }

    override fun getCount(): Int {
        return menus.size
    }
}
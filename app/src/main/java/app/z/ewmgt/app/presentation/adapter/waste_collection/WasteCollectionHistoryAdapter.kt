package app.z.ewmgt.app.presentation.adapter.waste_collection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import app.z.data.util.toDefaultFormatString
import app.z.domain.model.tablemodel.WasteCollectionHistoryTableModel
import app.z.ewmgt.app.presentation.adapter.ObservableRecyclerViewAdapter
import app.z.ewmgt.databinding.ItemWasteCollectionHistoryBinding

/**
 * Created by Zharfan on 17/01/2021
 * */

class WasteCollectionHistoryAdapter (wasteCollections: ObservableList<WasteCollectionHistoryTableModel>) :
    ObservableRecyclerViewAdapter<WasteCollectionHistoryTableModel, WasteCollectionHistoryAdapter.Holder>(wasteCollections) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):Holder {
        return Holder(ItemWasteCollectionHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(private val binding: ItemWasteCollectionHistoryBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(waste : WasteCollectionHistoryTableModel) {
            binding.dateIwch.text = waste.date.toDefaultFormatString()
            binding.addressIwch.text = "${waste.address} ${waste.wilayah}"
            binding.beaconIdIwch.text = waste.homeId
            binding.totalIwch.text = waste.total.toString()
        }
    }
}
package app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import app.z.data.util.*
import app.z.domain.model.tablemodel.DryWasteCollectionTableModel
import app.z.domain.model.tablemodel.WasteCollectionTableModel
import app.z.domain.usecase.dryschedule.DryScheduleUseCase
import app.z.domain.usecase.schedule.ScheduleUseCase
import app.z.ewmgt.R
import app.z.ewmgt.app.ext.addTo
import app.z.ewmgt.app.presentation.BaseViewModel
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm.ConfirmDryWasteCollectionActivity
import app.z.ewmgt.app.rx.StickyAction
import app.z.ewmgt.app.util.Strings
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by Zharfan on 24/01/2021
 * */

class DryWasteCollectionViewModel @Inject constructor(private val context: Context,
                                                      private val dryScheduleUseCase: DryScheduleUseCase,
                                                      private val dryWasteCollectionRouter: DryWasteCollectionRouter) : BaseViewModel(dryWasteCollectionRouter) {

    val errorString = ObservableField<String>()
    val showError = StickyAction<Boolean>()

    val title = ObservableField<String>()
    val dryWasteCollections = ObservableArrayList<DryWasteCollectionTableModel>()

    override fun bound() {
        super.bound()

        title.set("${Strings.get(R.string.dry_waste_schedule_coll)} ${Date().toDefaultFormatString()}")

        if (Session.currentUser.isNotNull()){
            dryScheduleUseCase.execute(Session.currentUser!!,Date().toServerFormatString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetDrySchedules(it) }
                .addTo(disposables)
        }
    }

    private fun handleGetDrySchedules(result : DryScheduleUseCase.Result){
        when(result){
            is DryScheduleUseCase.Result.Success -> {
                dryWasteCollections.clear()
                dryWasteCollections.addAll(result.result)
            }
            is DryScheduleUseCase.Result.Failure -> {
                errorString.set(result.throwable.localizedMessage)
                showError.trigger(true)
            }
        }
    }

    fun onDryWasteSelected(dryWaste : DryWasteCollectionTableModel){
        dryWasteCollectionRouter.onDryWasteSelected(Bundle().apply {
            putString(ConfirmDryWasteCollectionActivity.DRY_WASTE,Gson().toJson(dryWaste))
        })
    }

    fun onFinish(){
        dryWasteCollectionRouter.onBack()
    }
}
package app.z.ewmgt.app.di.screen

import app.z.ewmgt.app.di.scope.PerScreen
import app.z.ewmgt.app.presentation.BaseActivity
import app.z.ewmgt.app.presentation.home.HomeRouter
import app.z.ewmgt.app.presentation.login.LoginRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.HomeWasteRouter
import app.z.ewmgt.app.presentation.menu.home_waste.home_waste.search.HomeWasteSearchRouter
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.WasteBankCSRouter
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_cs.balance_information.BalanceInformationRouter
import app.z.ewmgt.app.presentation.menu.home_waste.waste_bank_registration.WasteBankRegistrationRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.DryWasteCollectionRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_collection.dry_waste_collection.confirm.ConfirmDryWasteCollectionRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.customer_regist.CustomerRegistrationRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.dry_waste_type.DryWasteTypeRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.search_customer.SearchCustomerRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.WasteBankDepositRouter
import app.z.ewmgt.app.presentation.menu.waste_bank_front_end.waste_bank_deposit.confirm.ConfirmWasteBankDepositRouter
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.WasteCollectionRouter
import app.z.ewmgt.app.presentation.menu.waste_collection.waste_collection.search.WasteCollectionSearchRouter
import app.z.ewmgt.app.presentation.profile.ProfileRouter
import app.z.ewmgt.app.presentation.register.RegisterRouter
import app.z.ewmgt.app.presentation.welcome.WelcomeRouter
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference

/**
* Created by Zharfan on 09/11/2020
* */

@Module
class ScreenModule(private val activity: BaseActivity) {

  @PerScreen
  @Provides
  fun providesActivity(): BaseActivity {
    return activity
  }

  //Provides Router

  @PerScreen
  @Provides
  fun providesHomeRouter(): HomeRouter {
    return HomeRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesWelcomeRouter(): WelcomeRouter {
    return WelcomeRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesLoginRouter(): LoginRouter {
    return LoginRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesRegisterRouter(): RegisterRouter {
    return RegisterRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesProfileRouter(): ProfileRouter {
    return ProfileRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesHomeWasteRouter(): HomeWasteRouter {
    return HomeWasteRouter(WeakReference(activity))
  }
  @PerScreen
  @Provides
  fun providesWasteBankRegistrationRouter(): WasteBankRegistrationRouter {
    return WasteBankRegistrationRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesWasteCollectionRouter(): WasteCollectionRouter {
    return WasteCollectionRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesHomeWasteSearchRouter(): HomeWasteSearchRouter {
    return HomeWasteSearchRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesWasteCollectionSearchRouter(): WasteCollectionSearchRouter {
    return WasteCollectionSearchRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesWasteBankCSRouter(): WasteBankCSRouter {
    return WasteBankCSRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesBalanceInformationRouter(): BalanceInformationRouter {
    return BalanceInformationRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesDryWasteCollectionRouter(): DryWasteCollectionRouter {
    return DryWasteCollectionRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesConfirmDryWasteCollectionRouter(): ConfirmDryWasteCollectionRouter {
    return ConfirmDryWasteCollectionRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesCustomerRegistrationRouter(): CustomerRegistrationRouter {
    return CustomerRegistrationRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesWasteBankDepositRouter(): WasteBankDepositRouter {
    return WasteBankDepositRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesSearchCustomerRouter(): SearchCustomerRouter {
    return SearchCustomerRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesDryWasteTypeRouter(): DryWasteTypeRouter {
    return DryWasteTypeRouter(WeakReference(activity))
  }

  @PerScreen
  @Provides
  fun providesConfirmWasteBankDepositRouter(): ConfirmWasteBankDepositRouter {
    return ConfirmWasteBankDepositRouter(WeakReference(activity))
  }
}

package app.z.ewmgt.app.customviews

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import app.z.data.util.isNotNull
import app.z.data.util.toDefaultFormatString
import app.z.ewmgt.R
import kotlinx.android.synthetic.main.custom_date_picker_layout.view.*
import java.util.*

/**
* Created by Zharfan on 04/08/2019
* */

class CustomDatePicker : LinearLayout {
    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private var date : Date? = null
    private var listener : CustomDatePickerListener? = null

    private var maxDate : Long? = null
    private var minDate : Long? = null


    init {
        LayoutInflater.from(context).inflate(R.layout.custom_date_picker_layout, this, true)
        date = Date()
        dateText?.text = date?.toDefaultFormatString()
        this.setOnClickListener {
            show()
        }
    }

    fun setDate (date : Date?) {
        this.date = date
        dateText?.text = date?.toDefaultFormatString()
    }

    fun getDate () : Date? {
        return date
    }

    fun setListener (listener : CustomDatePickerListener?) {
        this.listener = listener
    }

    fun setMinDate(minDate : Long?){
        this.minDate = minDate
    }

    fun setMaxDate(maxDate : Long?){
        this.maxDate = maxDate
    }

    private fun getMinDate() : Long? {
        return this.minDate
    }

    private fun getMaxDate() : Long? {
        return this.maxDate
    }

    fun show(){
        val c = Calendar.getInstance()
        if (date.isNotNull()){
            c.time = date!!
        }
        val dpd = DatePickerDialog(context,DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            val calendar = Calendar.getInstance()
            calendar.set(year,monthOfYear,dayOfMonth)
            date = calendar.time
            dateText?.text = date?.toDefaultFormatString()
            listener?.onDatePicked(date!!)
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
        dpd.datePicker.maxDate = if (getMaxDate().isNotNull()) getMaxDate()!! else Date().time
        if (getMinDate().isNotNull()) {
            dpd.datePicker.minDate = getMinDate()!!
        }
        dpd.show()
    }

    interface CustomDatePickerListener{
        fun onDatePicked(date : Date)
    }
}
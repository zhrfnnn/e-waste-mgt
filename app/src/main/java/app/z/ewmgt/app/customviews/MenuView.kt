package app.z.ewmgt.app.customviews

import android.content.Context
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import app.z.ewmgt.R

/**
 * Created by Zharfan on 15/01/2021
 * */

class MenuView : LinearLayout {

    constructor(context : Context) : super(context)
    constructor(context : Context, attributeSet: AttributeSet) : super(context,attributeSet) {
        initMenu(context,attributeSet)
    }
    constructor(context : Context, attributeSet: AttributeSet,defStyle : Int) : super(context,attributeSet,defStyle){
        initMenu(context,attributeSet)
    }

    private var menuIcon : Drawable? = null
    private var menuLabel : String? = null

    private var iconView : ImageView? = null
    private var labelView : TextView? = null

    private fun initMenu(context: Context,attributeSet: AttributeSet){
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.menu_view,this)

        iconView = findViewById(R.id.menu_icon_mv)
        labelView = findViewById(R.id.menu_label_mv)

        val ta = getContext().obtainStyledAttributes(attributeSet, R.styleable.MenuView)
        try {
            menuIcon = ta.getDrawable(R.styleable.MenuView_menuIcon)
            iconView?.setImageDrawable(menuIcon)

            menuLabel = ta.getString(R.styleable.MenuView_menuLabel)
            labelView?.text = menuLabel
        } finally {
            ta.recycle()
        }
    }
}
package app.z.data.util

import android.content.Context
import android.content.SharedPreferences
import app.z.domain.model.User
import com.google.gson.Gson

/**
* Created by Zharfan on 16/11/2020
* */

object Session {

    private const val MODE = Context.MODE_PRIVATE
    private var preferences: SharedPreferences? = null

    private var CURRENTUSER = Pair("currentUser", null)
    private var CURRENTBEACONMANAGER = Pair("currentBeaconManager", false)

    fun init(context: Context){
        preferences = context.getSharedPreferences("SHARED_SESSION", MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var currentUser: User?
        set(value) = preferences!!.edit {
            if (value != null) {
                it.putString(CURRENTUSER.first, Gson().toJson(value))
            } else {
                it.putString(CURRENTUSER.first, null)
            }
        }
        get() {
            preferences!!.getString(CURRENTUSER.first, CURRENTUSER.second).let {
                return Gson().fromJson(it, User::class.java)
            }
        }

    var currentBeaconManager : Boolean
        set(value) = preferences!!.edit {
            it.putBoolean(CURRENTBEACONMANAGER.first,value)
        }
        get() {
            return preferences!!.getBoolean(CURRENTBEACONMANAGER.first, CURRENTBEACONMANAGER.second)
        }
}
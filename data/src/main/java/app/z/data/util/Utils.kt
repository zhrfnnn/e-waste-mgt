package app.z.data.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

/**
 * Created by Zharfan on 01/01/2021
 * */

fun Any?.isNotNull() : Boolean {
    return this != null
}

fun log(text : String?){
    Log.w("devvv","$text")
}

fun Context.toast(text : String?){
    Toast.makeText(this,"$text", Toast.LENGTH_SHORT).show()
}

fun Context.underConstructionToast(){
    Toast.makeText(this,"Under Construction", Toast.LENGTH_SHORT).show()
}

fun File.compress(maxSize : Int) {
    if (this.length() > maxSize) {
        var streamLength = maxSize
        var compressQuality = 105
        val bmpStream = ByteArrayOutputStream()
        val uncompress = BitmapFactory.decodeFile(this.absolutePath, BitmapFactory.Options())
        while (streamLength >= maxSize && compressQuality > 5) {
            bmpStream.use {
                it.flush()
                it.reset()
            }
            compressQuality -= 5
            uncompress.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
            val bmpPicByteArray = bmpStream.toByteArray()
            streamLength = bmpPicByteArray.size
        }
        FileOutputStream(this).use {
            it.write(bmpStream.toByteArray())
        }
    }
}

fun View.visible(){
    this.visibility = View.VISIBLE
}

fun View.gone(){
    this.visibility = View.GONE
}

fun String?.toProfileUrl() : String? {
    if (this.isNotNull()){
        return "http://plasaweb.com/ewaste/files/profile/$this"
    }
    return null
}

fun String?.toWasteUrl() : String? {
    if (this.isNotNull()){
        return "http://plasaweb.com/ewaste/files/foto/$this"
    }
    return null
}

fun String?.toRequestBody(contentType : String) : RequestBody? {
    if (this.isNotNull()){
        return RequestBody.create(contentType.toMediaTypeOrNull(),this!!)
    }
    return null
}

fun Double?.toRequestBody(contentType : String) : RequestBody? {
    if (this.isNotNull()){
        return RequestBody.create(contentType.toMediaTypeOrNull(),this!!.toString())
    }
    return null
}

fun File?.toMultiPartBodyPart(param : String) : MultipartBody.Part? {
    if (this.isNotNull()){
        return MultipartBody.Part.createFormData(param,this!!.name,RequestBody.create("multipart/form-data".toMediaTypeOrNull(),this))
    }
    return null
}
package app.z.data.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Zharfan on 12/01/2021
 * */

fun Date?.toServerFormatString() : String? {
    return SimpleDateFormat("yyyy-MM-dd").format(this)
}

fun Date?.toDefaultFormatString() : String? {
    return SimpleDateFormat("dd MMM yyyy").format(this)
}

fun String?.toDateFromServer() : Date? {
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this)
}

fun String?.toTimeFromServer() : String? {
    //2021-01-15 03:35:10
    try {
        val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val timeFormat = SimpleDateFormat("HH:mm")

        return timeFormat.format(serverFormat.parse(this))
    } catch (e : Exception){
        e.printStackTrace()
    }
    return null
}
package app.z.data.repository

import app.z.data.Api
import app.z.data.mapper.Mapper
import app.z.domain.model.*
import app.z.domain.model.tablemodel.*
import app.z.domain.repository.Repository
import io.reactivex.Observable

/**
* Created by Zharfan on 06/01/2021
* */

class RepositoryImpl(
    private val api : Api,
    private val mapper : Mapper) : Repository {

    //register
    override fun register(user: User): Observable<Boolean> {
        return api.register(user).map {
            mapper.registerMap(it)
        }
    }

    override fun login(username: String,
                       password: String): Observable<User> {
        return api.login(username, password).map {
            mapper.loginMap(it)
        }
    }

    override fun getProfilesByUserId(user: User): Observable<User> {
        return api.getProfilesByUserId(user).map {
            mapper.profileMap(it[0])
        }
    }

    override fun updateProfile(user: User): Observable<Boolean> {
        return api.updateProfile(user).map {
            mapper.defaultBooleanMap(it)
        }
    }

    override fun photoProfile(user: User): Observable<Boolean> {
        return api.photoProfile(user).map {
            mapper.defaultBooleanMap(it)
        }
    }

    //location
    override fun getProvince(): Observable<List<Location>> {
        return api.getProvince().map {
            mapper.locationMap(it)
        }
    }

    override fun getCity(province: String): Observable<List<Location>> {
        return api.getCity(province).map {
            mapper.locationMap(it)
        }
    }

    override fun getDistrict(city: String): Observable<List<Location>> {
        return api.getDistrict(city).map {
            mapper.locationMap(it)
        }
    }

    override fun getSubDistrict(district: String): Observable<List<Location>> {
        return api.getSubDistrict(district).map {
            mapper.locationMap(it)
        }
    }

    override fun getPostal(subDistrict: String): Observable<List<Location>> {
        return api.getPostal(subDistrict).map {
            mapper.locationMap(it)
        }
    }

    //member type
    override fun getUserType(): Observable<List<UserType>> {
        return api.getUserType().map {
            mapper.memberTypeMap(it)
        }
    }

    //wilayah
    override fun getWilayah(): Observable<List<Wilayah>> {
        return api.getWilayah().map {
            mapper.wilayahMap(it)
        }
    }

    //home waste
    override fun requestCollectWaste(collect : String,
                                     user: User,
                                     date: String?,
                                     wetWeight: Double?,
                                     wetPhoto: String?,
                                     dryWeight: Double?,
                                     dryPhoto: String?,
                                     dry : String?): Observable<Boolean> {
        return api.requestCollectWaste(
            collect = collect,
            user = user,
            date = date,
            wetWeight = wetWeight,
            wetPhoto = wetPhoto,
            dryWeight = dryWeight,
            dryPhoto = dryPhoto,
            dry = dry).map {
            mapper.defaultBooleanMap(it)
        }
    }

    override fun historyHomeWaste(user: User,
                                  startDate: String?,
                                  endDate: String?): Observable<List<HomeWasteTableModel>> {
        return api.historyHomeWaste(
            user = user,
            startDate = startDate,
            endDate = endDate).map {
            mapper.homeWasteModelMap(it)
        }
    }

    override fun scheduleDriver(user: User,
                                date: String?): Observable<List<WasteCollectionTableModel>> {
        return api.scheduleDriver(
            user = user,
            date = date).map {
            mapper.wasteCollectionModelMap(it)
        }
    }

    override fun dryScheduleDriver(user: User,
                                date: String?): Observable<List<DryWasteCollectionTableModel>> {
        return api.scheduleDriver(
            user = user,
            date = date).map {
            mapper.dryWasteCollectionModelMap(it)
        }
    }

    override fun updateWasteStatus(user: User,
                                   wasteId: String?): Observable<Boolean> {
        return api.updateWasteStatus(
            user = user,
            wasteId = wasteId).map {
            mapper.defaultBooleanMap(it)
        }
    }

    override fun historyWasteCollection(user: User,
                                        startDate: String?,
                                        endDate: String?): Observable<List<WasteCollectionHistoryTableModel>> {
        return api.historyWasteCollection(
            user = user,
            startDate = startDate,
            endDate = endDate).map {
            mapper.wasteCollectionHistoryModelMap(it)
        }
    }

    //waste bank
    override fun findWasteBank(user: User): Observable<List<User>> {
        return api.findWasteBank(user).map {
            mapper.findWasteBankMap(it)
        }
    }

    override fun getDryWasteTypes(): Observable<List<DryWasteType>> {
        return api.getDryWaste().map {
            mapper.dryWasteTypeMap(it)
        }
    }

    override fun registerWasteBank(user: User, wasteBankId: String): Observable<Boolean> {
        return api.registerWasteBank(user,wasteBankId).map {
            mapper.defaultBooleanMap(it)
        }
    }

    //balance

    override fun balanceInformation(user: User,
                                    startDate: String?,
                                    endDate: String?): Observable<List<BalanceInformationTableModel>> {
        return api.balanceInformation(user,startDate,endDate).map {
            mapper.balanceMap(it)
        }
    }

    override fun findCustomer(wasteBank: User, value: String): Observable<List<User>> {
        return api.findCustomer(wasteBank,value).map {
            mapper.profileMap(it)
        }
    }
}
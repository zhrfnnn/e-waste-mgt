package app.z.data.response.user

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 11/01/2021
* */

class DefaultBooleanResponse (
    @SerializedName("result")
    var result : Boolean? = null
)
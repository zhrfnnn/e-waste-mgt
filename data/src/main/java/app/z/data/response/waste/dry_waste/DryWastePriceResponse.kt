package app.z.data.response.waste.dry_waste

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 25/01/2021
 * */

class DryWastePriceResponse(
    /*
    [
    {
        "category": "ORGANIK ( ORG)",
        "waste_code": "OR01",
        "waste_name": "Organik Campur",
        "price": "0"
    },
    {
        "category": "PLASTIK (P)",
        "waste_code": "P08",
        "waste_name": "PP Warna (sejenis kemasan",
        "price": "350"
    },
    {
        "category": "LOGAM  Lain-lain",
        "waste_code": "LPG",
        "waste_name": "Perunggu",
        "price": "1400"
    }
]*/
    @SerializedName("category")
    var category : String? = null,
    @SerializedName("waste_code")
    var code : String? = null,
    @SerializedName("waste_name")
    var name : String? = null,
    @SerializedName("price")
    var price : String? = null
)
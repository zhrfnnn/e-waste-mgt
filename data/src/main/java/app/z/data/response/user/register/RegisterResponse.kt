package app.z.data.response.user.register

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 06/01/2021
* */

class RegisterResponse (
    /*
    [
    {
        "result": true
    }
]*/
    @SerializedName("result")
    var result : Boolean? = null
)
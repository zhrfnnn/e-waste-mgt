package app.z.data.response.waste.wastecollection

import com.google.gson.annotations.SerializedName

class HistoryWCResponse (
    /*
    [
{
    "id": "23",
    "collect_id": "0",
    "member": "1",
    "order_time": "2021-01-15 00:00:00",
    "pickup_time": "2021-01-15 00:00:00",
    "driver_id": null,
    "status": "0",
    "collect": "1",
    "waste_id": "1",
    "weight": "15.00",
    "photo": "cropped1447191586.jpg",
    "price": "0",
    "total": "0",
    "moddate": "2021-01-15 03:01:01",
    "address1": " JL RAYA",
    "address2": "",
    "beacon_id": "CF:D7:6F:64:2E:7C",
    "wilayah": "Komp Modernland 2"
},
{
    "id": "22",
    "collect_id": "0",
    "member": "1",
    "order_time": "2021-01-15 00:00:00",
    "pickup_time": "2021-01-15 00:00:00",
    "driver_id": null,
    "status": "0",
    "collect": "1",
    "waste_id": "1",
    "weight": "14.00",
    "photo": "4109_VALORANT_LOGO.PNG",
    "price": "0",
    "total": "0",
    "moddate": "2021-01-15 02:59:32",
    "address1": " JL RAYA",
    "address2": "",
    "beacon_id": "CF:D7:6F:64:2E:7C",
    "wilayah": "Komp Modernland 2"
}
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("collect_id")
    var collectId : String? = null,
    @SerializedName("member")
    var memberId : String? = null,
    @SerializedName("order_time")
    var orderTime : String? = null,
    @SerializedName("pickup_time")
    var pickupTime : String? = null,
    @SerializedName("driver_id")
    var driverId : String? = null,
    @SerializedName("status")
    var status : String? = null,
    @SerializedName("collect")
    var collect : String? = null,
    @SerializedName("waste_id")
    var wasteId : String? = null,
    @SerializedName("weight")
    var weight : String? = null,
    @SerializedName("photo")
    var photo : String? = null,
    @SerializedName("total")
    var total : String? = null,
    @SerializedName("moddate")
    var modifiedDate : String? = null,
    @SerializedName("address1")
    var address : String? = null,
    @SerializedName("beacon_id")
    var beaconId : String? = null,
    @SerializedName("wilayah")
    var wilayah : String? = null
)
package app.z.data.response.user.balance

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 20/01/2021
 * */

class BalanceInformationResponse (
    /*
    [
    {
        "date": "2021-01-08 08:20:02",
        "notes": "Withdraw",
        "amount": "10000"
    }
]*/
    @SerializedName("date")
    var date : String? = null,
    @SerializedName("notes")
    var notes : String? = null,
    @SerializedName("amount")
    var amount : String? = null
)
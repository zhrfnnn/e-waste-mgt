package app.z.data.response.waste.wastebank

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 17/01/2021
 * */

class FindWBResponse (
    /*
    [
    {
        "id": "3",
        "name": "Pemilik WB",
        "homephone": "02100000",
        "handphone": "085600000",
        "beacon_id": "123XYZ",
        "nik": "0102030405",
        "postal": "16514",
        "city": "Kota Depok",
        "district": "Cinere",
        "subdistrict": "Cinere",
        "province": "Jawa Barat",
        "membertype": "2",
        "member_type": "Waste Bank"
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("homephone")
    var homephone : String? = null,
    @SerializedName("handphone")
    var handphone : String? = null,
    @SerializedName("beacon_id")
    var beaconId : String? = null,
    @SerializedName("nik")
    var nik : String? = null,
    @SerializedName("postal")
    var postal : String? = null,
    @SerializedName("city")
    var city : String? = null,
    @SerializedName("district")
    var district : String? = null,
    @SerializedName("subdistrict")
    var subdistrict : String? = null,
    @SerializedName("province")
    var province : String? = null,
    @SerializedName("membertype")
    var membertype : String? = null
)
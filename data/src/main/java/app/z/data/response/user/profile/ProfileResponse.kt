package app.z.data.response.user.profile

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 06/01/2021
* */

class ProfileResponse (
    /*
    [
    {
        "id": "6",
        "name": "ZHARFAN TEST",
        "homephone": "123",
        "handphone": "087123123123",
        "address1": "JL.CILOBAK RAYA NO.59",
        "address2": "RT 04/01 PANGKALAN JATI",
        "city_id": "0",
        "wilayah_id": "0",
        "beacon_id": "0",
        "nik": "432143214321",
        "photo": "",
        "photoprofile": null,
        "membertype": "2",
        "email": "ZHARFAN@TEST.COM",
        "referral": "",
        "reg_date": "2021-01-06",
        "mod_date": "2021-01-06",
        "province": null,
        "city": null,
        "district": null,
        "subdistrict": null,
        "postal": null
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("homephone")
    var homephone : String? = null,
    @SerializedName("handphone")
    var handphone : String? = null,
    @SerializedName("address1")
    var address : String? = null,
    @SerializedName("city_id")
    var cityId : String? = null,
    @SerializedName("wilayah_id")
    var wilayahId : String? = null,
    @SerializedName("beacon_id")
    var beaconId : String? = null,
    @SerializedName("nik")
    var nik : String? = null,
    @SerializedName("photo")
    var nikPicture : String? = null,
    @SerializedName("photoprofile")
    var photoProfile : String? = null,
    @SerializedName("membertype")
    var memberType : String? = null,
    @SerializedName("email")
    var email : String? = null,
    @SerializedName("referral")
    var referralEmail : String? = null,
    @SerializedName("province")
    var province : String? = null,
    @SerializedName("city")
    var city : String? = null,
    @SerializedName("district")
    var district : String? = null,
    @SerializedName("subdistrict")
    var subdistrict : String? = null,
    @SerializedName("postal")
    var postal : String? = null,
    @SerializedName("wilayah")
    var wilayahName : String? = null
)
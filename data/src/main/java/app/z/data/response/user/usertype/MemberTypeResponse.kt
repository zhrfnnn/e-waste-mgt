package app.z.data.response.user.usertype

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 06/01/2021
* */

class MemberTypeResponse (
    /*
    [
    {
        "id": "3",
        "name": "Industrial Waste",
        "menu": "IW, WB"
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("menu")
    var menu : String? = null
)
package app.z.data.response.waste.homewaste

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 15/01/2021
 * */

class HistoryHWResponse (
    /*
    [
    {
        "id": "12",
        "collect_id": "29",
        "member": "1",
        "order_time": "2021-01-11 00:00:00",
        "pickup_time": "2021-01-11 00:00:00",
        "driver_id": null,
        "status": "0",
        "collect": "1",
        "waste_id": "1",
        "weight": "15.00",
        "photo": "IMG_HEADER.PNG",
        "price": "0",
        "total": "0",
        "moddate": "2021-01-11 21:26:41"
    },
    {
        "id": "13",
        "collect_id": "30",
        "member": "1",
        "order_time": "2021-01-11 00:00:00",
        "pickup_time": "2021-01-11 00:00:00",
        "driver_id": null,
        "status": "0",
        "collect": "1",
        "waste_id": "2",
        "weight": "15.00",
        "photo": "IMG_HEADER.PNG",
        "price": "0",
        "total": "0",
        "moddate": "2021-01-11 22:26:38"
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("collect_id")
    var collectId : Boolean? = null,
    @SerializedName("member")
    var member : String? = null,
    @SerializedName("order_time")
    var orderTime : String? = null,
    @SerializedName("pickup_time")
    var pickupTime : String? = null,
    @SerializedName("moddate")
    var modifiedTime : String? = null,
    @SerializedName("driver_id")
    var driverId : String? = null,
    @SerializedName("status")
    var status : String? = null,
    @SerializedName("collect")
    var collect : String? = null,
    @SerializedName("waste_id")
    var wasteId : String? = null,
    @SerializedName("weight")
    var weight : String? = null,
    @SerializedName("photo")
    var photo : String? = null,
    @SerializedName("price")
    var price : String? = null,
    @SerializedName("total")
    var total : String? = null
)
package app.z.data.response.user.login

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 15/01/2021
 * */

class LoginResponse (
    /*
    [
    {
        "id": "9",
        "name": "ZHARFAN MOBILE 2",
        "homephone": "",
        "handphone": "087875037445",
        "address1": "JL.CILOBAK RAYA NO.59",
        "address2": "",
        "city_id": "0",
        "wilayah_id": "0",
        "beacon_id": "123",
        "nik": "30001789456123",
        "photo": "",
        "photoprofile": null,
        "membertype": "1",
        "email": "EGOISTICZHRFNNN@GMAIL.COM",
        "referral": "",
        "reg_date": "2021-01-07",
        "mod_date": "2021-01-07",
        "lastlogin": "2021-01-07 03:17:55",
        "menu": "HW, WB, "
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("handphone")
    var handphone : String? = null,
    @SerializedName("address1")
    var address : String? = null,
    @SerializedName("beacon_id")
    var beaconId : String? = null,
    @SerializedName("nik")
    var nik : String? = null,
    @SerializedName("photo")
    var photoNIK : String? = null,
    @SerializedName("photoprofile")
    var photoprofile : String? = null,
    @SerializedName("membertype")
    var membertype : String? = null,
    @SerializedName("email")
    var email : String? = null,
    @SerializedName("referral")
    var referral : String? = null

)
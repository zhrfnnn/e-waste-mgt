package app.z.data.response.user.wilayah

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 12/01/2021
* */

class WilayahResponse (
    /*
    [
    {
        "id": "1",
        "fleet_id": "1",
        "wilayah": "Komp Mahkota Mas"
    },
    {
        "id": "2",
        "fleet_id": "1",
        "wilayah": "Komp Modernland 1"
    },
    {
        "id": "3",
        "fleet_id": "1",
        "wilayah": "Komp Modernland 2"
    }
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("fleet_id")
    var fleetId : String? = null,
    @SerializedName("wilayah")
    var wilayah : String? = null
)
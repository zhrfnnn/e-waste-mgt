package app.z.data.response.user.location

import com.google.gson.annotations.SerializedName

/**
* Created by Zharfan on 06/01/2021
* */

class LocationResponse (
    /*
    [
    {
        "province": "Bali"
    },
    {
        "province": "Bangka Belitung"
    }
    ]*/
    @SerializedName("province")
    var province : String? = null,
    @SerializedName("city")
    var city : String? = null,
    @SerializedName("district")
    var district : String? = null,
    @SerializedName("subdistrict")
    var subdistrict : String? = null,
    @SerializedName("postal")
    var postal : String? = null
)
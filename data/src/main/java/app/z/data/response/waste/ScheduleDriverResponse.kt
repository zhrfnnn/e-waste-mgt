package app.z.data.response.waste

import com.google.gson.annotations.SerializedName

/**
 * Created by Zharfan on 15/01/2021
 * */

class ScheduleDriverResponse (
    /*
    [
{
    "id": "23",
    "collect_id": "0",
    "member": "1",
    "order_time": "2021-01-15 00:00:00",
    "pickup_time": "2021-01-15 00:00:00",
    "driver_id": null,
    "status": "0",
    "collect": "1",
    "waste_id": "1",
    "weight": "15.00",
    "photo": "CROPPED1447191586.JPG",
    "price": "0",
    "total": "0",
    "moddate": "2021-01-15 03:01:01",
    "name": " HARIS BARU",
    "beacon_id": "123",
    "address1": " JL RAYA"
}
]*/
    @SerializedName("id")
    var id : String? = null,
    @SerializedName("collect_id")
    var collectId : String? = null,
    @SerializedName("member")
    var memberId : String? = null,
    @SerializedName("order_time")
    var orderTime : String? = null,
    @SerializedName("pickup_time")
    var pickupTime : String? = null,
    @SerializedName("driver_id")
    var driverId : String? = null,
    @SerializedName("status")
    var status : String? = null,
    @SerializedName("collect")
    var collect : String? = null,
    @SerializedName("waste_id")
    var waste_id : String? = null,
    @SerializedName("weight")
    var weight : String? = null,
    @SerializedName("photo")
    var photo : String? = null,
    @SerializedName("price")
    var price : String? = null,
    @SerializedName("total")
    var total : String? = null,
    @SerializedName("moddate")
    var modifiedDate : String? = null,
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("beacon_id")
    var beaconId : String? = null,
    @SerializedName("address1")
    var address : String? = null
)
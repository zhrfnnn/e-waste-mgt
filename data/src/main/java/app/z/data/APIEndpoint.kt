package app.z.data

import app.z.data.response.user.DefaultBooleanResponse
import app.z.data.response.user.balance.BalanceInformationResponse
import app.z.data.response.user.location.LocationResponse
import app.z.data.response.user.login.LoginResponse
import app.z.data.response.user.profile.ProfileResponse
import app.z.data.response.user.register.RegisterResponse
import app.z.data.response.user.usertype.MemberTypeResponse
import app.z.data.response.user.wilayah.WilayahResponse
import app.z.data.response.waste.ScheduleDriverResponse
import app.z.data.response.waste.dry_waste.DryWastePriceResponse
import app.z.data.response.waste.homewaste.HistoryHWResponse
import app.z.data.response.waste.wastebank.FindWBResponse
import app.z.data.response.waste.wastecollection.HistoryWCResponse
import app.z.data.util.toRequestBody
import app.z.domain.model.User
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

/**
* Created by Zharfan on 06/01/2021
* */

interface APIEndpoint {

    @Multipart
    @POST("register.php")
    fun register(@Part("name") name : RequestBody?,
                 @Part("homephone") homephone : RequestBody?,
                 @Part("handphone") handphone : RequestBody?,
                 @Part("address1") address1 : RequestBody?,
                 @Part("address2") address2 : RequestBody?,
                 @Part("province") province : RequestBody?,
                 @Part("city") city : RequestBody?,
                 @Part("district") district : RequestBody?,
                 @Part("subdistrict") subDistrict : RequestBody?,
                 @Part("postal") postal : RequestBody?,
                 @Part("beacon_id") beaconId : RequestBody?,
                 @Part("nik") nik : RequestBody?,
                 @Part photo : MultipartBody.Part?,
                 @Part("membertype") memberType : RequestBody?,
                 @Part("wilayah_id") wilayahId : RequestBody?,
                 @Part("email") email : RequestBody?,
                 @Part("referral") referral : RequestBody?,
                 @Part("password") password : RequestBody?) : Observable<List<RegisterResponse>>

    @FormUrlEncoded
    @POST("profile.php")
    fun getProfilesById(@Field("key") key : String = "member.id",
                        @Field("value") id : Long?) : Observable<List<ProfileResponse>>

    @FormUrlEncoded
    @POST("profile.php")
    fun updateProfile(@Field("act") act : String? = "save",
                      @Field("id") id : String?,
                      @Field("name") name : String?,
                      @Field("homephone") homephone : String?,
                      @Field("handphone") handphone : String?,
                      @Field("address1") address1 : String?,
                      @Field("address2") address2 : String?,
                      @Field("province") province : String?,
                      @Field("city") city : String?,
                      @Field("district") district : String?,
                      @Field("subdistrict") subDistrict : String?,
                      @Field("postal") postal : String?,
                      @Field("beacon_id") beaconId : String?,
                      @Field("nik") nik : String?,
                      @Field("membertype") memberType : String?,
                      @Field("wilayah_id") wilayahId : String?,
                      @Field("email") email : String?,
                      @Field("referral") referral : String?) : Observable<List<DefaultBooleanResponse>>

    @Multipart
    @POST("foto_profile.php")
    fun photoProfile(@Part("member") id : RequestBody?,
                     @Part photo : MultipartBody.Part?) : Observable<List<DefaultBooleanResponse>>

    @FormUrlEncoded
    @POST("login.php")
    fun login(@Field("u") username : String,
              @Field("p") password : String) : Observable<List<LoginResponse>>

    @GET("member_type.php")
    fun getUserType() : Observable<List<MemberTypeResponse>>

    @GET("wilayah.php")
    fun getWilayah() : Observable<List<WilayahResponse>>

    @FormUrlEncoded
    @POST("postal.php")
    fun getProvince(@Field("field") province : String = "province") : Observable<List<LocationResponse>>

    @FormUrlEncoded
    @POST("postal.php")
    fun getCity(@Field("field") city : String = "city",
                @Field("key") key : String = "province",
                @Field("value") province : String) : Observable<List<LocationResponse>>

    @FormUrlEncoded
    @POST("postal.php")
    fun getDistrict(@Field("field") district : String = "district",
                    @Field("key") key : String = "city",
                    @Field("value") city : String) : Observable<List<LocationResponse>>

    @FormUrlEncoded
    @POST("postal.php")
    fun getSubDistrict(@Field("field") subDistrict : String = "subdistrict",
                       @Field("key") key : String = "district",
                       @Field("value") district : String) : Observable<List<LocationResponse>>

    @FormUrlEncoded
    @POST("postal.php")
    fun getPostal(@Field("field") postal : String = "postal",
                  @Field("key") key : String = "subdistrict",
                  @Field("value") subDistrict : String) : Observable<List<LocationResponse>>

    @Multipart
    @POST("collect_hw.php")
    fun requestCollectWaste(@Part("collect") collect : RequestBody?,
                            @Part("member") userId : RequestBody?,
                            @Part("pickup_time") date : RequestBody?,
                            @Part("wetweight") wetWeight : RequestBody?,
                            @Part wetPhoto : MultipartBody.Part?,
                            @Part("dryweight") dryWeight : RequestBody?,
                            @Part dryPhoto : MultipartBody.Part?,
                            @Part("dry") dryTipe : RequestBody?) : Observable<List<DefaultBooleanResponse>>

    @FormUrlEncoded
    @POST("collect_hw_history.php")
    fun historyHomeWaste(@Field("member") userId : String,
                         @Field("start") startDate : String?,
                         @Field("end") endDate : String?) : Observable<List<HistoryHWResponse>>

    @FormUrlEncoded
    @POST("schedule.php")
    fun scheduleDriver(@Field("member") driverUserId : String,
                       @Field("date") date : String?) : Observable<List<ScheduleDriverResponse>>

    @FormUrlEncoded
    @POST("collect_wc_status.php")
    fun updateWasteStatus(@Field("member") driverUserId : String,
                          @Field("id") wasteId : String?) : Observable<List<DefaultBooleanResponse>>

    @FormUrlEncoded
    @POST("collect_wc_history.php")
    fun historyWasteCollection(@Field("member") driverUserId : String,
                         @Field("start") startDate : String?,
                         @Field("end") endDate : String?) : Observable<List<HistoryWCResponse>>

    @FormUrlEncoded
    @POST("find_wb.php")
    fun findWasteBank(@Field("member") userId : String) : Observable<List<FindWBResponse>>

    @GET("waste_price.php")
    fun getDryWaste() : Observable<List<DryWastePriceResponse>>

    @FormUrlEncoded
    @POST("collector_member.php")
    fun registerWasteBank(@Field("act") act : String = "save",
                          @Field("member") memberId : String?,
                          @Field("collector") wasteBankId : String?) : Observable<List<DefaultBooleanResponse>>

    /*@FormUrlEncoded
    @POST("collector_member.php")
    fun getRegisteredWasteBank(@Field("act") act : String = "save",
                               @Field("member") memberId : String?) : Observable<>*/

    @FormUrlEncoded
    @POST("balance.php")
    fun balanceInformation(@Field("member") userId : String,
                           @Field("start") startDate : String?,
                           @Field("end") endDate : String?) : Observable<List<BalanceInformationResponse>>


    @FormUrlEncoded
    @POST("find_member.php")
    fun findCustomer(@Field("member") wasteBankId : String,
                     @Field("value") value : String?) : Observable<List<ProfileResponse>>
}
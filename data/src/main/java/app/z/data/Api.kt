package app.z.data

import app.z.data.response.user.DefaultBooleanResponse
import app.z.data.response.user.balance.BalanceInformationResponse
import app.z.data.response.user.location.LocationResponse
import app.z.data.response.user.login.LoginResponse
import app.z.data.response.user.profile.ProfileResponse
import app.z.data.response.user.register.RegisterResponse
import app.z.data.response.user.usertype.MemberTypeResponse
import app.z.data.response.user.wilayah.WilayahResponse
import app.z.data.response.waste.ScheduleDriverResponse
import app.z.data.response.waste.dry_waste.DryWastePriceResponse
import app.z.data.response.waste.homewaste.HistoryHWResponse
import app.z.data.response.waste.wastebank.FindWBResponse
import app.z.data.response.waste.wastecollection.HistoryWCResponse
import app.z.data.util.toMultiPartBodyPart
import app.z.data.util.toRequestBody
import app.z.domain.model.User
import app.z.domain.model.tablemodel.BalanceInformationTableModel
import io.reactivex.Observable
import java.io.File
import javax.inject.Inject

/**
* Created by Zharfan on 06/01/2021
* */

class Api @Inject constructor(private val apiEndpoint: APIEndpoint) {

    fun register(user: User) : Observable<List<RegisterResponse>> {
        return apiEndpoint.register(
            name = user.fullname.toRequestBody("multipart/form-data"),
            homephone = null/*perlu ditanyakan*/,
            handphone = user.mobileNumber.toRequestBody("multipart/form-data"),
            address1 = user.address.toRequestBody("multipart/form-data"),
            address2 = null,
            province = user.province.toRequestBody("multipart/form-data"),
            city = user.city.toRequestBody("multipart/form-data"),
            district = user.district.toRequestBody("multipart/form-data"),
            subDistrict = user.subDistrict.toRequestBody("multipart/form-data"),
            postal = user.postalCode.toRequestBody("multipart/form-data"),
            beaconId = null/*perlu ditanyakan*/,
            nik = user.nik.toRequestBody("multipart/form-data"),
            photo = File(user.nikPicture).toMultiPartBodyPart("photo1"),
            memberType = user.position?.id.toString().toRequestBody("multipart/form-data"),
            wilayahId = user.wilayah?.id.toString().toRequestBody("multipart/form-data"),
            email = user.email?.toUpperCase().toRequestBody("multipart/form-data"),
            referral = user.referralEmail.toRequestBody("multipart/form-data"),
            password = user.password?.toUpperCase().toRequestBody("multipart/form-data")
        )
    }

    fun getProfilesByUserId(user: User) : Observable<List<ProfileResponse>> {
        return apiEndpoint.getProfilesById(
            id = user.id
        )
    }

    fun login(username : String, password : String) : Observable<List<LoginResponse>> {
        return apiEndpoint.login(
            username = username,
            password = password
        )
    }

    fun updateProfile(user: User) : Observable<List<DefaultBooleanResponse>> {
        return apiEndpoint.updateProfile(
            id = user.id.toString(),
            name = user.fullname,
            homephone = "",
            handphone = user.mobileNumber,
            address1 = user.address,
            address2 = null,
            province = user.province,
            city = user.city,
            district = user.district,
            subDistrict = user.subDistrict,
            postal = user.postalCode,
            beaconId = user.beaconId,
            nik = user.nik,
            memberType = user.position?.id.toString(),
            wilayahId = user.wilayah?.id.toString(),
            email = user.email,
            referral = user.referralEmail
        )
    }

    fun photoProfile(user: User) : Observable<List<DefaultBooleanResponse>> {
        return apiEndpoint.photoProfile(
            id = user.id.toString().toRequestBody("multipart/form-data"),
            photo = File(user.picture).toMultiPartBodyPart("photo1")
        )
    }

    fun getUserType() : Observable<List<MemberTypeResponse>> {
        return apiEndpoint.getUserType()
    }

    fun getWilayah() : Observable<List<WilayahResponse>> {
        return apiEndpoint.getWilayah()
    }

    fun getProvince() : Observable<List<LocationResponse>> {
        return apiEndpoint.getProvince()
    }

    fun getCity(province : String) : Observable<List<LocationResponse>> {
        return apiEndpoint.getCity(
            province = province
        )
    }

    fun getDistrict(city : String) : Observable<List<LocationResponse>> {
        return apiEndpoint.getDistrict(
            city = city
        )
    }

    fun getSubDistrict(district : String) : Observable<List<LocationResponse>> {
        return apiEndpoint.getSubDistrict(
            district = district
        )
    }

    fun getPostal(subDistrict : String) : Observable<List<LocationResponse>> {
        return apiEndpoint.getPostal(
            subDistrict = subDistrict
        )
    }

    fun requestCollectWaste(collect : String,
                            user: User,
                            date : String?,
                            wetWeight : Double?,
                            wetPhoto : String?,
                            dryWeight : Double?,
                            dryPhoto : String?,
                            dry : String?) : Observable<List<DefaultBooleanResponse>> {
        return apiEndpoint.requestCollectWaste(
            collect = collect.toRequestBody("multipart/form-data"),
            userId = user.id.toString().toRequestBody("multipart/form-data"),
            date = date.toRequestBody("multipart/form-data"),
            wetWeight = wetWeight.toRequestBody("multipart/form-data"),
            wetPhoto = if (wetPhoto.isNullOrBlank()) null else File(wetPhoto).toMultiPartBodyPart("photo1"),
            dryWeight = dryWeight.toRequestBody("multipart/form-data"),
            dryPhoto = if (dryPhoto.isNullOrBlank()) null else File(dryPhoto).toMultiPartBodyPart("photo2"),
            dryTipe = dry.toRequestBody("multipart/form-data")
        )
    }

    fun historyHomeWaste(user: User,
                         startDate : String?,
                         endDate : String?) : Observable<List<HistoryHWResponse>> {
        return apiEndpoint.historyHomeWaste(
            userId = user.id.toString(),
            startDate = startDate,
            endDate = endDate
        )
    }

    fun scheduleDriver(user: User,
                       date : String?) : Observable<List<ScheduleDriverResponse>>{
        return apiEndpoint.scheduleDriver(
            driverUserId = user.id.toString(),
            date = date
        )
    }

    fun updateWasteStatus(user: User,
                          wasteId : String?) : Observable<List<DefaultBooleanResponse>> {
        return apiEndpoint.updateWasteStatus(
            driverUserId = user.id.toString(),
            wasteId = wasteId
        )
    }

    fun historyWasteCollection(user: User,
                               startDate : String?,
                               endDate : String?) : Observable<List<HistoryWCResponse>> {
        return apiEndpoint.historyWasteCollection(
            driverUserId = user.id.toString(),
            startDate = startDate,
            endDate = endDate
        )
    }

    fun findWasteBank(user: User) : Observable<List<FindWBResponse>> {
        return apiEndpoint.findWasteBank(
            userId = user.id.toString()
        )
    }

    fun getDryWaste() : Observable<List<DryWastePriceResponse>> {
        return apiEndpoint.getDryWaste()
    }

    fun registerWasteBank(user: User,
                          wasteBankId : String?) : Observable<List<DefaultBooleanResponse>> {
        return apiEndpoint.registerWasteBank(
            memberId = user.id.toString(),
            wasteBankId = wasteBankId
        )
    }

    fun balanceInformation(user: User,
                           startDate : String?,
                           endDate : String?) : Observable<List<BalanceInformationResponse>> {
        return apiEndpoint.balanceInformation(
            userId = user.id.toString(),
            startDate = startDate,
            endDate = endDate
        )
    }

    fun findCustomer(wasteBank : User,
                     value : String) : Observable<List<ProfileResponse>> {
        return apiEndpoint.findCustomer(
            wasteBankId = wasteBank.id.toString(),
            value = value
        )
    }

}
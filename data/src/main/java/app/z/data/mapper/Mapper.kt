package app.z.data.mapper

import app.z.data.response.user.DefaultBooleanResponse
import app.z.data.response.user.balance.BalanceInformationResponse
import app.z.data.response.user.location.LocationResponse
import app.z.data.response.user.login.LoginResponse
import app.z.data.response.user.profile.ProfileResponse
import app.z.data.response.user.register.RegisterResponse
import app.z.data.response.user.usertype.MemberTypeResponse
import app.z.data.response.user.wilayah.WilayahResponse
import app.z.data.response.waste.ScheduleDriverResponse
import app.z.data.response.waste.dry_waste.DryWastePriceResponse
import app.z.data.response.waste.homewaste.HistoryHWResponse
import app.z.data.response.waste.wastebank.FindWBResponse
import app.z.data.response.waste.wastecollection.HistoryWCResponse
import app.z.data.util.Session
import app.z.data.util.toDateFromServer
import app.z.domain.model.*
import app.z.domain.model.tablemodel.*
import javax.inject.Inject

/**
* Created by Zharfan on 06/01/2021
* */

class Mapper @Inject constructor() {

    //defaultboolean
    fun defaultBooleanMap(responseList: List<DefaultBooleanResponse>) : Boolean {
        return responseList[0].result!!
    }

    //register
    fun registerMap(responseList : List<RegisterResponse>) : Boolean {
        return responseList[0].result!!
    }

    //login
    fun loginMap(responseList : List<LoginResponse>) : User {
        val responseUser = responseList[0]
        return User(
            id = responseUser.id!!.toLong(),
            fullname = responseUser.name,
            address = responseUser.address,
            nik = responseUser.nik,
            nikPicture = responseUser.photoNIK,
            position = UserType.getById(responseUser.membertype?.toInt()),
            email = responseUser.email,
            referralEmail = responseUser.referral,
            mobileNumber = responseUser.handphone,
            beaconId = responseUser.beaconId,
            wilayah = Wilayah()
        )
    }

    //profile
    fun profileMap(responseList: List<ProfileResponse>) : List<User> {
        return responseList.map { profileMap(it) }
    }
    fun profileMap(response : ProfileResponse) : User {
        return User(
            id = response.id!!.toLong(),
            fullname = response.name,
            address = response.address,
            picture = response.photoProfile,
            nik = response.nik,
            nikPicture = response.nikPicture,
            position = UserType.getById(response.memberType?.toInt()),
            email = response.email,
            referralEmail = response.referralEmail,
            mobileNumber = response.handphone,
            beaconId = response.beaconId,
            province = response.province,
            city = response.city,
            district = response.district,
            subDistrict = response.subdistrict,
            postalCode = response.postal,
            wilayahName = response.wilayahName,
            wilayah = Wilayah()
        )
    }

    //location
    fun locationMap(responseList: List<LocationResponse>): List<Location> {
        return responseList.map { (locationMap(it)) }
    }
    fun locationMap(response: LocationResponse): Location {
        return Location(
            province = response.province,
            city = response.city,
            district = response.district,
            subDistrict = response.subdistrict,
            postal = response.postal
        )
    }

    //membertype
    fun memberTypeMap(responseList: List<MemberTypeResponse>): List<UserType> {
        return responseList.map { (memberTypeMap(it)) }
    }
    fun memberTypeMap(response: MemberTypeResponse): UserType {
        return UserType(
            id = response.id?.toInt(),
            name = response.name
        )
    }

    //wilayah
    fun wilayahMap(responseList: List<WilayahResponse>): List<Wilayah> {
        return responseList.map { (wilayahMap(it)) }
    }
    fun wilayahMap(response: WilayahResponse): Wilayah {
        return Wilayah(
            id = response.id?.toInt(),
            wilayah = response.wilayah
        )
    }

    //homewastemodel
    fun homeWasteModelMap(responseList: List<HistoryHWResponse>): List<HomeWasteTableModel> {
        return responseList.map { (homeWasteModelMap(it)) }
    }
    fun homeWasteModelMap(response: HistoryHWResponse): HomeWasteTableModel {
        val homeWasteTableModel = HomeWasteTableModel(
            truckId = response.driverId,
            wetWaste = if (response.wasteId.equals("1")) response.weight?.toDouble() else null,
            dryWaste = if (!response.wasteId.equals("1")) response.weight?.toDouble() else null,
            total = response.total?.toDouble()
        )
        homeWasteTableModel.date = response.orderTime?.toDateFromServer()
        return homeWasteTableModel
    }

    //wastecollectionmodel
    fun wasteCollectionModelMap(responseList: List<ScheduleDriverResponse>): List<WasteCollectionTableModel> {
        return responseList.map { (wasteCollectionModelMap(it)) }
    }
    fun wasteCollectionModelMap(response: ScheduleDriverResponse): WasteCollectionTableModel {
        val wasteCollectionTableModel = WasteCollectionTableModel(
            id = response.id,
            homeId = response.beaconId,
            wasteWeight = response.weight?.toDoubleOrNull(),
            image = response.photo,
            status = response.status
        )
        wasteCollectionTableModel.time = response.modifiedDate

        return wasteCollectionTableModel
    }

    //dryWasteCollection

    fun dryWasteCollectionModelMap(responseList: List<ScheduleDriverResponse>): List<DryWasteCollectionTableModel> {
        return responseList.map { (dryWasteCollectionModelMap(it)) }
    }
    fun dryWasteCollectionModelMap(response: ScheduleDriverResponse): DryWasteCollectionTableModel {
        val wasteCollectionTableModel = DryWasteCollectionTableModel()
        wasteCollectionTableModel.id = response.id
        wasteCollectionTableModel.homeId = response.beaconId
        wasteCollectionTableModel.wasteWeight = response.weight?.toDoubleOrNull()
        wasteCollectionTableModel.image = response.photo
        wasteCollectionTableModel.status = response.status
        wasteCollectionTableModel.time = response.modifiedDate
        wasteCollectionTableModel.memberId = response.memberId?.toLongOrNull()
        wasteCollectionTableModel.price = response.price?.toDoubleOrNull()
        wasteCollectionTableModel.totalPrice = response.total?.toDoubleOrNull()
        return wasteCollectionTableModel
    }

    //wastecollectionhistorymodel
    fun wasteCollectionHistoryModelMap(responseList: List<HistoryWCResponse>): List<WasteCollectionHistoryTableModel> {
        return responseList.map { (wasteCollectionHistoryModelMap(it)) }
    }
    fun wasteCollectionHistoryModelMap(response: HistoryWCResponse): WasteCollectionHistoryTableModel {
        val wasteCollectionHistoryTableModel = WasteCollectionHistoryTableModel(
            address = response.address,
            wilayah = response.wilayah,
            homeId = response.beaconId,
            weight = response.weight?.toDoubleOrNull(),
            total = response.total?.toDoubleOrNull()
        )
        wasteCollectionHistoryTableModel.date = response.modifiedDate.toDateFromServer()
        return wasteCollectionHistoryTableModel
    }

    //wastebank
    fun findWasteBankMap(responseList : List<FindWBResponse>): List<User> {
        return responseList.map { findWasteBankMap(it) }
    }

    fun findWasteBankMap(response: FindWBResponse) : User {
        return User(
            id = response.id!!.toLong(),
            fullname = response.name,
            nik = response.nik,
            position = UserType.getById(response.membertype?.toInt()),
            mobileNumber = response.handphone,
            beaconId = response.beaconId,
            province = response.province,
            city = response.city,
            district = response.district,
            subDistrict = response.subdistrict,
            postalCode = response.postal,
            wilayah = Wilayah()
        )
    }

    //dryWasteType
    fun dryWasteTypeMap(responseList : List<DryWastePriceResponse>) : List<DryWasteType> {
        return responseList.map {
            dryWasteTypeMap(it)
        }
    }

    fun dryWasteTypeMap(response : DryWastePriceResponse) : DryWasteType {
        return DryWasteType(
            name = response.name,
            category = response.category,
            code = response.code,
            price = response.price?.toDoubleOrNull()
        )
    }

    //balanceinformation
    fun balanceMap(responseList : List<BalanceInformationResponse>) : List<BalanceInformationTableModel> {
        return responseList.map { balanceMap(it) }
    }

    fun balanceMap(response : BalanceInformationResponse) : BalanceInformationTableModel {
        val balanceInformationTableModel = BalanceInformationTableModel()
        balanceInformationTableModel.date = response.date.toDateFromServer()
        balanceInformationTableModel.notes = response.notes
        balanceInformationTableModel.mutation = response.amount?.toDoubleOrNull()
//        balanceInformationTableModel.balance

        return balanceInformationTableModel
    }
}